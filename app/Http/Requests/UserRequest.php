<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'name' => 'required',
//            'email' => 'required|unique:admins,email,'.$this->id,
//            'image' => 'sometimes|image',
//            'password' => 'required_without:id|confirmed',
//            'phone' => 'required|numeric|unique:admins,phone,'.$this->id,
        ];
    }

    public function messages()
    {
        return [
//            'name.required' => 'هذا الحقل مطلوب',
//            'gender.required' => 'هذا الحقل مطلوب',
//            'email.required' => 'هذا الحقل مطلوب',
//            'password.min' => 'عدد الحروف لا تقل عن 6 احرف',
//            'email.unique' => 'البريد الالكتروني موجود من قبل يجب اضافه بريد اخر'
        ];
    }
}
