@extends('dashboard.layouts.master')

@section('title', trans('back.edit-var',['var'=>trans('back.permission')]))


@section('content')


    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <i class="icon-arrow-right6 position-left"></i>
                    <span class="text-semibold">@lang('back.home')</span>
                    - @lang('back.create-var',['var'=>trans('back.permission')])
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ route('dashboard.index') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a>
                </li>
                <li><a href="{{ route('permissions.index') }}"><i
                                class="icon-admin position-left"></i> @lang('back.permissions')</a></li>
                <li class="active">@lang('back.edit-var',['var'=>trans('back.permission')])</li>
            </ul>

            @include('dashboard.includes.quick-links')
        </div>
    </div>
    <!-- /page header -->




    <div class="row" style="margin: 15px;">

        <div class="col-md-12">

        @include('dashboard.includes.errors')

            <!-- Basic layout-->
            <form action="{{ route('permissions.update',$group->id) }}" class="form-horizontal" method="post"
                  enctype="multipart/form-data">
{{--                {{ Form::token() }}--}}
                @csrf
                @method('PUT')
                <input type="hidden" name="administration_group_id" value="{{$group->id}}"/>
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title"> تعديل صلاحية جديدة </h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">

                        <div class="form-group">
                            <label class="col-lg-3 control-label"> {{ trans('back.name_of_job') }} </label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="name" value="{{ $group->name }}"
                                       placeholder="{{ trans('back.name_of_job') }}" required>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-lg-3 control-label"> {{ trans('dash.desc') }} </label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="description" value="{{ $group->description }}"
                                       placeholder="{{ trans('dash.desc') }}" required>
                            </div>
                        </div>


                        @foreach(cruds() as $i => $crud)
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h5 class="panel-title">@lang('dash.'.$crud)</h5>
                                    <div class="heading-elements">
                                        <ul class="icons-list">
                                            <li><a data-action="collapse"></a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label class="control-label col-lg-3"> عرض </label>
                                                <div class="col-lg-9">
                                                    <div class="checkbox checkbox-switchery switchery-xs">
                                                        <label>
                                                            {!! Form::checkbox('perms[]', "$crud.index", checkIfHasRole($group, $crud, 'index'), ['class' => 'switchery form-control']) !!}                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label class="control-label col-lg-3"> انشاء </label>
                                                <div class="col-lg-9">
                                                    <div class="checkbox checkbox-switchery switchery-xs">
                                                        <label>
                                                            {!! Form::checkbox('perms[]', "$crud.create", checkIfHasRole($group, $crud, 'create'), ['class' => 'switchery form-control']) !!}
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label class="control-label col-lg-3"> حفظ الانشاء </label>
                                                <div class="col-lg-9">
                                                    <div class="checkbox checkbox-switchery switchery-xs">
                                                        <label>
                                                            {!! Form::checkbox('perms[]', "$crud.store", checkIfHasRole($group, $crud, 'create'), ['class' => 'switchery form-control']) !!}
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label class="control-label col-lg-3"> تعديل </label>
                                                <div class="col-lg-9">
                                                    <div class="checkbox checkbox-switchery switchery-xs">
                                                        <label>
                                                            {!! Form::checkbox('perms[]', "$crud.edit", checkIfHasRole($group, $crud, 'edit'), ['class' => 'switchery form-control']) !!}                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label class="control-label col-lg-3"> حفظ التعديل </label>
                                                <div class="col-lg-9">
                                                    <div class="checkbox checkbox-switchery switchery-xs">
                                                        <label>
                                                            {!! Form::checkbox('perms[]', "$crud.update", checkIfHasRole($group, $crud, 'edit'), ['class' => 'switchery form-control']) !!}                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label class="control-label col-lg-3"> حذف </label>
                                                <div class="col-lg-9">
                                                    <div class="checkbox checkbox-switchery switchery-xs">
                                                        <label>
                                                            {!! Form::checkbox('perms[]', "ajax-delete-".str()->singular($crud), checkIfHasRole($group, $crud, 'delete'), ['class' => 'switchery form-control']) !!}
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach


                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h5 class="panel-title">@lang('dash.settings')</h5>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label class="control-label col-lg-3"> عرض </label>
                                            <div class="col-lg-9">
                                                <div class="checkbox checkbox-switchery switchery-xs">
                                                    <label>
                                                        <input type="checkbox" name="perms[]" class="switchery"
                                                               value="settings.index">
                                                        @if(in_array('settings.index',$perms))  @endif
                                                        {{--{!! Form::checkbox('perms[]', "$crud.index", checkIfHasRole($group, $crud, 'index'), ['class' => 'switchery form-control']) !!}                                                        </label>--}}

                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label class="control-label col-lg-3"> تعديل </label>
                                            <div class="col-lg-9">
                                                <div class="checkbox checkbox-switchery switchery-xs">
                                                    <label>
                                                        <input type="checkbox" name="perms[]" class="switchery"
                                                               value="settings.updateAll">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                        {{-- ==== edit ====== --}}

{{--                        <div class="panel panel-flat">--}}
{{--                            <div class="panel-heading">--}}
{{--                                <h5 class="panel-title"> {{ trans('back.clients') }} </h5>--}}

{{--                                <div class="heading-elements">--}}
{{--                                    <ul class="icons-list">--}}
{{--                                        <li><a data-action="collapse"></a></li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="panel-body">--}}
{{--                                <div class="row">--}}

{{--                                    <div class="col-lg-2">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label class="control-label col-lg-3">--}}
{{--                                                عرض{{ trans('back.clients') }} </label>--}}
{{--                                            <div class="col-lg-9">--}}
{{--                                                <div class="checkbox checkbox-switchery switchery-xs">--}}
{{--                                                    <label>--}}
{{--                                                        <input type="checkbox" name="perms[]" class="switchery"--}}
{{--                                                               value="client.index"--}}

{{--                                                               @if(in_array('client.index',$perms)) checked--}}
{{--                                                                @endif >--}}
{{--                                                    </label>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-lg-3">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label class="control-label col-lg-3"> {{ trans('dash.create') }} </label>--}}
{{--                                            <div class="col-lg-9">--}}
{{--                                                <div class="checkbox checkbox-switchery switchery-xs">--}}
{{--                                                    <label>--}}
{{--                                                        <input type="checkbox" name="perms[]" class="switchery"--}}
{{--                                                               value="client.create"--}}
{{--                                                               @if(in_array('client.create',$perms)) checked @endif>--}}
{{--                                                    </label>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-lg-3">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label class="control-label col-lg-3"> {{ trans('dash.edit') }} </label>--}}
{{--                                            <div class="col-lg-9">--}}
{{--                                                <div class="checkbox checkbox-switchery switchery-xs">--}}
{{--                                                    <label>--}}
{{--                                                        <input type="checkbox" name="perms[]" class="switchery"--}}
{{--                                                               value="client.edit"--}}
{{--                                                               @if(in_array('client.edit',$perms)) checked @endif>--}}
{{--                                                    </label>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-lg-3">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label class="control-label col-lg-3"> {{ trans('dash.delete') }} </label>--}}
{{--                                            <div class="col-lg-9">--}}
{{--                                                <div class="checkbox checkbox-switchery switchery-xs">--}}
{{--                                                    <label>--}}
{{--                                                        <input type="checkbox" name="perms[]" class="switchery"--}}
{{--                                                               value="ajax-delete-client"--}}
{{--                                                               @if(in_array('ajax-delete-client',$perms)) checked @endif>--}}
{{--                                                    </label>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}


                        <div class="text-right">
                            {{--<input type="submit" class="btn btn-primary" name="forward"--}}
                            {{--value=" {{ trans('dash.update_and_forword_2_list') }} "/>--}}
                            <input type="submit" class="btn btn-success" name="back"
                                   value=" {{ trans('dash.update_and_come_back') }} "/>
                        </div>
                    </div>
                </div>
            </form>
            <!-- /basic layout -->


        </div>


    </div>



@stop
