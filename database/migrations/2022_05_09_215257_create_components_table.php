<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComponentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('components', function (Blueprint $table) {
            $table->id();
            $table->integer('qty');
            $table->double('price_gram'); // سعر 50 جرام
            $table->double('size'); //الحجم بالجرام 50
            $table->double('size_one_gram'); // وزن الجرام الواحد
            $table->text('description')->nullable();
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('item_id');
            $table->foreign('product_id')
                ->references('id')->on('products')->onDelete('cascade');
            $table->foreign('item_id')
                ->references('id')->on('items')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('components');
    }
}
