<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\DelegateRequest;
use App\Models\Delegate;
use App\Models\Supplier;
use Illuminate\Http\Request;

class DelegateController extends Controller
{
    public function index()
    {
        $delegates = Delegate::latest()->get();
        return view('dashboard.Delegates.index',compact('delegates'));
    }

    public function create()
    {
        $suppliers = Supplier::get();
        return view('dashboard.Delegates.create',compact('suppliers'));
    }

    public function store(DelegateRequest $request)
    {
        $input = $request->all();
        $input['visiting'] = $request->input('visiting');
//        dd($request->all());
        Delegate::create($input);
        return redirect()->route('delegates.index')->with('message', 'تم إضافه المندوب بنجاح');

    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $suppliers = Supplier::get();
        $delegate = Delegate::whereId($id)->first();
//        $delegateVis = explode(',', $delegate->visiting);
        if ($delegate){
            return view('dashboard.Delegates.edit',compact('delegate','suppliers'));
        }else{
            return back()->with('error', 'لا يوجد بيانات');

        }
    }


    public function update(DelegateRequest $request, $id)
    {
        $delegate = Delegate::whereId($id)->first();
        if ($delegate){
//            $delegate->update($request->validated());
            $input = $request->all();
            $input['visiting'] = $request->input('visiting');
//            dd($request->all());
            $delegate->update($input);
            return redirect()->route('delegates.index')->with('message', 'تم تعديل المندوب بنجاح');

        }else{
            return back()->with('error', 'لا يوجد بيانات');

        }
    }


    public function destroy($id)
    {
        $delegate = Delegate::whereId($id)->first();
        if ($delegate){
            $delegate->delete();
            return redirect()->route('delegates.index')->with('message', 'تم حذف المندوب بنجاح');
        }else{
            return back()->with('error', 'لا يوجد بيانات');

        }
    }
}
