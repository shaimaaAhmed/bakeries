<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DelegateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'    => 'required|string',
            'supplier_id'    => 'required',
            'visiting'    => 'required',
//            'visiting_dates'    => 'required',
            'phone'    => 'required|numeric',
            'notes' => 'nullable|string',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'اسم المندوب مطلوب',
            'name.string' => 'يجب ان يكون اسم المندوب حروف',
            'phone.required' => 'رقم المندوب مطلوب',
            'phone.numeric' => 'يجب ان يكون رقم المندوب ارقام',
            'notes.string' => 'يجب ان يكون ملاحظه المندوب كلمات',
            'supplier_id.required' => 'اسم التاجر مطلوب',
            'visiting.required' => 'مواعيد الزياره مطلوبه',
//            'visiting_dates.required' => 'مواعيد الزياره مطلوبه',
        ];
    }
}
