@extends('dashboard.layouts.master')

@section('title', 'تعديل فاتوره مشتريات')

@section('content')
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">إداره المخابز</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('index')}}">الرئسيه</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('invoices.index')}}">فواتير المشتريات</a>
                                </li>
                                <li class="breadcrumb-item active"><a href="{{route('invoices.edit',$invoice->id)}}">تعديل فاتوره مشتريات</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                <div class="mb-1 breadcrumb-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                        <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body"><section class="form-control-repeater">
                <div class="row">
                    <!-- Invoice repeater -->
                    <div class="col-12">
                        <div class="card">
                            @include('dashboard.includes.errors')
                            <div class="card-header">
                                <h4 class="card-title">تعديل فاتوره مشتريات</h4>
                            </div>
                            <div class="card-body">
                                <form class="form form-horizontal invoice-repeater" action="{{route('invoices.update',$invoice->id)}}" method="post">
                                    @csrf
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="first-name">رقم الفاتوره</label>
                                                    <input type="number" id="first-name" class="form-control" value="{{$invoice->number}}" name="number" placeholder="رقم الفاتوره" />
                                                    @if($errors->has('number'))
                                                        <span class="text-danger">{{ $errors->first('number') }}</span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="first-name">تاريخ الفاتوره</label>
                                                    <input type="date" id="first-name" class="form-control" value="{{$invoice->date}}" name="date" placeholder="تاريخ الفاتوره" />
                                                    @if($errors->has('date'))
                                                        <span class="text-danger">{{ $errors->first('date') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="first-name">الوقت</label>
                                                    <input type="time" id="first-name" class="form-control" value="{{$invoice->time}}" name="time" placeholder="الوقت" />
                                                    @if($errors->has('time'))
                                                        <span class="text-danger">{{ $errors->first('time') }}</span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="first-name">الخصم</label>
                                                    <input type="number" id="first-name" class="form-control" value="{{$invoice->discount}}" name="discount" placeholder="الخصم" />
                                                    @if($errors->has('discount'))
                                                        <span class="text-danger">{{ $errors->first('discount') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="first-name">المبلغ قبل الخصم</label>
                                                    <input type="number" id="first-name" class="form-control" value="{{$invoice->total_before_dis}}" name="total_before_dis" placeholder="المبلغ قبل الخصم" />
                                                    @if($errors->has('total_before_dis'))
                                                        <span class="text-danger">{{ $errors->first('total_before_dis') }}</span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="first-name">المبلغ الكلي</label>
                                                    <input type="number" id="first-name" class="form-control" value="{{$invoice->total}}" name="total" placeholder="المبلغ الكلي" />
                                                    @if($errors->has('total'))
                                                        <span class="text-danger">{{ $errors->first('total') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" data-select2-id="46">
                                            <label class="form-label" for="select2-basic">اختار التاجر</label>
                                            <select class="select2 form-select" id="select2-basic" name="supplier_id">
                                                <option></option>
                                                @forelse($suppliers as $supplier)
                                                    <option value="{{$supplier->id}}" {{$invoice->supplier_id == $supplier->id ? 'selected' :" "}}>{{$supplier->name}}</option>
                                                @empty
                                                    <option> لايوجد بيانات</option>
                                                @endforelse
                                            </select>
                                            @if($errors->has('supplier_id'))
                                                <span class="text-danger">{{ $errors->first('supplier_id') }}</span>
                                            @endif
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="first-name">المبلغ المدفوع</label>
                                                    <input type="number" id="first-name" class="form-control" value="{{$invoice->paid}}" name="paid" placeholder="المبلغ المدفوع" />
                                                    @if($errors->has('paid'))
                                                        <span class="text-danger">{{ $errors->first('paid') }}</span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="first-name">المبلغ المتبقي</label>
                                                    <input type="number" id="first-name" class="form-control" value="{{$invoice->residual}}" name="residual" placeholder="المبلغ المتبقي" />
                                                    @if($errors->has('residual'))
                                                        <span class="text-danger">{{ $errors->first('residual') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div data-repeater-list="invoice">
                                        <div data-repeater-item>
                                            @foreach($invoice->purchases as $purchase)
                                            <div class="row d-flex align-items-end">
                                                <div class="col-md-2 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="itemname">إختار إسم الصنف</label>
                                                        {{--<select class="form-control" id="itemname" aria-describedby="itemname" name="item_id[]">--}}
                                                        <select class="form-control" id="itemname" aria-describedby="itemname" name="item_id[]">
                                                            @foreach($items as $item)
                                                                <option value="{{$item->id}}" {{$purchase->item_id == $item->id ? 'selected' : " "}}>{{$item->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        @if($errors->has('item_id[]'))
                                                            <span class="text-danger">{{ $errors->first('item_id[]') }}</span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-md-2 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="itemcost">إختار الوحده</label>
                                                        <select class="form-control" id="itemcost" aria-describedby="itemcost" name="unit_id[]">
                                                            @foreach($units as $unit)
                                                                <option value="{{$unit->id}}" {{$purchase->unit_id == $unit->id ? 'selected' : " "}}>{{$unit->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        @if($errors->has('unit_id[]'))
                                                            <span class="text-danger">{{ $errors->first('unit_id[]')}}</span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-md-2 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="itemquantity">الكميه</label>
                                                        <input
                                                                type="number"
                                                                class="form-control"
                                                                id="itemquantity"
                                                                aria-describedby="itemquantity"
                                                                placeholder="1"
                                                                name="qty[]"
                                                                value="{{$purchase->qty}}"
                                                        />
                                                        @if($errors->has('qty[]'))
                                                            <span class="text-danger">{{ $errors->first('qty[]') }}</span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-md-2 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="staticprice">السعر</label>
                                                        <input type="text" name="price[]" class="form-control" id="staticprice" aria-describedby="staticprice" placeholder="السعر"  value="{{$purchase->price}}"/>
                                                        @if($errors->has('price[]'))
                                                            <span class="text-danger">{{ $errors->first('price[]') }}</span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-md-2 col-12">
                                                    <div class="mb-1 mt-2">
                                                        <label class="form-label" for="exampleFormControlTextarea1">ملاحظه</label>
                                                        <textarea class="form-control" name="notes[]" id="exampleFormControlTextarea1" placeholder="ملاحظه">{{$purchase->notes}}</textarea>
                                                        @if($errors->has('notes[]'))
                                                            <span class="text-danger">{{ $errors->first('notes[]') }}</span>
                                                        @endif
                                                    </div>
                                                </div>

                                                {{--<div class="col-md-2 col-12 mb-50">--}}
                                                    {{--<div class="mb-1">--}}
                                                        {{--<button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete type="button">--}}
                                                            {{--<i data-feather="x" class="me-25"></i>--}}
                                                            {{--<span>حذف</span>--}}
                                                        {{--</button>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            </div>
                                            <hr />
                                            @endforeach
                                        </div>
                                    </div>
                                    {{--<div class="row">--}}
                                        {{--<div class="col-12">--}}
                                            {{--<button class="btn btn-icon btn-primary" type="button" data-repeater-create>--}}
                                                {{--<i data-feather="plus" class="me-25"></i>--}}
                                                {{--<span>اضافه جديده</span>--}}
                                            {{--</button>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    <div class="col-sm-9 offset-sm-3">
                                        <button type="submit" class="btn btn-primary me-1" >حفظ</button>
                                        <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /Invoice repeater -->
                </div>
            </section>

        </div>
    </div>
@stop

@section('scripts')
    <script>

        $("#itemname").change(function (e) {
            e.preventDefault();
            var itme_id = $("#itemname").val();
            // console.log(itme_id);
            if(itme_id){
                $.ajax({
                    url: 'item/'+itme_id+'',
                    type:'get',
                    success:function (data) {
                        // console.log(data);
                        if(data.status === 1){
                            document.getElementById("itemcost").value = data.data.unit_id;
                            // document.getElementById("staticprice").value = data.data.price;
                        }
                    },
                    error: function (jqXhr, textStatus, errorMessage) { // error callback
                        alert(errorMessage);
                    }
                });
            }
            else{
                $("#itemcost").empty();
                // $("#staticprice").empty();
            }
        });
    </script>
@endsection