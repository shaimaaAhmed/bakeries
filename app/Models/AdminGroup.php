<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminGroup extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function getPermissionsArrAttribute()
    {
        return json_decode($this->permissions);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
