@extends('dashboard.layouts.master')

@section('title', 'إضافه مندوب جديد')

@section('style')

    <link rel="stylesheet" type="text/css" href="{{asset('dashboard/app-assets/vendors/css/forms/select/select2.min.css')}}">

@endsection

@section('content')

    <div class="content-wrapper container-xxl p-0 " style="margin: -15px">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">إداره المخابز</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('index')}}">الرئسيه</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('delegates.index')}}">المندوبين</a>
                                </li>
                                <li class="breadcrumb-item active"><a href="{{route('delegates.create')}}">إضافه مندوب جديده</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                <div class="mb-1 breadcrumb-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                        <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-delegate" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-delegate" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-delegate" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-delegate" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body"><!-- Basic Horizontal form layout section start -->
            <section id="basic-horizontal-layouts">
                <div class="row">
                    <div class="col-md-12 col-12">
                        <div class="card">
                            @include('dashboard.includes.errors')
                            <div class="card-header">
                                <h4 class="card-title">إضافه مندوب جديد</h4>
                            </div>
                            <div class="card-body">
                                <form class="form form-horizontal" action="{{route('delegates.store')}}" method="post">
                                   @csrf
                                    <div class="row">
                                        <div class="col-12">
                                      <div class="mb-1 row">
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="first-name">اسم المندوب</label>
                                                    <input type="text" id="first-name" class="form-control" value="{{old('name')}}" name="name" placeholder="اسم المندوب" />
                                                    @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong style="color: red;">{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                               <div class="col-sm-6">
                                                        <label class="col-form-label" for="first-name">رقم المندوب</label>
                                                        <input type="number" id="first-name" class="form-control" value="{{old('phone')}}" name="phone" placeholder="رقم المندوب" />
                                                        @error('phone')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong style="color: red;">{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                        </div>
                                     </div>
                                        <div class="col-md-12" data-select2-id="46" style="margin: 0px !important;">
                                            <label class="form-label" for="select2-basic">اختار التاجر</label>
                                            <select class="select2 form-select" id="select2-basic" name="supplier_id">
                                               <option></option>
                                                @forelse($suppliers as $supplier)
                                                    <option value="{{$supplier->id}}">{{$supplier->name}}</option>
                                                @empty
                                                    <option> لايوجد بيانات</option>
                                                @endforelse
                                            </select>
                                            @error('supplier_id')
                                            <span class="invalid-feedback" role="alert">
                                                            <strong style="color: red;">{{ $message }}</strong>
                                                        </span>
                                            @enderror
                                        </div>
                                        <div class="col-12">
                                        <div class="row">
                                            <div class="mb-1">
                                            <label class="col-form-label" >مواعيد الزياره</label>
                                            <div class="form-check form-check-inline">
                                                <div class="col-sm-1">
                                                    <input type="checkbox" class="form-check-input" value="السبت" id="customCheck1" name="visiting[]" @if(old('visiting[]')) checked @endif/>
                                                    <label class="form-check-label" for="customCheck1">السبت</label>
                                                </div>
                                                <div class="col-sm-1">
                                                    <input type="checkbox" value="الأحد" class="form-check-input" id="customCheck6" name="visiting[]" @if(old('visiting[]')) checked @endif />
                                                    <label class="form-check-label" for="customCheck6">الأحد</label>
                                                 </div>
                                                 <div class="col-sm-2">
                                                    <input type="checkbox" value="الإثنين" class="form-check-input" id="customCheck5" name="visiting[]" @if(old('visiting[]')) checked @endif />
                                                    <label class="form-check-label" for="customCheck5">الإثنين</label>
                                                 </div>
                                                <div class="col-sm-2">
                                                    <input type="checkbox" value="الثلاثاء" class="form-check-input" id="customCheck14" name="visiting[]" @if(old('visiting[]')) checked @endif/>
                                                    <label class="form-check-label" for="customCheck4">الثلاثاء</label>
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="checkbox" value="الأربعاء" class="form-check-input" id="customCheck3" name="visiting[]" @if(old('visiting[]')) checked @endif/>
                                                    <label class="form-check-label" for="customCheck3">الأربعاء</label>
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="checkbox" class="form-check-input" id="customCheck2" value="الخميس" name="visiting[]" @if(old('visiting[]')) checked @endif/>
                                                    <label class="form-check-label" for="customCheck2">الخميس</label>
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="checkbox" value="الجمعه" class="form-check-input" id="customCheck" name="visiting[]" @if(old('visiting[]')) checked @endif/>
                                                    <label class="form-check-label" for="customCheck">الجمعه</label>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="mb-1 col-sm-12">
                                                    <label class="form-label" for="exampleFormControlTextarea1">ملاحظه</label>
                                                    <textarea class="form-control" name="notes" id="exampleFormControlTextarea1" rows="3" placeholder="ملاحظه">{{old('notes')}}</textarea>
                                                    @error('notes')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong style="color: red;">{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-9 offset-sm-3">
                                            <button type="submit" class="btn btn-primary me-1" >حفظ</button>
                                            <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Basic Horizontal form layout section end -->
        </div>
    </div>

@stop
@section('script')
    <script src="{{asset('dashboard/app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>

    <script src="{{asset('dashboard/app-assets/js/scripts/forms/form-select2.min.js')}}"></script>
    @endsection