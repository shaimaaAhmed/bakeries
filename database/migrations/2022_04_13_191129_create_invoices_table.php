<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->integer('number')->unique();
            $table->time('time');
            $table->date('date');
            $table->double('total_before_dis')->nullable();
            $table->double('discount');
            $table->double('total')->nullable();
            $table->double('paid')->nullable();
            $table->double('residual')->nullable();
            $table->enum('status',['paid','deferred','partial']); //دفع -آجل -دفع جزئي
            $table->enum('type',['buying','vibrant','mortal'])->default('buying'); //  مرتجع - شراء- mortal=هالك
            $table->text('notes')->nullable();
            $table->unsignedBigInteger('delegate_id');
            $table->foreign('delegate_id')
                ->references('id')->on('delegates')->onDelete('cascade');
            $table->foreignId('supplier_id')
                ->constrained('suppliers')->onDelete('cascade');
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
