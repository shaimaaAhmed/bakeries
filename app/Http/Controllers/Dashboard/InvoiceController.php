<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\InvoiceRequest;
use App\Models\Delegate;
use App\Models\Invoice;
use App\Models\Item;
use App\Models\Purchase;
use App\Models\Stock;
use App\Models\Supplier;
use App\Models\Unit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InvoiceController extends Controller
{
    public function index()
    {
        $invoices = Invoice::latest()->get();
        return view('dashboard.Invoices.index',compact('invoices'));
    }

    public function create()
    {
        $items = Item::get();
        $units = Unit::get();
        $suppliers = Supplier::get();
//        return view('dashboard.Invoices.create1',compact('items','units','suppliers'));
        return view('dashboard.Invoices.create-new',compact('items','units','suppliers'));

    }

    public function store(InvoiceRequest $request)
    {
//   DB::beginTransaction();
//dd($request->all());
        try {
            $status = "" ;
           $sub = $request->total - $request->paid;
            if ($sub == 0){
                $status = "paid";
            }elseif(($request->paid < $request->total) && $request->paid != 0 ){
                $status = "partial";
            }else{
                $status = "deferred";
            }
        $user = auth()->user()->id;
        $invoice = Invoice::create([
            'user_id' => $user,
            'number' => $request->number,
            'discount' => $request->discount, // on total finish
            'supplier_id' => $request->supplier_id,
            'delegate_id'  => $request->delegate_id,
            'total_before_dis' => $request->total_before_dis,
            'total' => $request->total,
            'paid' => $request->paid,
            'residual' => $request->residual,
            'type' => $request->type,
            'status' => $status,
            'notes' => $request->notes,
            'time' => Carbon::now()->format('H:i'),
            'date' => date("Y-m-d", strtotime(Carbon::now()->format('Y-m-d'))),
//            'time' => date("H:i:s", strtotime($request->time)),

        ]);

        if ($invoice){

    //// create Purchase
            $data = $request->except(['user_id','supplier_id','delegate_id']);
            $subTotal = 0;
            foreach ($data['invoice'] as $key => $val){
                if(!empty($val)){
                    if($key < count($data['invoice'])-1)
                    {
                        $itemId =$data['invoice'][$key]['item_id'];
                        $price =$data['invoice'][$key]['price'];
                        $priceUnit =$data['invoice'][$key]['price_unit'];
                        $item = Item::where('id',$itemId)->first();
                        $item->update([
                            'price' => $price,
                            'price_unit' => $priceUnit,
                        ]);
                     $subTotalold = ($price) * ($data['invoice'][$key]['qty']);
                     $subTotal += $subTotalold ;

                        $size =$data['invoice'][$key]['size'];
                        $count_item =$data['invoice'][$key]['count_item'];
                        $typeUnit =$data['invoice'][$key]['type_unit'];
                        $wight = 0 ;
                        if ($typeUnit == 'كيلوا' || $typeUnit == 'لتر' || $typeUnit == 'كيلو'){
                            $wight = $size*1000*$count_item;
                        }else{
                            $wight = $size*$count_item;
                        }


                     $attribute = new Purchase;
                     $attribute->invoice_id = $invoice->id;
                     $attribute->item_id = $itemId;
                     $attribute->unit_id = $data['invoice'][$key]['unit_id'];
                     $attribute->qty = $data['invoice'][$key]['qty'] == null ? 1 : $data['invoice'][$key]['qty'];
                     $attribute->price = $price;
                     $attribute->date = date("Y-m-d", strtotime(Carbon::now()->format('Y-m-d')));
                     $attribute->count_item = $data['invoice'][$key]['count_item'];
                     $attribute->price_unit = $priceUnit;
//                     $attribute->notes = $data['invoice'][$key]['notes'];
                     $attribute->total = $subTotalold;
                        $attribute->weight = $size;
                        $attribute->weight_total = $wight;
                     $attribute->save();

        /////////// create stock
                      $type = $request->type;
                        $item =$data['invoice'][$key]['item_id'];
                        $min_stock =$data['invoice'][$key]['min_stock'];

                        $stock = Stock::where('item_id',$item)->first();
                        $qunty = $data['invoice'][$key]['qty']== null ? 1 : $data['invoice'][$key]['qty'];

                        $priceTotal = $data['invoice'][$key]['price']*$qunty;
                     if($type == 'buying'){
                        if($stock){
                           $qty = $stock->qty;
                            $count = $stock->count_item + $count_item;
                               $subQty = $qty + $qunty;
                            $status = "";
                            if($subQty > $min_stock || $subQty == $min_stock){$status ='available';}elseif($subQty == 0) {$status ='not_available';} else $status ='weak';
                            $stock->update([
                                'qty' => $subQty,
                                'wight' =>$stock->wight + $wight,
                                'price' => $stock->price + $priceTotal,
                                'count_item' => $count ,
                                'status' => $status ,
//                                'status' => $count > $min_stock || $count == $min_stock ? 'available' : $count == 0? 'not_available' :'weak' ,

                            ]);
                        }else{
                            $type_wight = '';
                            if($typeUnit == 'لتر'){$type_wight ='ملي';}elseif ($typeUnit == 'كيلوا' || $typeUnit == 'كيلو'){$type_wight ='جرام';} else{$type_wight =$typeUnit;};
                            Stock::create([
                                'item_id' => $item,
                                'qty' => $qunty,
                                'price' => $priceTotal,
                                'count_item' =>$count_item,
                                'wight' =>$wight,
                                'status' => $qunty < $min_stock ? 'weak' :'available',
                                'type_wight'  => $type_wight,
                                'category' => $data['invoice'][$key]['category'],
                                'company' => $data['invoice'][$key]['company'],
                                'unit_id' => $data['invoice'][$key]['unit_id'],
                                'min_stock' => $min_stock

                            ]);
                        }
                     }
                     else{
                         $sub =  $stock->count_item - $count_item;
                         $qty = $stock->qty;
                         $suQty =$qty - $qunty;
                         $status ="";
                         if($suQty > $min_stock || $suQty == $min_stock) $status ='available'; elseif ($suQty == 0) $status = 'not_available'; else  $status ='weak';
                         if($stock){

                             $stock->update([
                                 'qty' => $suQty,
                                 'wight' => $stock->wight - $wight,
                                 'price' => $stock->price - $priceTotal,
                                 'count_item' => $sub,
                                 'status' => $status ,
                             ]);
                         }
                     }
                 }else{

                        $itemId =$data['invoice'][$key]['item_id['];
                        $price =$data['invoice'][$key]['price['];
                        $priceUnit =$data['invoice'][$key]['price_unit'];
                        $item = Item::where('id',$itemId)->first();
                        $item->update([
                            'price' => $price,
                            'price_unit' => $priceUnit,
                        ]);
                    $subTotalold = ($price) * ($data['invoice'][$key]['qty[']);
                        $subTotal += $subTotalold ;
                        $wight = 0.0 ;
                        $size =$data['invoice'][$key]['size'];
                        $count_item =$data['invoice'][$key]['count_item'];
                        $typeUnit =$data['invoice'][$key]['type_unit'];

                        if ($typeUnit == 'كيلوا' || $typeUnit == 'لتر' || $typeUnit == 'كيلو'){
                            $wight = ($size*1000)* $count_item;
                        }else{
                            $wight = $size*$count_item;
                        }
                    $attribute = new Purchase;
                    $attribute->invoice_id = $invoice->id;
                    $attribute->item_id = $itemId;
                    $attribute->unit_id = $data['invoice'][$key]['unit_id['];
                    $attribute->qty = $data['invoice'][$key]['qty[']== null ? 1 : $data['invoice'][$key]['qty['];
                    $attribute->price =$data['invoice'][$key]['price['];
                    $attribute->date = date("Y-m-d", strtotime(Carbon::now()->format('Y-m-d')));
                    $attribute->count_item = $data['invoice'][$key]['count_item'];
                    $attribute->price_unit = $data['invoice'][$key]['price_unit'];
                    $attribute->total = $subTotalold;
                    $attribute->weight = $size;
                    $attribute->weight_total = $wight;
                    $attribute->save();
                 /////stoke create
                        $type = $request->type;

                        $item =$data['invoice'][$key]['item_id['];
                        $min_stock =$data['invoice'][$key]['min_stock'];


                        $stock = Stock::where('item_id',$item)->first();
                        $qunty = $data['invoice'][$key]['qty[']== null ? 1 : $data['invoice'][$key]['qty['];
                        $priceTotal = $data['invoice'][$key]['price[']*$qunty;

                        if($type == 'buying'){

                            if($stock){
                                $qty = $stock->qty;
                                $subQty = $qty + $qunty;
                                $count = $stock->count_item + $count_item;
                                $staut = "";
                                   if($subQty > $min_stock || $subQty == $min_stock) $staut= 'available'; elseif ( $subQty == 0) $staut='not_available'; else  $staut ='weak';
                                $stock->update([
                                    'qty' => $subQty,
                                    'wight' =>$stock->wight + $wight,
//                                    'type_wight'  => $data['invoice'][$key]['type_unit'],
                                    'price' => $stock->price + $priceTotal,
                                    'count_item' => $count ,
                                    'status' => $staut ,
//                                    'category_id' => $data['invoice'][$key]['category_id['],
//                                    'unit_id' => $data['invoice'][$key]['unit_id[']
                                ]);
                            }else{
                                $type_wight = '';
                                if($typeUnit == 'لتر'){$type_wight ='ملي';}elseif ($typeUnit == 'كيلوا' || $typeUnit == 'كيلو'){$type_wight ='جرام';} else{$type_wight =$typeUnit;};
//                                $status = "";

                                Stock::create([
                                    'item_id' => $item,
                                    'qty' => $qunty,
                                    'price' => $priceTotal,
                                    'count_item' =>$count_item,
                                    'wight' =>$wight,
                                    'min_stock' => $min_stock,
                                    'status' => $qunty < $min_stock ? 'weak' :'available',
                                    'type_wight'  => $type_wight,
                                    'category' => $data['invoice'][$key]['category'],
                                    'company' => $data['invoice'][$key]['company'],
                                    'unit_id' => $data['invoice'][$key]['unit_id[']
                                ]);
                            }

                        } else{
                            $sub =  $stock->count_item - $count_item;
                            $qty = $stock->qty;
                            $suQty =$qty - $qunty;
                            $status ="";
                            if($suQty > $min_stock || $suQty == $min_stock) $status ='available'; elseif ($suQty == 0) $status = 'not_available'; else  $status ='weak';
                            if($stock){

                                $stock->update([
                                    'qty' => $suQty,
                                    'wight' => $stock->wight - $wight,
                                    'price' => $stock->price - $priceTotal,
                                    'count_item' => $sub,
                                    'status' => $status ,
                                ]);
                            }
                        }
                        }
                }
                }
            }else{
            return back()->with('error', 'حدث خطأ عند إضافه فاتوره مشتريات ');
        }

            $invoice->save();
            /////stoke create
            return redirect()->route('invoices.index')->with('message', 'تم إضافه فاتوره مشتريات بنجاح');


//            DB::commit();
            // all good

        } catch (\Exception $e) {
            return $e;
//            DB::rollback();
            return back()->with('error', 'حدث خطأ عند إضافه فاتوره مشتريات ');
        }

    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $invoice = Invoice::whereId($id)->first();
        $items = Item::get();
        $units = Unit::get();
        $suppliers = Supplier::get();
        if ($invoice){
            return view('dashboard.Invoices.edit',compact('invoice','items','units','suppliers'));
        }else{
            return back()->with('error', 'لا يوجد بيانات');

        }
    }


    public function update(InvoiceRequest $request, $id)
    {
        dd($request->all());
        $invoice = Invoice::whereId($id)->first();
        if ($invoice){
            $updateInvoice = $request->except(['idAttr','invoice']);
            $invoice->update($updateInvoice);
            $invoice->save;
            $data = $request->except(['number','total','paid',
                'residual','status','type','notes']);
            $subTotal = 0;
            foreach($data['idAttr'] as $key=>$attr) {
//                dd($data['invoice'][$key]['item_id[']);
               $purchase = Purchase::where(['item_id' => $data['invoice'][$key]['item_id[']])->
                where('invoice_id',$invoice->id)->first();
                /////stoke update
                $type = $request->type;

                $item =$data['invoice'][$key]['item_id['];
                $size =$data['invoice'][$key]['size['];
                $count_item =$data['invoice'][$key]['count_item['];
                $typeUnit =$data['invoice'][$key]['type_unit['];
                $min_stock =$data['invoice'][$key]['min_stock['];
                $wight = 0.0 ;
                if ($typeUnit == 'كيلوا' || $typeUnit == 'لتر' || $typeUnit == 'كيلو'){
                    $wight = ($size*1000)* $count_item;
                }else{
                    $wight = $size*$count_item;
                }
                $stock = Stock::where('item_id',$item)->first();
                $qunty = $data['invoice'][$key]['qty[']== null ? 1 : $data['invoice'][$key]['qty['];
                $priceTotal = $data['invoice'][$key]['price[']*$qunty;

                if($type == 'buying'){

                    if($stock){
                        $qty = $stock->qty;
                        $subQty = $qty + $qunty;
                        $count = $stock->count_item + $count_item;
                        $staut = "";
                        if($subQty > $min_stock || $subQty == $min_stock) $staut= 'available'; elseif ( $subQty == 0) $staut='not_available'; else  $staut ='weak';
                        $stock->update([
                            'qty' => $subQty,
                            'wight' =>$stock->wight + $wight,
//                                    'type_wight'  => $data['invoice'][$key]['type_unit'],
                            'price' => $stock->price + $priceTotal,
                            'count_item' => $count ,
                            'status' => $staut ,
                        ]);
                    }
                } else{
                    $sub =  $stock->count_item - $count_item;
                    $qty = $stock->qty;
                    $suQty =$qty - $qunty;
                    $status ="";
                    if($suQty > $min_stock || $suQty == $min_stock) $status ='available'; elseif ($suQty == 0) $status = 'not_available'; else  $status ='weak';
                   // notivcation main stock
                    if($stock){

                        $stock->update([
                            'qty' => $suQty,
                            'wight' => $stock->wight - $wight,
                            'price' => $stock->price - $priceTotal,
                            'count_item' => $sub,
                            'status' => $status ,
                        ]);
                    }
                }

                $subTotalold = ($data['invoice'][$key]['price[']) * ($data['invoice'][$key]['qty[']);
                $subTotal += $subTotalold;
                Purchase::where(['id' => $data['idAttr'][$key]])->update([
                    'invoice_id' => $invoice->id,
                    'item_id' => $data['invoice'][$key]['item_id['],
//                $attribute->unit_id = $data['invoice'][$key]['unit_id['];
                    'qty' => $data['invoice'][$key]['qty['] == null ? 1 : $data['invoice'][$key]['qty['],
                    'price' => $data['invoice'][$key]['price['],
                    'count_item' => $data['invoice'][$key]['count_item['],
                    'price_unit' => $data['invoice'][$key]['price_unit['],
                    'total' => $subTotalold,
                ]);
            }
            } else{
                return back()->with('error', 'حدث خطأ عند إضافه فاتوره مشتريات ');
        }
        return redirect()->route('invoices.index')->with('message', 'تم تعديل فاتوره مشتريات بنجاح');
    }


    public function destroy($id)
    {
        $invoice = Invoice::whereId($id)->first();
        if ($invoice){
            $invoice->delete();
            return redirect()->route('invoices.index')->with('message', 'تم حذف فاتوره مشتريات بنجاح');
        }else{
            return back()->with('error', 'لا يوجد بيانات');

        }
    }


    public function item($id){
        $data = Item::where('id', $id)->with('company','category')->first();
        return response()->json(['status' => 1, 'message' => 'success', 'data' => $data]);
    }

    public function delegate($id){
        $data = Delegate::where('supplier_id', $id)->get();
//        return response()->json($data);
            return response()->json(['status' => 1, 'message' => 'success', 'data' => $data]);
    }

    public function edit1($id)
    {
        $invoice = Invoice::findOrFail($id);
        $invoiceServices = InvoiceService::where('invoice_id',$id)->get();
        $subTotal = 0;
        $discount = 0;
        $total = 0 ;
        foreach ($invoiceServices as $invoiceService){
            $subTotalold = $invoiceService->price*$invoiceService->qty;
            $discountold = $subTotalold*($invoiceService->discount/100);
            $totalold =$subTotalold-$discountold;
            $subTotal += $subTotalold ;
            $discount += $discountold;
            $total += $totalold;
        }
        $clients = Client::select('id', 'name')->get();
        $services = Service::where('status','Enable')->select('id', 'name')->get();
        return view("backend.invoices.edit",compact('invoice','clients',
            'services','invoiceServices','total','subTotal','discount'));

    }


    public function update1(Request $request, $id)
    {
        $user = auth()->user()->id;
//        $invoiceServices = InvoiceService::where('invoice_id',$id)->get();
        $invoice = Invoice::findOrFail($id);
        $validator = $this->validate($request, [
            'client_id'    => 'required',
            'status'    => 'required',
            'date'    => 'required|date',
            'due_date'    => 'required|date',
            'notes'    => 'sometimes',
//            'service_id'    => 'required',
//            'qty'    => 'required',
//            'price'    => 'required',
//            'discount'    => 'required',
            'notes1'    => 'sometimes',
        ]);
        $invoice->update([
            'client_id' => $request->client_id,
            'user_id' => $user,
            'status' => $request->status,
            'date' => $request->date,
            'due_date' => $request->due_date,
            'notes' => $request->notes,
        ]);

        if ($invoice){

            $data = $request->except(['client_id','status','date',
                'due_date','notes']);
//        dd($data);
            foreach($data['idAttr'] as $key=>$attr){
                $subTotal = $data['price'][$key]*$data['qty'][$key];
                $discount = $subTotal*($data['discount'][$key]/100);
                $total =$subTotal-$discount;
                InvoiceService::where(['id'=>$data['idAttr'][$key]])->update([
                    'service_id' => $data['service_id'][$key],
                    'qty' => $data['qty'][$key],
                    'price'=>$data['price'][$key],
                    'discount' => $data['discount'][$key],
                    'total' => $total,
                    'notes' =>$data['notes1'][$key],

                ]);
            }
            return redirect()->route('invoices.index')->with([
                'message' => 'Invoice updated successfully',
                'alert-type' => 'success'
            ]);
        }else{
            return back()->with([
                'message' => 'You have not permission to continue this process',
                'alert-type' => 'danger'
            ]);
        }
    }
}
