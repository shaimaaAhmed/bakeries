<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Chef;
use App\Models\Order;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderController extends Controller
{
//
//    public function orderRequest(){
//        $products = Product::get();
//        return view('dashboard.Orders.order-request',compact('products'));
//    }

    public function index()
    {
        $orders = Order::latest()->get();
        return view('dashboard.Orders.index',compact('orders'));
    }


    public function create()
    {
        $products = Product::get();
        $chefs = Chef::get();
        return view('dashboard.Orders.create',compact('products','chefs'));
    }

    public function store(Request $request)
    {
        $user = auth()->id();
        try{
        $data = $request->all();
        foreach ($data['invoice'] as $key => $val){
            if(!empty($val)){

                $discount = $data['invoice'][$key]['discount'];
                if($key < count($data['invoice'])-1)
                {
                    $price =$data['invoice'][$key]['price'];
                    $qty = $data['invoice'][$key]['qty'] == null ? 1 : $data['invoice'][$key]['qty'];
                    $subtotal = $price * $qty;
                    $total =$subtotal- $discount;

                    $attribute = new Order;
                    $attribute->date = date("Y-m-d", strtotime(Carbon::now()->format('Y-m-d')));
                    $attribute->time = Carbon::now()->format('H:i');
                    $attribute->chef_id = $request->chef_id;
                    $attribute->user_id = $user;
                    $attribute->status ='جاري الانتظار';

                    $attribute->product_id = $data['invoice'][$key]['product_id'];
                    $attribute->qty = $qty ;
                    $attribute->price = $price;
                    $attribute->discount = $discount;
                    $attribute->total = $total;
                    $attribute->save();
                }else{
                    $price =$data['invoice'][$key]['price['];
                    $qty = $data['invoice'][$key]['qty['] == null ? 1 : $data['invoice'][$key]['qty['];
                    $subtotal = $price * $qty;
                    $total =$subtotal- $discount;

                    $attribute = new Order;
                    $attribute->date = date("Y-m-d", strtotime(Carbon::now()->format('Y-m-d')));
                    $attribute->time = Carbon::now()->format('H:i');
                    $attribute->chef_id = $request->chef_id;
                    $attribute->user_id = $user;
                    $attribute->status ='جاري الانتظار';

                    $attribute->product_id = $data['invoice'][$key]['product_id['];
                    $attribute->qty =$qty;
                    $attribute->price =$price;
                    $attribute->discount =$discount;
                    $attribute->total =$total;
                    $attribute->save();
                }

            }
        }
        return redirect()->route('orders.index')->with('success', 'تم اضافه المنتج بنجاح');

        }
        catch (\Exception $er){
            return $er;
            return redirect()->route('orders.index')->with('error', 'حدث خطأ يرجي المحاوله مره اخري');
        }
    }


    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $date =date("Y-m-d", strtotime(Carbon::now()->format('Y-m-d')));
        $orders = Order::whereDate($date)->get();
        $order = Order::whereId($id)->first();
        $products = Product::get();
        $chefs = Chef::get();
        return view('dashboard.Orders.edit',compact('products','chefs','order'));

    }


    public function update(Request $request, $id)
    {
        dd($request->all());
    }

    public function product($id){
        $data = Product::where('id', $id)->first();
        return response()->json(['status' => 1, 'message' => 'success', 'data' => $data]);
    }

    public function destroy($id)
    {
        $order = Order::whereId($id)->first();
        if ($order){
            $order->delete();
            return redirect()->route('orders.index')->with('message', 'تم حذف الطلب بنجاح');
        }else{
            return back()->with('error', 'لا يوجد بيانات');

        }
    }
}
