<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\SupplierRequest;
use App\Models\Company;
use App\Models\Supplier;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    public function index()
    {
        $suppliers = Supplier::latest()->get();
        return view('dashboard.Suppliers.index',compact('suppliers'));
    }

    public function create()
    {
        $companies = Company::get();
        return view('dashboard.Suppliers.create',compact('companies'));
    }

    public function store(SupplierRequest $request)
    {
        Supplier::create($request->validated());
        return redirect()->route('suppliers.index')->with('message', 'تم إضافه التاجر بنجاح');

    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $companies = Company::get();
        $supplier = Supplier::whereId($id)->first();
        if ($supplier){
            return view('dashboard.Suppliers.edit',compact('supplier','companies'));
        }else{
            return back()->with('error', 'لا يوجد بيانات');

        }
    }


    public function update(SupplierRequest $request, $id)
    {
        $supplier = Supplier::whereId($id)->first();
        if ($supplier){
            $supplier->update($request->validated());
            return redirect()->route('suppliers.index')->with('message', 'تم تعديل التاجر بنجاح');

        }else{
            return back()->with('error', 'لا يوجد بيانات');

        }
    }


    public function destroy($id)
    {
        $supplier = Supplier::whereId($id)->first();
        if ($supplier){
            $supplier->delete();
            return redirect()->route('suppliers.index')->with('message', 'تم حذف التاجر بنجاح');
        }else{
            return back()->with('error', 'لا يوجد بيانات');

        }
    }
}
