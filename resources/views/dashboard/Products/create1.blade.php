@extends('dashboard.layouts.master')

@section('title', 'إضافه منتج جديد')

@section('content')

    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">إداره المخابز</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('index')}}">الرئسيه</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('products.index')}}">المنتجات</a>
                                </li>
                                <li class="breadcrumb-item active"><a href="{{route('products.create')}}">إضافه منتج جديد</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                <div class="mb-1 breadcrumb-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                        <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body"><!-- Basic Horizontal form layout section start -->
            <section id="basic-horizontal-layouts">
                <div class="row">
                    <div class="col-md-12 col-12">
                        <div class="card">
                            @include('dashboard.includes.errors')
                            <div class="card-header">
                                <h4 class="card-title">إضافه منتج جديد</h4>
                            </div>
                            <div class="card-body">
                                <form class="form form-horizontal" action="{{route('products.store')}}" method="post" enctype="multipart/form-data">
                                   @csrf
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="col-sm-6">
                                            <label class="col-form-label" for="first-name">اسم المنتج</label>
                                            <input type="text" id="first-name" class="form-control" value="{{old('name')}}" name="name" placeholder="اسم المنتج" />
                                            @if($errors->has('name'))
                                                <span class="text-danger">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                                <div class="col-sm-6">
                                                <label class="col-form-label" for="first-name">كود المنتج</label>
                                                <input type="number" id="first-name" class="form-control" value="{{old('code')}}" name="code" placeholder="كود المنتج" />
                                                @if($errors->has('code'))
                                                    <span class="text-danger">{{ $errors->first('code') }}</span>
                                                @endif
                                            </div>
                                             </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="col-md-6 col-6">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="staticwright">وزن المنتج</label>
                                                        <input type="text" name="wright" class="form-control" id="staticwright" aria-describedby="staticwright" placeholder="وزن المنتج" step="any" />
                                                        @if($errors->has('wright'))
                                                            <span class="text-danger">{{ $errors->first('wright') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-6">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="staticsize">حجم المنتج</label>
                                                        <input type="text" name="size" class="form-control" id="staticsize" aria-describedby="staticsize" placeholder="حجم المنتج" step="any" />
                                                        @if($errors->has('size'))
                                                            <span class="text-danger">{{ $errors->first('size') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="col-md-6 col-6">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="staticnum_pieces">عدد القطع</label>
                                                        <input type="text" name="num_pieces" class="form-control" id="staticnum_pieces" aria-describedby="staticnum_pieces" placeholder="عدد القطع" step="any" />
                                                        @if($errors->has('num_pieces'))
                                                            <span class="text-danger">{{ $errors->first('num_pieces') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-6">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="staticunit_type">نوع الوحده</label>
                                                        <input type="text" name="unit_type" class="form-control" id="staticunit_type" aria-describedby="staticunit_type" placeholder="نوع الوحده" step="any" />
                                                        @if($errors->has('unit_type'))
                                                            <span class="text-danger">{{ $errors->first('unit_type') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="col-md-6 col-6">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="staticselling_price">سعر البيع</label>
                                                        <input type="text" name="selling_price" class="form-control" id="staticselling_price" aria-describedby="staticselling_price" placeholder="سعر البيع" step="any" />
                                                        @if($errors->has('selling_price'))
                                                            <span class="text-danger">{{ $errors->first('selling_price') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-6">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="staticwholesale_price">سعر الجمله</label>
                                                        <input type="text" name="wholesale_price" class="form-control" id="staticwholesale_price" aria-describedby="staticwholesale_price" placeholder="سعر الجمله" step="any" />
                                                        @if($errors->has('wholesale_price'))
                                                            <span class="text-danger">{{ $errors->first('wholesale_price') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="formFile">صوره المنتج</label>
                                                    <input type="file" id="formFile" class="form-control" value="{{old('image')}}" name="image" placeholder="صوره الشيف" />

                                                    @if($errors->has('image'))
                                                        <span class="text-danger">{{ $errors->first('image') }}</span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="form-label" for="exampleFormControlTextarea1">الوصف</label>
                                                    <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3" placeholder="الوصف">{{old('description')}}</textarea>

                                                    @if($errors->has('description'))
                                                        <span class="text-danger">{{ $errors->first('description') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                       
                                        <div class="col-sm-9 offset-sm-3">
                                            <button type="submit" class="btn btn-primary me-1" >حفظ</button>
                                            <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Basic Horizontal form layout section end -->
        </div>
    </div>

@stop