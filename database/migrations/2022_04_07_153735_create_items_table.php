<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('code')->unique();
            $table->string('wright');   // الوزن بالجرام او الملي  او الكيلوا او اللتر
            $table->string('type_unit');   // نوع الوحده كيلوا او لتر او جرام او ملي
//            $table->string('size')->nullable(); // الحجم بالجرام او الملي
            $table->double('price'); // سعر الوحده
            $table->double('count_item'); // عدد الوحده وليكن 12 زجاجه والوحده كرتونه
            $table->double('price_unit')->nullable(); // سعر الوحده الواحده
            $table->double('min_stock'); // الحد الادني للمخزون
            $table->text('description')->nullable();
            $table->text('notes')->nullable();
            $table->unsignedBigInteger('unit_id');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('category_id');
            $table->foreign('unit_id')
                ->references('id')->on('units')->onDelete('cascade');
            $table->foreign('company_id')
                ->references('id')->on('companies')->onDelete('cascade');
            $table->foreign('category_id')
                ->references('id')->on('categories')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
