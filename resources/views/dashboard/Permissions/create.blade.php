@extends('dashboard.layouts.master')

@section('title', trans('back.create-var',['var'=>trans('back.permission')]))


@section('content')


    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <i class="icon-arrow-right6 position-left"></i>
                    <span class="text-semibold">@lang('back.home')</span>
                    - @lang('back.create-var',['var'=>trans('back.permission')])
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ route('dashboard.index') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a>
                </li>
                <li><a href="{{ route('permissions.index') }}"><i
                            class="icon-admin position-left"></i> @lang('back.permissions')</a></li>
                <li class="active">@lang('back.create-var',['var'=>trans('back.permission')])</li>
            </ul>

            @include('dashboard.includes.quick-links')
        </div>
    </div>
    <!-- /page header -->


    @include('dashboard.includes.errors')


    <div class="row" style="margin: 15px;">
        <div class="col-md-12">

            <!-- Basic layout-->
            <form action="{{ route('permissions.store') }}" class="form-horizontal" method="post"
                  enctype="multipart/form-data">
                @csrf
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title"> اضافة صلاحية جديدة </h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>


                    <div class="panel-body">

                        <div class="form-group">
                            <label class="col-lg-3 control-label"> {{ trans('back.name_of_job') }} </label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="name"
                                       placeholder="{{ trans('back.name_of_job') }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label"> {{ trans('dash.desc') }} </label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="description"
                                       placeholder="{{ trans('dash.desc') }}" required>
                            </div>
                        </div>


                        @foreach(cruds() as $i => $crud)
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h5 class="panel-title">@lang('dash.'.$crud)</h5>
                                    <div class="heading-elements">
                                        <ul class="icons-list">
                                            <li><a data-action="collapse"></a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label class="control-label col-lg-3"> عرض </label>
                                                <div class="col-lg-9">
                                                    <div class="checkbox checkbox-switchery switchery-xs">
                                                        <label>
                                                            <input type="checkbox" name="perms[]" class="switchery"
                                                                   value="{{ ($crud) }}.index">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label class="control-label col-lg-3"> انشاء </label>
                                                <div class="col-lg-9">
                                                    <div class="checkbox checkbox-switchery switchery-xs">
                                                        <label>
                                                            {{--   <input type="hidden" name="perms[]" value="{{ $crud }}.store" >--}}
                                                            <input type="checkbox" name="perms[]" class="switchery"
                                                                   value="{{ $crud }}.create">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label class="control-label col-lg-3"> حفظ الانشاء </label>
                                                <div class="col-lg-9">
                                                    <div class="checkbox checkbox-switchery switchery-xs">
                                                        <label>
                                                            <input type="checkbox" name="perms[]" class="switchery"
                                                                   value="{{ $crud }}.store">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label class="control-label col-lg-3"> تعديل </label>
                                                <div class="col-lg-9">
                                                    <div class="checkbox checkbox-switchery switchery-xs">
                                                        <label>
                                                            {{--                                                            <input type="hidden" name="perms[]" value="{{ $crud }}.update" >--}}
                                                            <input type="checkbox" name="perms[]" class="switchery"
                                                                   value="{{ $crud }}.edit">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label class="control-label col-lg-3"> حفظ التعديل </label>
                                                <div class="col-lg-9">
                                                    <div class="checkbox checkbox-switchery switchery-xs">
                                                        <label>
                                                            <input type="checkbox" name="perms[]" class="switchery"
                                                                   value="{{ $crud }}.update">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label class="control-label col-lg-3"> حذف </label>
                                                <div class="col-lg-9">
                                                    <div class="checkbox checkbox-switchery switchery-xs">
                                                        <label>
                                                            <input type="checkbox" name="perms[]" class="switchery"
{{--                                                                   value="{{ $crud }}.destroy">--}}
                                                                   value="ajax-delete-{{ str()->singular($crud) }}">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach


                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h5 class="panel-title">@lang('dash.settings')</h5>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label class="control-label col-lg-3"> عرض </label>
                                            <div class="col-lg-9">
                                                <div class="checkbox checkbox-switchery switchery-xs">
                                                    <label>
                                                        <input type="checkbox" name="perms[]" class="switchery"
                                                               value="settings.index">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label class="control-label col-lg-3"> تعديل </label>
                                            <div class="col-lg-9">
                                                <div class="checkbox checkbox-switchery switchery-xs">
                                                    <label>
                                                        <input type="checkbox" name="perms[]" class="switchery"
                                                               value="settings.updateAll">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="text-right" style="padding-bottom: 10px; padding-left: 10px;">
                        <input type="submit" class="btn btn-primary" name="forward"
                               value=" {{ trans('dash.added_and_forward_to_list') }} "/>
                        <input type="submit" class="btn btn-success" name="back"
                               value=" {{ trans('dash.added_and_come_back') }} "/>
                    </div>
                </div>


            </form>
        </div>
        </form>
        <!-- /basic layout -->

    </div>


    {{--<div class="col-md-6">--}}
    {{--<div class="panel panel-flat">--}}

    {{--<div class="panel-heading">--}}
    {{--<h5 class="panel-title"> {{ trans('back.last_admins') }} </h5>--}}
    {{--<div class="heading-elements">--}}
    {{--<ul class="icons-list">--}}
    {{--<li><a data-action="collapse"></a></li>--}}
    {{--<li><a data-action="reload"></a></li>--}}
    {{--<li><a data-action="close"></a></li>--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="panel-body">--}}

    {{--<table class="table table-bordered table-hover">--}}
    {{--<tr class="text-center">--}}
    {{--<th> {{ trans('dash.full_name') }} </th>--}}
    {{--<th> {{ trans('dash.mobile') }} </th>--}}
    {{--<th> {{ trans('dash.image') }} </th>--}}
    {{--</tr>--}}
    {{--@forelse($last_admins as $dashboard)--}}
    {{--<tr>--}}
    {{--<td> {{ $dashboard->full_name }} </td>--}}
    {{--<td> {{ $dashboard->phone }} </td>--}}
    {{--<td> <img height="80px" src="{{ $dashboard->imageurl }}" /> </td>--}}
    {{--</tr>--}}
    {{--@empty--}}
    {{--@endforelse--}}
    {{--</table>--}}
    {{--</div>--}}

    {{--</div>--}}
    {{--</div>--}}
    </div>



@stop


