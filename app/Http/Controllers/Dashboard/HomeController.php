<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class HomeController extends Controller
{
    public function home()
    {
//        $user =auth()->user();
        return view('dashboard.index');
    }

    public function viewLogin(){
        if (auth()->user()){
            return back();
        }else {
            return view('dashboard.auth.login');
        }
    }

    public function login(Request $request){
         $request->validate([
            'email'    => 'required',
            'password' => 'required',
        ], [
            'email.required'=>'يرجي كتابه البريد الالكتروني بطريقه صحيحه',
            'password.required'=>'يرجي كتابه كلمه السر الخاصه بك',
        ]);
        $user = User::where('email', $request->input('email'))->first();
        if ($user) {
            if (auth()->attempt($request->only('email', 'password'))) {
//                auth()->login($user);
                return redirect()->route('index');
            } else {
                return back()->with('error','يوجد خطا في الباسورد او الايميل');
            }
        }
        return back()->with('error','يوجد خطا في الباسورد او الايميل');

    }


    public function UpdateProfile(UserRequest $request, User $user)
    {
        $request_data = $request->except(['password', 'password_confirmation', 'image', 'submit']);

        if ($request->hasFile('image'))
        {
            File::delete('public/uploads/users/' . $user->image);
            $request_data['image'] = uploaded($request->image, 'user');
        }
        $user->update($request_data);
        return redirect()->route('show_profile')->with('success', 'تم تعديل بنجاح');
    }

    public function showProfile()
    {
        $data['user'] = User::where('id',auth()->user()->id)->first();

        return view('dashboard.users.profile',$data);
    }

    public function logout(){
        Auth::guard('web')->logout();
        return redirect()->route('dashboard.login');
    }
}
