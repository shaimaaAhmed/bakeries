@extends('dashboard.layouts.master')

@section('title', 'إضافه مكون جديد')

@section('style')

    <link rel="stylesheet" type="text/css" href="{{asset('dashboard/app-assets/vendors/css/forms/select/select2.min.css')}}">

@endsection

@section('content')

    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">إداره المخابز</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('index')}}">الرئسيه</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('components.index')}}">المكونات</a>
                                </li>
                                <li class="breadcrumb-item active"><a href="{{route('components.create')}}">إضافه مكون جديد</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                <div class="mb-1 breadcrumb-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                        <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body"><!-- Basic Horizontal form layout section start -->
            <section class="form-control-repeater">
                <div class="row">
                    <div class="col-md-12 col-12">
                        <div class="card">
                            @include('dashboard.includes.errors')
                            <div class="card-header">
                                <h4 class="card-title">إضافه مكون جديد</h4>
                            </div>
                            <div class="card-body">
                                <form class="form form-horizontal invoice-repeater" action="{{route('components.store')}}" method="post">
                                   @csrf
                                    <div class="row">
                                        <div class="col-md-6 mb-1">
                                            <label class="form-label" for="select2-basic">اختار المنتج</label>
                                            <select class="select2 form-select" id="select2-basic" name="product_id">
                                                <option> </option>
                                                @forelse($products as $product)
                                                    <option value="{{$product->id}}" {{old('product_id')}}>{{$product->name}}</option>
                                                @empty
                                                    <option> لايوجد بيانات</option>
                                                @endforelse
                                            </select>
                                            @if($errors->has('product_id'))
                                                <span class="text-danger">{{ $errors->first('product_id') }}</span>
                                            @endif
                                        </div>
                                        <div class="col-md-6 mb-1">
                                            <label class="form-label" for="select2-basic">اختار الصنف</label>
                                            <select class="select2 form-select" id="select2-basic" name="item_id">
                                                <option> </option>
                                                @forelse($items as $item)
                                                    <option value="{{$item->id}}" {{old('item_id')}}>{{$item->name}}</option>
                                                @empty
                                                    <option> لايوجد بيانات</option>
                                                @endforelse
                                            </select>
                                            @if($errors->has('item_id'))
                                                <span class="text-danger">{{ $errors->first('item_id') }}</span>
                                            @endif
                                        </div>

                                        <div class="col-sm-12">
                                            <label class="col-form-label" for="qty">الكميه</label>
                                            <input type="number" id="qty" class="form-control" value="{{old('qty')}}" name="qty" placeholder="كود المكون" />
                                            @if($errors->has('qty'))
                                                <span class="text-danger">{{ $errors->first('qty') }}</span>
                                            @endif
                                        </div>

                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="size">الحجم بالجرام</label>
                                                    <input type="number" id="size" class="form-control" value="{{old('size')}}" name="size" placeholder="الحجم بالجرام مثال 50" />
                                                    @if($errors->has('size'))
                                                        <span class="text-danger">{{ $errors->first('size') }}</span>
                                                    @endif
                                                </div>

                                                <div class="col-md-6 col-6">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="staticprice_gram">سعر الحجم</label>
                                                        <input type="number" name="price_gram" class="form-control" id="staticprice_gram" aria-describedby="staticprice_gram" placeholder="سعر الحجم(سعر 50 جرام) " step="any" />
                                                        @if($errors->has('price_gram'))
                                                            <span class="text-danger">{{ $errors->first('price_gram') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="mb-1 col-sm-12">
                                                    <label class="form-label" for="exampleFormControlTextarea1">الوصف</label>
                                                    <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3" placeholder="الوصف">{{old('description')}}</textarea>
                                                    @if($errors->has('description'))
                                                        <span class="text-danger">{{ $errors->first('description') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div data-repeater-list="invoice">
                                            <div data-repeater-item class="invoice_add">
                                                <div class="row d-flex align-items-end">
                                                    <div class="col-md-1 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="itemname">إختار إسم الصنف</label>
                                                            {{--<select class="form-control" id="itemname" aria-describedby="itemname" name="item_id[]">--}}
                                                            <select class="form-control item select2 form-select" id="itemname" aria-describedby="itemname" name="item_id[]" onchange="showItems(this)" required>
                                                                <option></option>
                                                                @foreach($items as $item)
                                                                    <option value="{{$item->id}}" {{old('item_id')== $item->id ? 'selected' :''}}>{{$item->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            @if($errors->has('item_id[]'))
                                                                <span class="text-danger">{{ $errors->first('item_id[]') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1 col-12 company_base" >
                                                        <div class="mb-1">
                                                            <label class="form-label" for="companyname"> إسم الشركه</label>
                                                            <input name="company" type="text" readonly="" value="{{old('company')}}" class="form-control-plaintext" placeholder="الشركه">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-1 col-12 price_base">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="staticprice">السعر</label>
                                                            <input type="text" name="price[]" value="{{old('price')}}" class="form-control itemprice" onchange="showPriceItems(this)" id="staticprice" aria-describedby="staticprice" placeholder="السعر" step="any" />
                                                            @if($errors->has('price[]'))
                                                                <span class="text-danger">{{ $errors->first('price[]') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-1 col-12 qty_base">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="itemquantity">الكميه</label>
                                                            <input value="{{old('qty',1)}}"
                                                                   type="number"
                                                                   class="form-control itemquantity"
                                                                   id="itemquantity"
                                                                   aria-describedby="itemquantity"
                                                                   placeholder="1"
                                                                   name="qty[]"
                                                                   onchange="showQuantityItems(this)"
                                                            />
                                                            @if($errors->has('qty[]'))
                                                                <span class="text-danger">{{ $errors->first('qty[]') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-1 col-12 count_item_base">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="count_item">عدد الوحده</label>
                                                            <input onchange="showCount(this)" value="{{old('count_item',0)}}"
                                                                   type="number"
                                                                   class="form-control"
                                                                   id="count_item"
                                                                   placeholder="عدد الوحده وليكن 14 زجاجه"
                                                                   aria-describedby="count_item"
                                                                   name="count_item"
                                                            />
                                                            @if($errors->has('count_item[]'))
                                                                <span class="text-danger">{{ $errors->first('count_item[]') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1 col-12 price_unit_base">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="price_unit">سعر الوحده</label>
                                                            <input type="text" readonly="" name="price_unit" class="form-control-plaintext" id="price_unit" value="{{old('price_unit',0)}}" step="any">
                                                            @if($errors->has('price_unit'))
                                                                <span class="text-danger">{{ $errors->first('price_unit') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1 col-12 size_base">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="size_base">حجم الوحده</label>
                                                            <input type="text" readonly="" name="size" class="form-control-plaintext" id="size_base" value="{{old('size',0)}}" step="any">
                                                            @if($errors->has('size'))
                                                                <span class="text-danger">{{ $errors->first('size') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-1 col-12 min_stock_base" >
                                                        <div class="mb-1">
                                                            <input name="min_stock" type="hidden" readonly="" class="form-control-plaintext" placeholder="الحد الادني للمخزون" value="{{old('min_stock')}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1 col-12 type_unit_base" >
                                                        <div class="mb-1">
                                                            <input name="type_unit" type="hidden" readonly="" class="form-control-plaintext" placeholder="نوع الوحده" value="{{old('type_unit')}}">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-1 col-12 mb-50">
                                                        <div class="mb-1">
                                                            <button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete type="button">
                                                                <i data-feather="x" class="me-25"></i>
                                                                <span>حذف</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <button class="btn btn-icon btn-primary" type="button" data-repeater-create>
                                                    <i data-feather="plus" class="me-25"></i>
                                                    <span>اضافه جديده</span>
                                                </button>
                                            </div>
                                        </div>

                                        <div class="col-sm-9 offset-sm-3">
                                            <button type="submit" class="btn btn-primary me-1" >حفظ</button>
                                            <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Basic Horizontal form layout section end -->
        </div>
    </div>

@stop
@section('scripts')
    {{--<script src="{{asset('dashboard/app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>--}}

{{--    <script src="{{asset('dashboard/app-assets/js/scripts/forms/form-select2.min.js')}}"></script>--}}
    @endsection