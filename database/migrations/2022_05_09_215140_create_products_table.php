<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('code')->unique();
            $table->double('wright'); //الوزن وليكن 250 جرام
            $table->string('size'); // الحجم وليكن وسط او كبير
            $table->integer('num_pieces'); //عدد القطع
            $table->string('unit_type'); // نوع الوحده مثال قطعه
            $table->double('cost_price')->default(0); // سعر التكلفه
            $table->double('selling_price'); // سعر البيع
            $table->double('wholesale_price'); // سعر الجمله
            $table->double('total_weight_comp')->default(0); // إجمالي وزن المكون
            $table->string('image')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
