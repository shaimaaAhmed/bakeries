<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Delegate extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function supplier(){
        return $this->belongsTo(Supplier::class);
    }

    public function setVisitingAttribute($value)
    {
        $this->attributes['visiting'] = json_encode($value);
    }

    public function getVisitingAttribute($value)
    {
//        return  json_decode($this->visiting);
        return $this->attributes['visiting'] = json_decode($value);
    }
}
