@extends('dashboard.layouts.master')

@section('title', trans('back.edit-var',['var'=>trans('back.user')]))

@section('content')
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <i class="icon-arrow-right6 position-left"></i>
                    <span class="text-semibold">@lang('back.home')</span>
                    - @lang('back.edit-var',['var'=>trans('back.user')])
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/dashboard') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a></li>
                <li><a href="{{ route('users.index') }}"><i class="icon-users position-left"></i> @lang('back.users')
                    </a></li>
                <li class="active">@lang('back.edit-var',['var'=>trans('back.user')])</li>

            </ul>

            @include('dashboard.includes.quick-links')
        </div>
    </div>

    @include('dashboard.includes.errors')


    <div class="row" style="margin: 15px">
        <div class="col-md-6">

            <!-- Basic layout-->
            <form action="{{ route('users.update',$user->id) }}" class="form-horizontal" method="post"
                  enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <input name="id" value="{{$user -> id}}" type="hidden">
                {{--<input type="hidden" name="provider_uuid" value="{{ $user->uuid }}" />--}}
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title"> {{ trans('dash.edit_data') }} </h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-lg-3 control-label">{{ trans('dash.full_name') }}</label>
                            <div class="col-lg-9">
                                <input type="text" name="name" value="{{$user->name}}" class="form-control"
                                       placeholder="{{ trans('dash.full_name') }}">
                            </div>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                        	<strong style="color: red;">{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>


                        <div class="form-group">
                            <label class="col-lg-3 control-label">{{ trans('dash.email') }}</label>
                            <div class="col-lg-9">
                                <input type="email" name="email" class="form-control" value="{{ $user->email }}"
                                       placeholder="{{ trans('dash.email') }}" required>
                            </div>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                        	<strong style="color: red;">{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">{{ trans('dash.mobile') }}</label>
                            <div class="col-lg-9">
                                <input type="text" name="phone" value="{{ $user->phone }}" class="form-control"
                                       placeholder="{{ trans('dash.mobile') }}">
                            </div>
                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                        	<strong style="color: red;">{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">{{ trans('dash.address') }}</label>
                            <div class="col-lg-9">
                                <input type="text" name="address" value="{{ $user->address }}" class="form-control"
                                       placeholder="{{ trans('dash.address') }}">
                            </div>
                            @error('address')
                            <span class="invalid-feedback" role="alert">
                        	<strong style="color: red;">{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>


                        <div class="form-group">
                            <label class="col-lg-3 control-label">{{ trans('dash.gender') }}</label>
                            <div class="col-lg-9">

                                <select name="gander" class="select-border-color border-warning">
                                    <option  {{ $user->gander == 'male' ? 'selected' : '' }} value="male"> {{trans('dash.male')}} </option>
                                    <option {{ $user->gander == 'female' ? 'selected' : '' }} value="female"> {{trans('dash.female')}} </option>
                                </select>
                            </div>
                            @error('gander')
                            <span class="invalid-feedback" role="alert">
                        	<strong style="color: red;">{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>


                        <div class="form-group">
                            <label class="control-label">في حاله اذا لم ترد اضافه صوره لا يتم اضافتها</label><br>
                            <label class="col-lg-3 control-label"> {{ trans('dash.image') }}</label>
                            <div class="col-lg-9">
                                <input type="file" class="file-styled" name="image">
                                <span class="help-block">التنسيقات المقبولة: gif, png, jpg. Max file size 2Mb</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label"> {{ trans('dash.password') }} </label>
                            <div class="col-lg-9">
                                <input type="password" class="form-control" name="password"
                                       placeholder=" {{ trans('dash.password') }} " />
                            </div>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                        	<strong style="color: red;">{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label"> {{ trans('dash.confirm_password') }} </label>
                            <div class="col-lg-9">
                                <input type="password" class="form-control" name="password_confirmation"
                                       placeholder=" {{ trans('dash.confirm_password') }} " />
                            </div>
                            @error('password_confirmation')
                            <span class="invalid-feedback" role="alert">
                        	<strong style="color: red;">{{ $message }}</strong>
                        </span>
                            @enderror

                        </div>


                        <div class="text-right">
                            <input type="submit" class="btn btn-primary"
                                   value=" {{ trans('dash.update_and_forword_2_list') }} "/>
                        </div>
                    </div>
                </div>
            </form>
            <!-- /basic layout -->
        </div>

        <div class="col-md-6">
            <div class="panel panel-flat">

                <div class="panel-heading">
                    <h5 class="panel-title"> {{ trans('dash.image') }} </h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <center>
                        <img src="{{ $user->ImagePath }}"/>
                    </center>
                </div>
            </div>
        </div>
    </div>



@stop
