@extends('dashboard.layouts.master')

@section('title', 'أضافه طلب منتج')

@section('content')
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">إداره المخابز</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('index')}}">الرئسيه</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('orders.index')}}">الطلبات</a>
                                </li>
                                <li class="breadcrumb-item active"><a href="{{route('orders.create')}}">أضافه طلب منتج</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                <div class="mb-1 breadcrumb-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                        <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body"><section class="form-control-repeater">
                <div class="row">
                    <!-- Invoice repeater -->
                    <div class="col-12">
                        <div class="card">
                            @include('dashboard.includes.errors')
                            <div class="card-header">
                                <h4 class="card-title">أضافه طلب منتج</h4>
                            </div>
                            <div class="card-body">
                                <form class="form form-horizontal invoice-repeater" action="{{route('orders.store')}}" method="post">
                                    @csrf
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="col-md-12" data-select2-id="46">
                                                    <label class="form-label" for="select2-basic">إختار الشيف</label>
                                                    <select class="select2 form-select" id="select2-basic" name="chef_id">
                                                       @foreach($chefs as $chef)
                                                            <option value="{{$chef->id}}">{{$chef->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @if($errors->has('chef_id'))
                                                        <span class="text-danger">{{ $errors->first('chef_id') }}</span>
                                                    @endif

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div data-repeater-list="invoice">
                                        <div data-repeater-item class="invoice_add">
                                            <div class="row d-flex align-items-end">
                                                <div class="col-md-2 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="itemname">إختار إسم المنتج</label>
                                                         <select class="form-control item select2 form-select" id="itemname" aria-describedby="itemname" name="product_id[]" onchange="showProducts(this)" required>
                                                            <option></option>
                                                            @foreach($products as $product)
                                                                <option value="{{$product->id}}" {{old('product')== $product->id ? 'selected' :''}}>{{$product->name." ".$product->size}}</option>
                                                            @endforeach
                                                        </select>
                                                        @if($errors->has('product[]'))
                                                            <span class="text-danger">{{ $errors->first('product[]') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-12 price_base">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="staticprice">السعر</label>
                                                        <input type="text" name="price[]" value="{{old('price')}}" class="form-control itemprice" onchange="showPriceItems(this)" id="staticprice" aria-describedby="staticprice" placeholder="السعر" step="any" />
                                                        @if($errors->has('price[]'))
                                                            <span class="text-danger">{{ $errors->first('price[]') }}</span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-md-2 col-12 qty_base">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="itemquantity">الكميه</label>
                                                        <input value="{{old('qty',1)}}"
                                                               type="number"
                                                               class="form-control itemquantity"
                                                               id="itemquantity"
                                                               aria-describedby="itemquantity"
                                                               placeholder="1"
                                                               name="qty[]"
                                                               onchange="showQuantityItems(this)"
                                                        />
                                                        @if($errors->has('qty[]'))
                                                            <span class="text-danger">{{ $errors->first('qty[]') }}</span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-md-2 col-12 discount_base">
                                                    <div class="mb-1">
                                                        <label class="col-form-label" for="first-name">مبلغ الخصم</label>
                                                        <input type="number" id="discount" onchange="showDiscount(this)"  class="form-control discount" value="{{old('discount',0)}}" name="discount" placeholder="مبلغ الخصم" step="any"/>
                                                        @if($errors->has('discount'))
                                                            <span class="text-danger">{{ $errors->first('discount') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-12 total_base" >
                                                    <div class="mb-1">
                                                        <label class="col-form-label" for="total">المبلغ الكلي</label>
                                                        <input name="total" type="number" readonly="" class="form-control-plaintext" placeholder="المبلغ الكلي" value="{{old('total')}}" step="any">
                                                        @if($errors->has('total'))
                                                            <span class="text-danger">{{ $errors->first('total') }}</span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-md-1 col-12 mb-50">
                                                    <div class="mb-1">
                                                        <button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete type="button">
                                                            <i data-feather="x" class="me-25"></i>
                                                            <span>حذف</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <button class="btn btn-icon btn-primary" type="button" data-repeater-create>
                                                <i data-feather="plus" class="me-25"></i>
                                                <span>اضافه جديده</span>
                                            </button>
                                        </div>
                                    </div>
                                    <!-- Invoice Total starts -->
                                    {{--<div class="col-md-12 mt-2">--}}
                                        {{--<div class="mb-1 row">--}}
                                            {{--<div class="col-sm-2">--}}
                                                {{--<label class="form-label" for="total_before_dis">المبلغ قبل الخصم</label>--}}
                                                {{--<input type="text" readonly="" name="total_before_dis" class="form-control-plaintext" id="total_before_dis" value="0" step="any">--}}
                                                {{--<p>المبلغ قبل الخصم: $1800</p>--}}
                                            {{--</div>--}}
                                            {{--<div class="col-sm-2 mb-1">--}}
                                                {{--<label class="col-form-label" for="first-name">مبلغ الخصم</label>--}}
                                                {{--<input type="number" id="discount" class="form-control discount" value="{{old('discount',0)}}" name="discount" placeholder="مبلغ الخصم" step="any"/>--}}
                                                {{--@if($errors->has('discount'))--}}
                                                    {{--<span class="text-danger">{{ $errors->first('discount') }}</span>--}}
                                                {{--@endif--}}
                                            {{--</div>--}}
                                            {{--<div class="col-sm-2">--}}
                                                {{--<p class="invoice-total-title">نسبه الخصم:</p>--}}
                                                {{--<input type="number" id="discount_percentage" readonly="" class="form-control-plaintext discount_percentage" value="0" name="discount_percentage"  step="any"/>--}}
                                                {{--<p id="discount_percentage" class="invoice-total-amount discount_percentage"> 0 %</p>--}}
                                            {{--</div>--}}
                                            {{--<hr class="my-25" />--}}
                                            {{--<div class="col-sm-2 mt-2">--}}
                                                {{--<p class="invoice-total-title">المبلغ الكلي:$1690</p>--}}
                                                {{--<label class="form-label" for="staticprice">المبلغ الكلي</label>--}}
                                                {{--<input type="text" readonly="" name="total" class="form-control-plaintext all_total" id="all_total" value="0" step="any">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-12">--}}
                                        {{--<div class="mb-1 row">--}}
                                            {{--<div class="col-sm-6">--}}
                                                {{--<label class="col-form-label" for="first-name">المبلغ المدفوع</label>--}}
                                                {{--<input type="number" id="paid" class="form-control" value="{{old('paid',0)}}" name="paid" placeholder="المبلغ المدفوع" step="any"/>--}}
                                                {{--@if($errors->has('paid'))--}}
                                                    {{--<span class="text-danger">{{ $errors->first('paid') }}</span>--}}
                                                {{--@endif--}}
                                            {{--</div>--}}
                                            {{--<div class="col-sm-6">--}}
                                                {{--<label class="col-form-label" for="first-name">المبلغ المتبقي</label>--}}
                                                {{--<input type="number" id="residual" class="form-control" value="{{old('residual',0)}}" name="residual" placeholder="المبلغ المتبقي" step="any"/>--}}
                                                {{--@if($errors->has('residual'))--}}
                                                    {{--<span class="text-danger">{{ $errors->first('residual') }}</span>--}}
                                                {{--@endif--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    <!-- Invoice Total ends -->

                                    <div class="col-sm-9 offset-sm-3">
                                        <button type="submit" class="btn btn-primary me-1" >حفظ</button>
                                        <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /Invoice repeater -->
                </div>
            </section>

        </div>
    </div>
@stop

@section('scripts')

    <script>
        var oldQuantity  = 0;
        var oldPrice  = 0;
        function showProducts(that) {
            var product_id = $(that).val();
            // console.log(product_id);
            if(product_id){
                $.ajax({
                    url: 'product/'+product_id,
                    type:'get',
                    success:function (data) {
                        // console.log(data);
                        if(data.status === 1){
                            oldPrice = that.parentNode.parentNode.parentNode.getElementsByClassName('price_base')[0].getElementsByTagName('input')[0].value = data.data.cost_price
                            oldQuantity =  that.parentNode.parentNode.parentNode.getElementsByClassName('qty_base')[0].getElementsByTagName('input')[0].value = 1
                            that.parentNode.parentNode.parentNode.getElementsByClassName('discount_base')[0].getElementsByTagName('input')[0].value = 0

                        }
                        if(oldQuantity == 1){
                            suptotal = parseFloat(oldQuantity) * parseFloat(oldPrice);
                            that.parentNode.parentNode.parentNode.getElementsByClassName('total_base')[0].getElementsByTagName('input')[0].value = Math.round(100 * suptotal) / 100;

                        }
                    },

                    error: function (jqXhr, textStatus, errorMessage) { // error callback
                        alert(errorMessage);
                    }
                });
            }
        }

        // function total_ammount_price() {
        //     $('.itemprice').each(function(){
        //         var value = $(this).val();
        //         if(value.length != 0)
        //         {
        //             suptotal = oldQuantity * value;
        //             // sum += Math.round(100*suptotal)/100;
        //             sum += suptotal;
        //             // sum += parseFloat(suptotal);
        //         }
        //     });
        //     $('#total_before_dis').val(sum);
        //     $('#all_total').val(sum);
        // }

        function showDiscount(that){
            var Discount = $(that).val();
            var subtotal = that.parentNode.parentNode.parentNode.getElementsByClassName('total_base')[0].getElementsByTagName('input')[0].value ;
              var totl = subtotal - Discount;
             that.parentNode.parentNode.parentNode.getElementsByClassName('total_base')[0].getElementsByTagName('input')[0].value = totl ;
        }

        function showQuantityItems(that){
            var suptotal = 0;
            var qunt = $(that).val();
            var price = that.parentNode.parentNode.parentNode.getElementsByClassName('price_base')[0].getElementsByTagName('input')[0].value ;
            suptotal = parseFloat(qunt) * parseFloat(price);
           that.parentNode.parentNode.parentNode.getElementsByClassName('total_base')[0].getElementsByTagName('input')[0].value = Math.round(100 * suptotal) / 100;
        }

        function newTotalPrice (Quantity){
            var sum = 0;
            var suptotal = 0;

            $('.itemprice').each(function(){
                var value = $(this).val();
                if(value.length != 0)
                {
                    suptotal = Quantity * value;
                    sum += Math.round(100*suptotal)/100;

                }
            });
            $('#total_before_dis').val(sum);
            $('#all_total').val(sum);
        }

        $(document).ready(function(){
            $("#discount").on("input", function(){
                var total = document.getElementById('total_before_dis').value;
                var Discount = $(this).val();
                var sub = total - Discount;
                var div = (Discount / total)*100;
                document.getElementById('discount_percentage').innerText = Math.round(div)+ "%";
                // $("#discount_percentage").val(div);
                $("#all_total").val(sub);
            });

            $("#paid").on("input", function(){
                var total = document.getElementById('all_total').value;
                var Paid = $(this).val();
                var sub = total - Paid;
                $("#residual").val(sub);
            });
        });


    </script>
@endsection