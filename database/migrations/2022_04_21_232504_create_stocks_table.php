<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Stocks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('item_id');
            $table->string('category');
            $table->unsignedBigInteger('unit_id');
            $table->double('qty');
            $table->double('price');
            $table->string('company');
            $table->double('wight')->nullable(); // الوزن الكلي بالجرام او الملي
            $table->string('type_wight')->nullable(); // نوع الوزن بالجرام او الملي
            $table->double('count_item'); // عدد الوحده وليكن 14 زجاجه والوحده كرتونه
            $table->enum('status',['available','not_available','weak'])->default('available'); // ضعيف -غير متوفر - متوفر
            $table->double('min_stock')->nullable();
            $table->foreign('item_id')
                ->references('id')->on('items')->onDelete('cascade');
//            $table->foreign('category_id')
//                ->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('unit_id')
                ->references('id')->on('units')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Stocks');
    }
}
