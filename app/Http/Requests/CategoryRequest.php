<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'    => 'required|string|unique:categories,name,'.$this->id,
            'notes' => 'nullable|string',
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'اسم القسم مطلوب',
            'name.string'=>'يجب ان يكون اسم القسم كلمه',
            'name.unique'=>'اسم القسم موجود من قبل',
            'notes.string'=>'يجب ان يكون ملاحظه القسم كلمات',
        ];
    }
}
