<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
                <li class="nav-item me-auto" {{ \Request::is('/index') ? 'active' : '' }}><a class="navbar-brand" href="{{route('index')}}">
                    <span class="brand-logo">
                        <img src="{{asset('uploads/Bakery-logo.png')}}" height="40" width="40">
                    </span>
                        <h2 class="brand-text mb-0">إدارة المخابز</h2></a>
                </li>
                <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i></a></li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <!-- Horizontal menu content-->
    <div class="main-menu-content">
            <!-- include ../../../includes/mixins-->
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" nav-item {{ \Request::is('index') ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{route('index')}}"><i data-feather="home"></i><span class="menu-title text-truncate" data-i18n="Dashboards">الرئسية</span></a>
            </li>
            <li class=" navigation-header"><span data-i18n="Apps &amp; Pages">القائمه الرئسية </span><i data-feather="more-horizontal"></i>
            </li>

            <li class="nav-item {{ \Request::is('companies/*') ? 'has-sub open' : '' }}"><a class="d-flex align-items-center" href="{{route('companies.index')}}" ><i data-feather="layers"></i><span data-i18n="User Interface">الشركات</span></a>
                <ul class="menu-content">
                        <li class="{{ \Request::is('companies/create') ? 'active' : '' }}"><a class=" d-flex align-items-center" href="{{route('companies.create')}}" data-bs-toggle="" data-i18n="Typography"><i data-feather="type"></i><span data-i18n="Typography">اضافه شركه جديده</span></a>
                        </li>
                        <li class="{{ \Request::is('companies') ? 'active' : '' }}"><a class=" d-flex align-items-center" href="{{route('companies.index')}}" data-bs-toggle="" data-i18n="Feather"><i data-feather="eye"></i><span data-i18n="Feather">الشركات</span></a>
                        </li>
                    </ul>
                </li>


                <li class="nav-item {{ \Request::is('categories/*') ? 'has-sub open' : '' }}"><a class="d-flex align-items-center" href="{{route('categories.index')}}" ><i data-feather="layers"></i><span data-i18n="User Interface">الأقسام</span></a>
                    <ul class="menu-content">
                        <li class="{{ \Request::is('categories/create') ? 'active' : '' }}"><a class=" d-flex align-items-center" href="{{route('categories.create')}}" data-bs-toggle="" data-i18n="Typography"><i data-feather="type"></i><span data-i18n="Typography">اضافه قسم جديده</span></a>
                        </li>
                        <li class="{{ \Request::is('categories') ? 'active' : '' }}"><a class=" d-flex align-items-center" href="{{route('categories.index')}}" data-bs-toggle="" data-i18n="Feather"><i data-feather="eye"></i><span data-i18n="Feather">الأقسام</span></a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item {{ \Request::is('items/*') ? 'has-sub open' : '' }}" ><a class="d-flex align-items-center" href="{{route('items.index')}}" ><i data-feather="edit"></i><span data-i18n="Forms &amp; Tables">الأصناف</span></a>
                    <ul class="menu-content">
                        <li class="{{ \Request::is('items/create') ? 'active' : '' }}"><a class=" d-flex align-items-center" href="{{route('items.create')}}" data-i18n="Form Layout"><i data-feather="box"></i><span data-i18n="Form Layout">إضافه صنف جديد</span></a>
                        </li>
                        <li class="{{ \Request::is('items') ? 'active' : '' }}"><a class=" d-flex align-items-center" href="{{route('items.index')}}" data-i18n="Form Wizard"><i data-feather="package"></i><span data-i18n="Form Wizard">الأصناف</span></a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item {{ \Request::is('suppliers/*') ? 'has-sub open' : '' }}" ><a class="nav-link d-flex align-items-center" href="{{route('suppliers.index')}}" ><i data-feather="file-text"></i><span data-i18n="Pages">التجار</span></a>
                    <ul class="menu-content">
                        <li class="{{ \Request::is('suppliers/create') ? 'active' : '' }}"><a class=" d-flex align-items-center" href="{{route('suppliers.create')}}" data-i18n="Profile"><i data-feather="user"></i><span data-i18n="Profile">إضافه تاجر جديد</span></a>
                        </li>
                        <li class="{{ \Request::is('suppliers') ? 'active' : '' }}"><a class=" d-flex align-items-center" href="{{route('suppliers.index')}}" data-i18n="FAQ"><i data-feather="help-circle"></i><span data-i18n="FAQ">التجار</span></a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item {{ \Request::is('delegates/*') ? 'has-sub open' : '' }}"><a class="d-flex align-items-center" href="{{route('delegates.index')}}" ><i data-feather="bar-chart-2"></i><span data-i18n="Charts &amp; Maps">المندوبين</span></a>
                    <ul class="menu-content">
                        <li class="{{ \Request::is('delegates/create') ? 'active' : '' }}"><a class=" d-flex align-items-center" href="{{route('delegates.create')}}" data-i18n="Third Level"><i data-feather="circle"></i><span data-i18n="Third Level">إضافه مندوب جديد</span></a>
                        </li>
                        <li class="{{ \Request::is('delegates') ? 'active' : '' }}"><a class=" d-flex align-items-center" href="{{route('delegates.index')}}"  data-i18n="Leaflet Maps"><i data-feather="map"></i><span data-i18n="Leaflet Maps">المندوبين</span></a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item {{ \Request::is('invoices/*') ? 'has-sub open' : '' }}" ><a class="d-flex align-items-center" href="{{route('invoices.index')}}" ><i data-feather="bar-chart-2"></i><span data-i18n="Charts &amp; Maps">فواتير المشتريات</span></a>
                    <ul class="menu-content">
                        <li class="{{ \Request::is('invoices/create') ? 'active' : '' }}"><a class=" d-flex align-items-center" href="{{route('invoices.create')}}" data-bs-toggle="" data-i18n="Third Level"><i data-feather="circle"></i><span data-i18n="Third Level">اضافه فاتوره مشتريات</span></a>
                        </li>
                        <li class="{{ \Request::is('invoices') ? 'active' : '' }}"><a class=" d-flex align-items-center" href="{{route('invoices.index')}}" data-bs-toggle="" data-i18n="Leaflet Maps"><i data-feather="map"></i><span data-i18n="Leaflet Maps">فواتير المشتريات</span></a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item {{ \Request::is('stocks/*') ? 'has-sub open' : '' }}" ><a class="d-flex align-items-center" href="{{route('stocks.index')}}" ><i data-feather="box"></i><span data-i18n="Misc">المخازن</span></a>
                    <ul class="menu-content">
                        {{--<li data-menu=""><a class=" d-flex align-items-center" href="{{route('invoices.create')}}" data-bs-toggle="" data-i18n="Disabled Menu"><i data-feather="eye-off"></i><span data-i18n="Disabled Menu">اضافه مخزن</span></a>--}}
                        {{--</li>--}}
                        <li class="{{ \Request::is('stocks') ? 'active' : '' }}"><a class=" d-flex align-items-center" href="{{route('stocks.index')}}" data-bs-toggle="" data-i18n="Documentation"><i data-feather="folder"></i><span data-i18n="Documentation">المخازن</span></a>
                        </li>

                    </ul>
                </li>

                <li class="nav-item {{ \Request::is('products/*') ? 'has-sub open' : '' }}" ><a class="d-flex align-items-center" href="{{route('products.index')}}" ><i data-feather="bar-chart-2"></i><span data-i18n="Charts &amp; Maps">منتجات(تصنيع)</span></a>
                    <ul class="menu-content">
                        <li class="{{ \Request::is('products/create') ? 'active' : '' }}" ><a class=" d-flex align-items-center" href="{{route('products.create')}}" data-bs-toggle="" data-i18n="Third Level"><i data-feather="circle"></i><span data-i18n="Third Level">اضافه منتج</span></a>
                        </li>
                        <li class="{{ \Request::is('products') ? 'active' : '' }}"><a class=" d-flex align-items-center" href="{{route('products.index')}}" data-bs-toggle="" data-i18n="Leaflet Maps"><i data-feather="map"></i><span data-i18n="Leaflet Maps">منتجات(تصنيع)</span></a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item {{ \Request::is('components/*') ? 'has-sub open' : '' }}" ><a class="d-flex align-items-center" href="{{route('components.index')}}" ><i data-feather="bar-chart-2"></i><span data-i18n="Charts &amp; Maps">المكونات</span></a>
                    <ul class="menu-content">
                        <li class="{{ \Request::is('components/create') ? 'active' : '' }}"><a class=" d-flex align-items-center" href="{{route('components.create')}}" data-bs-toggle="" data-i18n="Third Level"><i data-feather="circle"></i><span data-i18n="Third Level">اضافه مكون</span></a>
                        </li>
                        <li class="{{ \Request::is('components') ? 'active' : '' }}"><a class=" d-flex align-items-center" href="{{route('components.index')}}" data-bs-toggle="" data-i18n="Leaflet Maps"><i data-feather="map"></i><span data-i18n="Leaflet Maps">المكونات</span></a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item {{ \Request::is('chefs/*') ? 'has-sub open' : '' }}" ><a class="d-flex align-items-center" href="{{route('chefs.index')}}" ><i data-feather="bar-chart-2"></i><span data-i18n="Charts &amp; Maps">الشيفات</span></a>
                    <ul class="menu-content">
                        <li class="{{ \Request::is('chefs/create') ? 'active' : '' }}"><a class=" d-flex align-items-center" href="{{route('chefs.create')}}" data-bs-toggle="" data-i18n="Third Level"><i data-feather="circle"></i><span data-i18n="Third Level">اضافه شيف</span></a>
                        </li>
                        <li class="{{ \Request::is('chefs') ? 'active' : '' }}"><a class=" d-flex align-items-center" href="{{route('chefs.index')}}" data-bs-toggle="" data-i18n="Leaflet Maps"><i data-feather="map"></i><span data-i18n="Leaflet Maps">الشيفات</span></a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item {{ \Request::is('orders/*') ? 'has-sub open' : '' }}"><a class="d-flex align-items-center" href="{{route('orders.index')}}" ><i data-feather="bar-chart-2"></i><span data-i18n="Charts &amp; Maps">الطلبات</span></a>
                    <ul class="menu-content">
                        <li class="{{ \Request::is('orders/create') ? 'active' : '' }}"><a class=" d-flex align-items-center" href="{{route('orders.create')}}" data-bs-toggle="" data-i18n="Third Level"><i data-feather="circle"></i><span data-i18n="Third Level">اضافه طلب</span></a>
                        </li>
                        <li class="{{ \Request::is('orders') ? 'active' : '' }}"><a class=" d-flex align-items-center" href="{{route('orders.index')}}" data-bs-toggle="" data-i18n="Leaflet Maps"><i data-feather="map"></i><span data-i18n="Leaflet Maps">الطلبات</span></a>
                        </li>
                    </ul>
                </li>
            </ul>
    </div>
</div>