<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ComponentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'qty'    => 'required',
            'product_id'    => 'required',
            'item_id'    => 'required',
            'price_gram'    => 'required',
            'size'    => 'required',
            'description' => 'nullable|string',
        ];
    }

    public function messages()
    {
        return [
            'product_id.required' => 'المنتج مطلوب',
            'item_id.required' => 'الصنف مطلوب',
            'qty.required' => 'الكميه مطلوب',
            'size.required' => 'الحجم بالجرام مطلوب',
            'price_gram.required' => 'سعر الجرامات مطلوب',
            'description.string' => 'يجب ان يكون ملاحظه الشركه كلمات',
        ];
    }
}
