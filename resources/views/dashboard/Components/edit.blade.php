@extends('dashboard.layouts.master')

@section('title', 'تعديل مكون ')


@section('content')

    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">إداره المخابز</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('index')}}">الرئسيه</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('components.index')}}">المكونات</a>
                                </li>
                                <li class="breadcrumb-item active"><a href="{{route('components.edit',$component->id)}}">تعديل مكون </a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                <div class="mb-1 breadcrumb-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                        <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body"><!-- Basic Horizontal form layout section start -->
            <section id="basic-horizontal-layouts">
                <div class="row">
                    <div class="col-md-12 col-12">
                        <div class="card">
                            @include('dashboard.includes.errors')
                            <div class="card-header">
                                <h4 class="card-title">تعديل مكون {{$component->name}}</h4>
                            </div>
                            <div class="card-body">
                                <form class="form form-horizontal" action="{{route('components.update',$component->id)}}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <div class="row">
                                        <div class="col-md-6 mb-1">
                                            <label class="form-label" for="select2-basic">اختار القسم</label>
                                            <select class="select2 form-select" id="select2-basic" name="category_id">
                                                <option> </option>
                                                @forelse($categories as $category)
                                                    <option value="{{$category->id}}" {{$category->id == $component->category_id ? 'selected' : ' '}}>{{$category->name}}</option>
                                                @empty
                                                    <option> لايوجد بيانات</option>
                                                @endforelse
                                            </select>
                                            @error('category_id')
                                            <span class="invalid-feedback" role="alert">
                                                            <strong style="color: red;">{{ $message }}</strong>
                                                        </span>
                                            @enderror
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="col-form-label" for="first-name">اسم المكون</label>
                                            <input type="text" id="first-name" class="form-control" value="{{$component->name}}" name="name" placeholder="اسم المكون" />
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                            <strong style="color: red;">{{ $message }}</strong>
                                                        </span>
                                            @enderror
                                        </div>

                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <input name="id" value="{{$component->id}}" type="hidden">
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="first-name">كود المكون</label>
                                                    <input type="number" id="first-name" class="form-control" value="{{$component->code}}" name="code" placeholder="كود المكون" />
                                                    @error('code')
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong style="color: red;">{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="col-md-6 col-6">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="staticprice">السعر</label>
                                                        <input type="text" name="price" class="form-control" id="staticprice" aria-describedby="staticprice" placeholder="السعر" value="{{$component->price}}" step="any" />
                                                        @if($errors->has('price'))
                                                            <span class="text-danger">{{ $errors->first('price') }}</span>
                                                        @endif
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-md-6 mb-1">
                                            <label class="form-label" for="select2-basic">اختار الوحده</label>
                                            <select class="select2 form-select" id="select2-basic" name="unit_id">
                                                <option> </option>
                                                @forelse($units as $unit)
                                                    <option value="{{$unit->id}}" {{$unit->id == $component->unit_id ? 'selected' : ' '}} >{{$unit->name}}</option>
                                                @empty
                                                    <option> لايوجد بيانات</option>
                                                @endforelse
                                            </select>
                                            @error('unit_id')
                                            <span class="invalid-feedback" role="alert">
                                                            <strong style="color: red;">{{ $message }}</strong>
                                                        </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-6 mb-1" data-select2-id="46">
                                            <label class="form-label" for="select2-basic">اختار الشركه</label>
                                            <select class="select2 form-select" id="select2-basic" name="company_id">
                                                <option></option>
                                                @forelse($companies as $company)
                                                    <option value="{{$company->id}}" {{$company->id == $component->company_id ? 'selected' : ' '}}>{{$company->name}}</option>
                                                @empty
                                                    <option> لايوجد بيانات</option>
                                                @endforelse
                                            </select>
                                            @error('company_id')
                                            <span class="invalid-feedback" role="alert">
                                                            <strong style="color: red;">{{ $message }}</strong>
                                                        </span>
                                            @enderror
                                        </div>

                                        <div class="col-sm-6">
                                            <label class="col-form-label" for="first-name">الوزن</label>
                                            <input type="text" id="first-name" class="form-control" value="{{$component->wright}}" name="wright" step="any" />
                                            @error('wright')
                                            <span class="invalid-feedback" role="alert">
                                                            <strong style="color: red;">{{ $message }}</strong>
                                                        </span>
                                            @enderror
                                        </div>

                                        <div class="col-sm-6">
                                            <label class="col-form-label" for="first-name">نوع الوزن </label>
                                            <input type="text" id="first-name" class="form-control" value="{{old('type_unit',$component->type_unit)}}" name="type_unit" placeholder="مثال كيلوا او ملي او جرام او لتر " />
                                            @error('type_unit')
                                            <span class="invalid-feedback" role="alert">
                                                            <strong style="color: red;">{{ $message }}</strong>
                                                        </span>
                                            @enderror
                                        </div>

                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="first-name">عدد الوحده</label>
                                                    <input type="text" id="first-name" class="form-control" value="{{$component->count_item}}" name="count_item" placeholder="عدد الوحده (مثال الكرتونه تحتوي علي 12 زجاجه)" />
                                                    @error('count_item')
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong style="color: red;">{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="first-name">الحد الادني للمخزون</label>
                                                    <input type="number" id="first-name" class="form-control" value="{{$component->min_stock}}" name="min_stock" placeholder="الحد الادني للمخزون" />
                                                    @error('min_stock')
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong style="color: red;">{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="mb-1 col-sm-12">
                                                    <label class="form-label" for="exampleFormControlTextarea1">الوصف</label>
                                                    <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3" placeholder="الوصف">{{$component->description}}</textarea>
                                                    @error('description')
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong style="color: red;">{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="mb-1 col-sm-12">
                                                    <label class="form-label" for="exampleFormControlTextarea1">ملاحظه</label>
                                                    <textarea class="form-control" name="notes" id="exampleFormControlTextarea1" rows="3" placeholder="ملاحظه">{{$component->notes}}</textarea>
                                                    @error('notes')
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong style="color: red;">{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-sm-9 offset-sm-3">
                                            <button type="submit" class="btn btn-primary me-1" >حفظ</button>
                                            <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Basic Horizontal form layout section end -->
        </div>
    </div>
@stop
