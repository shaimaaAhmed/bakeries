<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    public function getImagePathAttribute()
    {
        $image = Product::where('id', $this->id)->first()->image;
        if (!$image) {
            return "لا يوجد صوره";
        }
        return asset('uploads/products/' . $this->image);
    }

    public function components(){
        return $this->hasMany(Component::class );
    }

    public function orders(){
        return $this->hasMany(Order::class);
    }

}
