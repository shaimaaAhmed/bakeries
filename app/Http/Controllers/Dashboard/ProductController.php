<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Models\Component;
use App\Models\Item;
use App\Models\Product;
use Illuminate\Support\Facades\File;
use Exception ;

class ProductController extends Controller
{
    
    public function index()
    {
        $products = Product::latest()->get();
        return view('dashboard.Products.index', compact('products'));
    }

    public function createDeteils()
    {
        $items = Item::get();
        return view('dashboard.Products.create1',compact('items'));
    }


    public function storeDeteils(ProductRequest $request){
        try{
            $request->except(['image']);
            $image ='';
            if ($request->image) $image =  uploaded($request->image, 'product');

            $product = Product::create($request->validated());
            $product->update([
                'image' =>$image,
            ]);
            $product->save();
            return redirect()->route('products.index')->with('success', 'تم اضافه المنتج بنجاح');

        }
        catch (\Exception $er){
//            return $er;
            return redirect()->route('products.index')->with('error', 'حدث خطأ يرجي المحاوله مره اخري');
        }
    }

    public function create()
    {
        $items = Item::get();
        return view('dashboard.Products.create',compact('items'));

    }


    public function store(ProductRequest $request)
    {

        try{
            $request->except(['image']);
            $image ='';
            if ($request->image) $image =  uploaded($request->image, 'product');

            $product = Product::create($request->validated());
            $product->update([
                'image' =>$image,
            ]);
            $product->save();
            $data = $request->except(['user_id','supplier_id','delegate_id']);
            if ($product){
                foreach ($data['invoice'] as $key => $val){
                    if(!empty($val)){
                        $sizeOnegram = $data['invoice'][$key]['size']/$product->wright;
                        if($key < count($data['invoice'])-1)
                        {
                            $attribute = new component;
                            $attribute->product_id = $product->id;
                            $attribute->item_id = $data['invoice'][$key]['item_id'];
                            $attribute->size = $data['invoice'][$key]['size'];
                            $attribute->qty = $data['invoice'][$key]['qty'] == null ? 1 : $data['invoice'][$key]['qty'];
                            $attribute->price_gram = $data['invoice'][$key]['price_gram'];
                            $attribute->size_one_gram = $sizeOnegram;
                            $attribute->save();
                        }else{
                            $attribute = new component;
                            $attribute->product_id = $product->id;
                            $attribute->item_id = $data['invoice'][$key]['item_id['];
                            $attribute->size = $data['invoice'][$key]['size'];
                            $attribute->qty = $data['invoice'][$key]['qty[']== null ? 1 : $data['invoice'][$key]['qty['];
                            $attribute->price_gram =$data['invoice'][$key]['price_gram['];
                            $attribute->size_one_gram = $sizeOnegram;
                            $attribute->save();
                        }

                    }
                }
            }else{

                return back()->with('error', 'حدث خطأ يرجي المحاوله مره اخري');

            }
            return redirect()->route('products.index')->with('success', 'تم اضافه المنتج بنجاح');

        }
        catch (\Exception $er){
//            return $er;
            return redirect()->route('products.index')->with('error', 'حدث خطأ يرجي المحاوله مره اخري');
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        try{
            $items = Item::get();
            $product = Product::find($id);
            if (!$product) {
                return redirect()->route('products.index')->with('error','لا يوجد شيف بهذه البيانات');
            }

            return view('dashboard.Products.edit', compact('product','items'));
        }
        catch (\Exception $ex){

            return redirect()->route('products.index')->with('error', 'حدث خطأ يرجي المحاوله مره اخري');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
//        dd($request->all());
        try{
            $product = Product::find($id);
            $request_data = $request->except('image');
            
            if ($request->has('image'))
            {
                File::delete(public_path('uploads/products/' . $product->image));

                $request_data['image'] = uploaded($request->image, 'product');
            }
            $product->update($request_data);

            if ($product){

                $data = $request->except(['name','code','selling_price',
                    'wholesale_price','cost_price','description','image']);
                foreach($data['idAttr'] as $key=>$attr){
                    $sizeOnegram = $data['size_component'][$key]/$product->wright;
                    Component::where(['id'=>$data['idAttr'][$key]])->update([
                        'item_id' => $data['item_id'][$key],
                        'product_id' => $product->id,
                        'qty' => $data['qty'][$key],
                        'price_gram'=>$data['price_gram'][$key],
                        'size' => $data['size_component'][$key],
                        'size_one_gram' => $sizeOnegram,
                    ]);
                }
                return redirect()->route('products.index')->with('message', 'تم التعديل  بنجاح');

                }else{
                    return back()->with('error','حدث خطأ يرجي المحاوله مره اخري');
                 }
        }
        catch (\Exception $ex){
//            return $ex;
            return redirect()->route('products.index')->with('error', 'حدث خطأ يرجي المحاوله مره اخري');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        if(!$product) return back()->with('error', 'لا يوجد بيانات');
        try
        {
            File::delete(public_path('uploads/products/' . $product->image));
            $product->delete();
            return redirect()->route('products.index')->with('message', 'تم حذف المنتج بنجاح');
        }
        catch (Exception $e) {
            return back()->with('error', 'لا يوجد بيانات');
        }

    }

}
