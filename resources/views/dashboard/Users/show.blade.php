@extends('dashboard.layouts.master')

@section('title', $user->name)

@section('content')
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <i class="icon-arrow-right6 position-left"></i> 
                    <span class="text-semibold">@lang('back.home')</span> - {{ ucwords($user->name) }}
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/admin-panel') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a></li>
                <li><a href="{{ url('/admin-panel/users') }}"><i class="icon-users position-left"></i> @lang('back.users')</a></li>
                <li class="active">{{ $user->name }}</li>
            </ul>

			@include('dashboard.includes.quick-links')
        </div>
    </div>
    <!-- /page header -->
    <div class="content">
    	<div class="row">
    		<div class="col-md-8 col-md-offset-2">
                <!-- Basic table -->
                <div class="panel panel-flat">
                    <div class="panel-body">
                    	<div class="row">
                    		<div class="col-md-3" style="float: {{ floating('right', 'left') }}">
                    			<div class="thumbnail">
								<div class="thumb">
									<label>@lang('dash.image')</label>
									<img style="width: 300px;height: 200px;" src="{{ getImage('users', $user->image) }}" alt="">
									<div class="caption-overflow">
										<span>
											<a href="{{ route('users.edit', $user->id) }}" class="btn btn-info btn-sm">@lang('back.edit')</a>
										</span>
									</div>
								</div>

								<div class="caption">
									<label>@lang('dash.full_name')</label>
									<h6 class="text-semibold no-margin-top" title="{{ $user->name }}">
										{{ $user->name}}
									</h6>
									{{--<p>{{ ucwords($user->neighborhood) }}</p>--}}
									{{--<p>{{ ucwords($user->address) }}</p>--}}
								</div>
							</div>
                    		</div>
                    		<div class="col-md-9" style="float: {{ floating('left', 'right') }}">								
								<div class="well">
				                    <dl dir="{{ direction() }}">
										<dt>@lang('dash.address')</dt>
										<dd>{{ ucwords($user->address) }}</dd>

										<dt>@lang('dash.email')</dt>
										<dd><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></dd>

										<dt>@lang('dash.mobile')</dt>
										<dd>{{ $user->phone ?? trans('dash.no-value')  }}</dd>

										<dt>@lang('dash.gender')</dt>
										@if($user->gander=='male' )
												<dd>ذكر</dd>
											@else
												<dd>أنثي</dd>
											@endif
										{{--<dt>@lang('dash.form-status')</dt>--}}
										{{--<dd>--}}
						                    {{--@if($user->status == 1) <span class="label label-success">@lang('backend.active')</span>--}}
						                    {{--@else <span class="label label-danger">@lang('backend.disactive')</span>--}}
						                    {{--@endif--}}
										{{--</dd>--}}

										<dt>@lang('back.since')</dt>
										<dd>{{ $user->created_at->diffForHumans() }}</dd>

										{{--<dt>@lang('backend.reservations')</dt>--}}
										{{--<dd>{{ $user->reservations->count() }}</dd>--}}
									</dl>
								</div>
                    		</div>
                    	</div>
                    </div>
                </div>
                <!-- /basic table -->
    		</div>
    	</div>
    </div>
@stop