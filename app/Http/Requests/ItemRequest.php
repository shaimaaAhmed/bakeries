<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ItemRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'name'    => 'required|string',
            'wright'    => 'required',
//            'size'    => 'required|string',
            'unit_id'    => 'required',
            'price'    => 'required|numeric',
            'company_id'    => 'required',
            'category_id'    => 'required',
            'min_stock'    => 'required',
            'count_item'    => 'required',
            'type_unit'    => 'required',
            'code'    => 'required|numeric|unique:items,code,'.$this->id,
            'description' => 'nullable|string',
            'notes' => 'nullable|string',
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'اسم الصنف مطلوب',
            'name.string'=>'يجب ان يكون اسم الصنف حروف',
            'code.required'=>'كود الصنف مطلوب',
            'wright.required'=>'وزن الصنف مطلوب',
//            'size.string'=>'وزن الصنف حروف',
            'code.numeric'=>'يجب ان يكون كود الصنف ارقام',
            'price.required'=>'سعر الصنف مطلوب',
            'price.numeric'=>'يجب ان يكون سعر الصنف ارقام',
            'code.unique'=>' كود الصنف موجود من قبل',
            'description.string'=>'يجب ان يكون وصف الصنف كلمات',
            'notes.string'=>'يجب ان يكون ملاحظه الصنف كلمات',
            'unit_id.required'=>'اسم الوحده مطلوب',
            'company_id.required'=>'اسم الشركه مطلوب',
            'category_id.required'=>'اسم القسم مطلوب',
            'min_stock.required'=>'الحد الادني للمخزون مطلوب',
            'count_item.required'=>'عدد الوحده مطلوب',
            'type_unit.required'=>'نوع حجم الوحده مطلوب',
        ];
    }

}
