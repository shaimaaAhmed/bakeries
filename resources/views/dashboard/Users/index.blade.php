@extends('dashboard.layouts.master')

@section('title', trans('back.users'))

@section('content')

    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <i class="icon-arrow-right6 position-left"></i>
                    <span class="text-semibold">@lang('back.dashboard')</span> - @lang('back.users')
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ route('dashboard.index') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a>
                </li>
                <li class="active">@lang('back.users')</li>
            </ul>

            @include('dashboard.includes.quick-links')
        </div>
    </div>
    <!-- /page header -->


    @include('dashboard.includes.errors')

    <!-- Basic datatable -->
    <div class="panel panel-flat" dir="{{ direction() }}" style="margin: 20px;">
        <div class="panel-heading">
            @include('dashboard.includes.table-header', ['collection' => $users, 'name' => 'users', 'icon' => 'users'])
        </div>
        <br>
        <div class="list-icons" style="padding-right: 10px;">
            <a href="{{ route('users.create') }}" class="btn btn-success btn-labeled btn-labeled-left"><b><i
                        class="icon-plus2"></i></b>اضافة عميل جديد</a>
        </div>


        <table class="table datatable-basic" id="users" style="font-size: 16px;">
            <thead>
            <tr>
                <th>#</th>
                <th>@lang('back.form-name')</th>
                <th>@lang('back.form-email')</th>
                {{--<th>@lang('back.form-mobile')</th>--}}
                <th>@lang('back.form-gender')</th>
                <th>@lang('back.form-image')</th>
                <th>@lang('back.since')</th>
                <th class="text-center">@lang('back.form-actions')</th>
            </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr id="user-row-{{ $user->id }}">

                        <td>{{ $user->id }}</td>

                        {{--<td><a href="{{ route('user.show', $user->id) }}"> {{ isNullable($user->full_name) }}</a></td>--}}
                        <td><a href="{{route('users.show', $user->id)}}"> {{ isNullable($user->name) }}</a></td>

                        {{--<td><a href="mailto:{{ $user->email }}">{{ str_limit_30(isNullable($user->email)) }}</a></td>--}}
                        <td>{{ $user->email }}</td>
                        {{--<td>{{ $user->mobile }}</td>--}}
                        @if($user->gander=='male' )
                            <td>ذكر</td>
                        @else
                            <td>أنثي</td>
                        @endif
                        @if($user->image)
                            <td><img src="{{asset($user->ImagePath)}}" width="60" height="60" class="img-circle"></td>
                            {{--                    <td><img width="60" height="60" class="img-circle" src="{{ getImage('users', $user->image_path)}}"--}}
                            {{--                             alt=""></td>--}}
                        @else
                            <td>لايوجد صوره</td>
                        @endif
                        <td>{{ $user->created_at->diffForHumans() }}</td>

                        <td class="text-center">
                            {{--@include('user.includes.edit-delete', ['route' => 'users', 'model' => $user])--}}

                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                                class="icon-menu9"></i></a>

                                    <ul class="dropdown-menu dropdown-menu-{{ floating('right', 'left') }}">
                                        {{--<li><a href="{{ route($route.'.edit',['id'=>$model->id]) }}"><i class="icon-database-edit2"></i>@lang('back.edit')</a></li>--}}

                                        <li>
                                            <a href="{{ route('users.edit',$user->id) }}"> <i
                                                        class="icon-database-edit2"></i>@lang('back.edit') </a>

                                        </li>
                                        {{--<li>--}}
                                        {{--<button id="{{$user->id}}" data-token="{{ csrf_token() }}"--}}
                                        {{--data-route="{{URL::route('user.destroy',$user->id)}}"--}}
                                        {{--type="button"class="destroy btn btn-danger btn-xs" >--}}
                                        {{--<i class="icon-database-remove"></i>@lang('back.delete')--}}
                                        {{--</button>--}}
                                        {{--</li>--}}

                                        <li>
                                            <a data-id="{{ $user->id }}" class="delete-action"
                                               href="{{ Url('/user/user/'.$user->id) }}">
                                                <i class="icon-database-remove"></i>@lang('back.delete')
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic datatable -->
@stop

@section('scripts')
    @include('dashboard.layouts.ajax_delete', ['model' => 'user'])


    {{--    <script>--}}
{{--        $('a.delete-action').on('click', function (e) {--}}
{{--            var id = $(this).data('id');--}}
{{--            var tbody = $('table#users tbody');--}}
{{--            var count = tbody.data('count');--}}

{{--            e.preventDefault();--}}

{{--            swal({--}}
{{--                title: "هل انت متأكد من حذف هذا العميل",--}}
{{--                // text: "سيتم الحذف بالانتقال لسلة المهملات",--}}
{{--                icon: "warning",--}}
{{--                buttons: true,--}}
{{--                dangerMode: true,--}}
{{--            })--}}
{{--                .then((willDelete) => {--}}
{{--                    if (willDelete) {--}}
{{--                        var tbody = $('table#users tbody');--}}
{{--                        var count = tbody.data('count');--}}

{{--                        $.ajax({--}}
{{--                            type: 'POST',--}}
{{--                            url: '{{ route('ajax-delete-user') }}',--}}
{{--                            data: {id: id},--}}
{{--                            success: function (response) {--}}
{{--                                if (response.deleteStatus) {--}}
{{--                                    // $('#user-row-'+id).fadeOut(); count = count - 1;tbody.attr('data-count', count);--}}
{{--                                    $('#user-row-' + id).remove();--}}
{{--                                    count = count - 1;--}}
{{--                                    tbody.attr('data-count', count);--}}
{{--                                    swal(response.message, {icon: "success"});--}}
{{--                                } else {--}}
{{--                                    swal(response.error);--}}
{{--                                }--}}
{{--                            },--}}
{{--                            error: function (x) {--}}
{{--                                crud_handle_server_errors(x);--}}
{{--                            },--}}
{{--                            complete: function () {--}}
{{--                                if (count == 1) tbody.append(`<tr><td colspan="5"><strong>No data available in table</strong></td></tr>`);--}}
{{--                            }--}}
{{--                        });--}}
{{--                    } else {--}}
{{--                        swal("تم الغاء العمليه");--}}
{{--                    }--}}
{{--                });--}}
{{--        });--}}

{{--    </script>--}}




@stop
