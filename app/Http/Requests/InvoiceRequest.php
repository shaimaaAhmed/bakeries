<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number'    => 'required|integer|unique:invoices,number,'.$this->id,
//            'date'    => 'required',
//            'date'    => 'required|date',
//            'time'    => 'required',
//            'time'    => 'required|mimes',
            'discount'    => 'required|numeric',
            'supplier_id'    => 'required',
//            'delegate_id'    => 'required',
            'type'    => 'required',
//
            'item_id.*'    => 'required',
            'unit_id.*'    => 'required',
            'qty.*'    => 'required|numeric',
//            'price'    => 'required|numeric',
            'notes' => 'nullable|string',
            'count_item.*' => 'required',
            'invoice' => 'required',
            'price_unit' => 'nullable',
        ];
    }

    public function messages()
    {
        return [
            'number.required'=>'رقم الفاتوره مطلوب',
            'number.integer'=>'يجب ان يكون رقم الفاتوره رقم',
            'number.unique'=>'رقم الفاتوره موجود من قبل',
//            'date.required'=>'التاريخ مطلوب',
//            'date.date'=>'يجب ان يكون التاريخ ',
//            'time.required'=>'الوقت مطلوب',
//            'time.mimes'=>'يجب ان يكون وقتا',
            'discount.required'=>'الخصم مطلوب',
            'count_item.required'=>'عدد الصنف مطلوب',
            'discount.numeric'=>'يجب ان يكون الخصم رقم ',
            'supplier_id.required'=>'اسم التاجر مطلوب',
            'delegate_id.required'=>'اسم المندوب مطلوب',

            'item_id.*.required'=>'اسم الصنف مطلوب',
            'type.required'=>'نوع الفاتوره مطلوب',
            'unit_id.*.required'=>'اسم الوحده مطلوب',
            'qty.*.required'=>'الكميه مطلوب',
//            'qty.numeric'=>'الكميه مطلوب',
            'price.*.required'=>'يجب ان يكون السعر رقم',
//            'price.numeric'=>'يجب ان يكون السعر رقم',
            'notes.string'=>'يجب ان يكون الملاحظه كلمات',
        ];
    }
}
