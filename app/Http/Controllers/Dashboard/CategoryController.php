<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::latest()->get();
        return view('dashboard.Categories.index',compact('categories'));
    }

   
    public function create()
    {
        return view('dashboard.Categories.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'    => 'required|string|unique:categories',
            'notes' => 'nullable|string',
        ],
            [
                'name.required'=>'اسم القسم مطلوب',
                'name.string'=>'يجب ان يكون اسم القسم كلمه',
                'name.unique'=>'اسم القسم موجود من قبل',
                'notes.string'=>'يجب ان يكون ملاحظه القسم كلمات',
            ]);

        Category::create([
            'name'=>$request->name,
            'notes'=>$request->notes,
        ]);
        return redirect()->route('categories.index')->with('message', 'تم إضافه القسم بنجاح');

    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $category = Category::whereId($id)->first();
        if ($category){
            return view('dashboard.Categories.edit',compact('category'));
        }else{
            return back()->with('error', 'لا يوجد بيانات');

        }
    }

   
    public function update(CategoryRequest $request, $id)
    {
        $category = Category::whereId($id)->first();
        if ($category){
            $category->update($request->validated());
            return redirect()->route('categories.index')->with('message', 'تم تعديل القسم بنجاح');

        }else{
            return back()->with('error', 'لا يوجد بيانات');

        }
    }
    
    public function destroy($id)
    {
        $category = Category::whereId($id)->first();
        if ($category){
            $category->delete();
            return redirect()->route('categories.index')->with('message', 'تم حذف القسم بنجاح');
        }else{
            return back()->with('error', 'لا يوجد بيانات');

        }
    }
}
