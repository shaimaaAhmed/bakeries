<li>
    <a href="{{route('dashboard.index')}}"><i class="icon-home2"></i> <span> {{ trans('dash.dashboard') }} </span></a>

</li>

<li>
    <a href="{{route('show_profile')}}"><i class="icon-profile"></i> <span> @lang('dash.account_settings') </span></a>

</li>

@foreach(models() as $icon => $model)
    @php $route = str()->plural($model); @endphp
    @if(permission_route_checker($route.'.index') )
        <li class="{{ request()->is(request()->is(app()->getLocale().'/dashboard/'.$route.'/*')) || request()->is(request()->is(app()->getLocale().'/dashboard/'.$route)) ? 'active' : '' }}">
            <a href="#"><i class="icon-{{$icon}}"></i> <span> {{ trans('dash.'.$route) }} </span></a>
            <ul>
                @if(permission_route_checker($route.'.index'))
                    <li class="{{ request()->is(app()->getLocale().'/dashboard/'.$route) ? 'active' : '' }}">
                        {{--<a href="#">{{ trans('dash.all') }}</a>--}}
                        <a href="{{route($route.'.index')}}">{{ trans('dash.all') }}</a>
                    </li>
                @endif

                @if(permission_route_checker($route.'.create'))
                    <li class="{{ request()->is(app()->getLocale().'/dashboard/'.$route.'/create') ? 'active' : '' }}">
                        {{--<a href="#"> {{ trans('dash.add') }}</a>--}}
                        <a href="{{route($route.'.create')}}"> {{ trans('dash.add') }}</a>
                    </li>
                @endif
            </ul>
        </li>
    @endif
@endforeach

<li class="{{ request()->is(request()->is(app()->getLocale().'/dashboard/orders'.'/*')) || request()->is(request()->is(app()->getLocale().'/dashboard/orders')) ? 'active' : '' }}">
    <a href="#"><i class="icon-stack"></i> <span> الطلبات</span></a>
    <ul>
        @if(permission_route_checker($route.'.index'))
            <li class="{{ request()->is(app()->getLocale().'/dashboard/orders') ? 'active' : '' }}">
                {{--<a href="#">{{ trans('dash.all') }}</a>--}}
                <a href="{{route('orders.index')}}">{{ trans('dash.all') }}</a>
            </li>
        @endif

    </ul>
</li>


<li class="{{ request()->is(request()->is(app()->getLocale().'/dashboard/settings'.'/*')) || request()->is(request()->is(app()->getLocale().'/dashboard/settings')) ? 'active' : '' }}">
    <a href="#"><i class="icon-gear"></i> <span> {{ trans('dash.settings') }} </span></a>
    <ul>
        @if(permission_route_checker($route.'.index'))
        <li class="{{ request()->is(app()->getLocale().'/dashboard/settings') ? 'active' : '' }}">
            {{--<a href="#">{{ trans('dash.all') }}</a>--}}
            <a href="{{route('settings.index')}}">{{ trans('dash.all') }}</a>
        </li>
        @endif

    </ul>
</li>

{{--<li>--}}
{{--<a href="#"><i class="icon-users"></i> <span> {{ trans('dash.users') }} </span></a>--}}
{{--    <ul>--}}
{{--        <li><a href="{{route('users.index')}}">{{ trans('dash.all') }}</a></li>--}}
{{--        <li><a href="{{route('users.create')}}"> {{ trans('dash.add') }}</a></li>--}}
{{--    </ul>--}}
{{--</li>--}}





