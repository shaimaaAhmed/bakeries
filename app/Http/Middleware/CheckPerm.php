<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckPerm
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = auth()->user();

        if(!auth()->check()) return redirect('/login');

        if($user->admin_group_id == 1) return $next($request);

        $currentRoute = $request->route()->getName();

        $adminPermissions = json_decode($user->admin_group->permissions);

//        if(!in_array($currentRoute, $adminPermissions)) abort(401);
        if(!in_array($currentRoute, $adminPermissions)) abort(510);

        if ($user->admin_group->permissions == "") abort(401);

        return $next($request);
    }
}
