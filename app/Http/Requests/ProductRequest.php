<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'    => 'required|string',
            'wright'    => 'required',
            'size'    => 'required|string',
            'num_pieces'    => 'required|numeric',
            'unit_type'    => 'required|string',
            'selling_price'    => 'required|numeric',
            'wholesale_price'    => 'required',
            'total_weight_comp'    => 'nullable',
            'code'    => 'required|numeric|unique:products,code,'.$this->id,
            'description' => 'nullable|string',
            'image' => 'nullable|image',
            'cost_price' => 'nullable',
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'اسم المنتج مطلوب',
            'name.string'=>'يجب ان يكون اسم المنتج حروف',
            'code.required'=>'كود المنتج مطلوب',
            'wright.required'=>'وزن المنتج مطلوب',
            'size.required'=>'وزن المنتج حروف',
            'size.string'=>'وزن المنتج حروف',
            'code.numeric'=>'يجب ان يكون كود المنتج ارقام',
            'wholesale_price.required'=>'سعر بيع الجمله المنتج مطلوب',
            'selling_price.required'=>'سعر بيع المنتج مطلوب',
            'selling_price.numeric'=>'يجب ان يكون سعر المنتج ارقام',
            'code.unique'=>' كود المنتج موجود من قبل',
            'description.string'=>'يجب ان يكون وصف المنتج كلمات',
            'image.image'=>'يجب ان يكون صوره المنتج صوره',
            'num_pieces.required'=>'عدد القطع مطلوب',
            'unit_type.required'=>'نوع حجم المنتج مطلوب',
        ];
    }
}
