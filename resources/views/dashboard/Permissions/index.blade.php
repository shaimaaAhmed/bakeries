@extends('dashboard.layouts.master')

@section('title', trans('back.permissions'))

@section('content')

    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <i class="icon-arrow-right6 position-left"></i>
                    <span class="text-semibold">@lang('back.dashboard')</span> - @lang('back.permissions')
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/dashboard') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a></li>
                <li class="active">@lang('back.permissions')</li>
            </ul>

            @include('dashboard.includes.quick-links')
        </div>
    </div>
    <!-- /page header -->

    @include('dashboard.includes.errors')

    <!-- Basic datatable -->
    <div class="panel panel-flat" dir="{{ direction() }}" style="margin: 20px;">
        <div class="panel-heading">
            @include('dashboard.includes.table-header', ['collection' => $permissions, 'name' => 'permissions', 'icon' => 'permissions'])
        </div><br>
        <div class="list-icons" style="padding-right: 10px;">
            <a href="{{ route('permissions.create') }}" class="btn btn-success btn-labeled btn-labeled-left"><b><i
                            class="icon-plus2"></i></b>اضافة صلاحيات جديدة</a>
        </div>


        <table class="table datatable-basic" id="permissions" style="font-size: 16px;">
            <thead>
            <tr>
                <th>#</th>
                <th>@lang('back.name_of_job')</th>

                <th>@lang('back.since')</th>
                <th class="text-center">@lang('back.form-actions')</th>
            </tr>
            </thead>
            <tbody>
            @foreach($permissions as $permission)
                <tr id="permission-row-{{ $permission->id }}">

                    <td>{{ $permission->id }}</td>

                    <td><a href="{{ route('permissions.show', $permission->id) }}"> {{ isNullable($permission->name) }}</a></td>

                    <td>{{ $permission->created_at->diffForHumans() }}</td>


                    <td class="text-center">
                        {{--@include('permission.includes.edit-delete', ['route' => 'permissions', 'model' => $permission])--}}
                        @if( $permission->id!=1)

                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                            class="icon-menu9"></i></a>

                                <ul class="dropdown-menu dropdown-menu-{{ floating('right', 'left') }}">

                                    <li>
                                        <a href="{{ route('permissions.edit',$permission->id) }}"> <i
                                                    class="icon-database-edit2"></i>@lang('back.edit') </a>
                                    </li>

                                    <li>
                                        <a data-id="{{ $permission->id }}" class="delete-action"
                                           href="{{ Url('/permission/permission/'.$permission->id) }}">
                                            <i class="icon-database-remove"></i>@lang('back.delete')
                                        </a>
                                    </li>

                                </ul>
                            </li>
                        </ul>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic datatable -->
@stop

@section('scripts')
    @include('dashboard.layouts.ajax_delete', ['model' => 'permission'])


    {{--    <script>--}}
{{--        $('a.delete-action').on('click', function (e) {--}}
{{--            var id = $(this).data('id');--}}
{{--            var tbody = $('table#permissions tbody');--}}
{{--            var count = tbody.data('count');--}}

{{--            e.preventDefault();--}}

{{--            swal({--}}
{{--                title: "هل انت متأكد من حذف هذه الصلاحية",--}}
{{--                // text: "سيتم الحذف بالانتقال لسلة المهملات",--}}
{{--                icon: "warning",--}}
{{--                buttons: true,--}}
{{--                dangerMode: true,--}}
{{--            })--}}
{{--                .then((willDelete) => {--}}
{{--                    if (willDelete) {--}}
{{--                        var tbody = $('table#permissions tbody');--}}
{{--                        var count = tbody.data('count');--}}

{{--                        $.ajax({--}}
{{--                            type: 'POST',--}}
{{--                            url: '{{ route('ajax-delete-permission') }}',--}}
{{--                            data: {id: id},--}}
{{--                            success: function (response) {--}}
{{--                                if (response.deleteStatus) {--}}
{{--                                    // $('#permission-row-'+id).fadeOut(); count = count - 1;tbody.attr('data-count', count);--}}
{{--                                    $('#permission-row-' + id).remove();--}}
{{--                                    count = count - 1;--}}
{{--                                    tbody.attr('data-count', count);--}}
{{--                                    swal(response.message, {icon: "success"});--}}
{{--                                }--}}
{{--                                else {--}}
{{--                                    swal(response.error);--}}
{{--                                }--}}
{{--                            },--}}
{{--                            error: function (x) {--}}
{{--                                crud_handle_server_errors(x);--}}
{{--                            },--}}
{{--                            complete: function () {--}}
{{--                                if (count == 1) tbody.append(`<tr><td colspan="5"><strong>No data available in table</strong></td></tr>`);--}}
{{--                            }--}}
{{--                        });--}}
{{--                    }--}}
{{--                    else {--}}
{{--                        swal("تم الغاء العمليه");--}}
{{--                    }--}}
{{--                });--}}
{{--        });--}}

{{--    </script>--}}


@stop
