<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class Chef extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function getImagePathAttribute()
    {
        $image = Chef::where('id', $this->id)->first()->image;
        if (!$image) {
            return "لا يوجد صوره";
        }
        return asset('uploads/chefs/' . $this->image);
    }


    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
    public function orders(){
        return $this->hasMany(Order::class);
    }
}
