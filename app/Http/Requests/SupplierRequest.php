<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'    => 'required|string',
            'company_id'    => 'required',
            'company_id'    => 'required',
            'phone'    => 'required|numeric',
            'notes' => 'nullable|string',
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'اسم التاجر مطلوب',
            'name.string'=>'يجب ان يكون اسم التاجر حروف',
            'phone.required'=>'رقم التاجر مطلوب',
            'phone.numeric'=>'يجب ان يكون رقم التاجر ارقام',
            'notes.string'=>'يجب ان يكون ملاحظه التاجر كلمات',
            'company_id.required'=>'اسم الشركه مطلوب',
        ];
    }
}
