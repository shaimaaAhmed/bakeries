<?php

namespace App\Providers;

use App\Models\Invoice;
use App\Models\Item;
use App\Models\Order;
use App\Models\Product;
use App\Models\Supplier;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->share('suppliers' , Supplier::count());
        view()->share('items' , Item::count());
        view()->share('products' , Product::count());
        view()->share('orders_co' , Order::where('status','جاري التجهيز')->count());
        view()->share('orders' , Order::where('status','جاري التجهيز')->latest()->take(6)->get());
        view()->share('invoices' , Invoice::latest()->take(5)->get());
    }
}
