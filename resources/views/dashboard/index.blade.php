@extends('dashboard.layouts.master')

@section('title', 'الرئسيه')


@section('content')
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
        </div>
        <div class="content-body"><!-- Dashboard Ecommerce Starts -->
            <section id="dashboard-ecommerce">
                <div class="row match-height">
                    <!-- Statistics Card -->
                    <div class="col-xl-12 col-md-12 col-12">
                        <div class="card card-statistics">
                            <div class="card-header">
                                <h4 class="card-title">الإحصائيات</h4>
                            </div>
                            <div class="card-body statistics-body">
                                <div class="row">
                                    <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
                                        <div class="d-flex flex-row">
                                            <div class="avatar bg-light-primary me-2">
                                                <div class="avatar-content">
                                                    <i data-feather="user" class="avatar-icon"></i>
                                                </div>
                                            </div>
                                            <div class="my-auto">
                                                <h4 class="fw-bolder mb-0">{{$suppliers}}</h4>
                                                <p class="card-text font-small-3 mb-0">التجار</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
                                        <div class="d-flex flex-row">
                                            <div class="avatar bg-light-info me-2">
                                                <div class="avatar-content">
                                                    <i data-feather="trending-up" class="avatar-icon"></i>
                                                </div>
                                            </div>
                                            <div class="my-auto">
                                                <h4 class="fw-bolder mb-0">{{$products}}</h4>
                                                <p class="card-text font-small-3 mb-0">المنتجات</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-sm-0">
                                        <div class="d-flex flex-row">
                                            <div class="avatar bg-light-danger me-2">
                                                <div class="avatar-content">
                                                    <i data-feather="box" class="avatar-icon"></i>
                                                </div>
                                            </div>
                                            <div class="my-auto">
                                                <h4 class="fw-bolder mb-0">{{$orders}}</h4>
                                                <p class="card-text font-small-3 mb-0">الطالبات النشطة</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-3 col-sm-6 col-12">
                                        <div class="d-flex flex-row">
                                            <div class="avatar bg-light-success me-2">
                                                <div class="avatar-content">
                                                    <i data-feather="box" class="avatar-icon"></i>
                                                </div>
                                            </div>
                                            <div class="my-auto">
                                                <h4 class="fw-bolder mb-0">{{$items}}</h4>
                                                <p class="card-text font-small-3 mb-0">الأصناف</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/ Statistics Card -->
                </div>

                <div class="row match-height">
                    <!-- Company Table Card -->
                    <div class="col-lg-12 col-12">
                        <div class="card card-company-table">
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>المنتج</th>
                                            <th>المستخدم</th>
                                            <th>الشيف</th>
                                            <th>التاريخ</th>
                                            <th>الحاله</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($orders as $order)
                                        <tr>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <div>
                                                        <div class="fw-bolder">{{$order->product->name}}</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <span>{{$order->user->name}}</span>
                                                </div>
                                            </td>
                                            <td class="text-nowrap">
                                                <div class="d-flex flex-column">
                                                    <span class="fw-bolder mb-25">{{$order->chef->name}}</span>
                                                </div>
                                            </td>
                                            <td>{{$order->date}}</td>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <span class="fw-bolder me-1">{{$order->status}}</span>
                                                </div>
                                            </td>
                                        </tr>
                                        @empty
                                            <tr class="text-center">
                                                <td>لا يوجد بيانات</td>
                                            </tr>
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/ Company Table Card -->

                    <div class="col-lg-12 col-12">
                        <div class="card card-company-table">
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>رقم الفاتوره</th>
                                            <th>التاريخ</th>
                                            <th>المتبقي</th>
                                            <th>الحاله</th>
                                            <th>النوع</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($invoices as $invoice)
                                            <tr>
                                                <td>
                                                    <div class="d-flex align-items-center">
                                                        <div>
                                                            <div class="fw-bolder">{{$invoice->number}}</div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center">
                                                        <span>{{$invoice->date}}</span>
                                                    </div>
                                                </td>
                                                <td class="text-nowrap">
                                                    <div class="d-flex flex-column">
                                                        <span class="fw-bolder mb-25">{{$invoice->residual}}</span>
                                                    </div>
                                                </td>
                                                @if($invoice->status == 'paid')
                                                    <td>مدفوع </td>
                                                @elseif($invoice->status == 'deferred')
                                                    <td>آجل</td>
                                                @else
                                                    <td>دفع جزئي </td>
                                                    @endif
                                                <td>
                                                    <div class="d-flex align-items-center">
                                                       @if($invoice->type == 'buying')
                                                                <span class="fw-bolder me-1">شراء </span>
                                                    @elseif($invoice->type == 'vibrant')
                                                            <span class="fw-bolder me-1">مرتجع</span>
                                                    @else
                                                            <span class="fw-bolder me-1">هالك </span>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr class="text-center">
                                                <td>لا يوجد بيانات</td>
                                            </tr>
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Dashboard Ecommerce ends -->

        </div>
    </div>
@stop
