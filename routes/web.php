<?php

use App\Http\Controllers\Dashboard\CategoryController;
use App\Http\Controllers\Dashboard\ChefController;
use App\Http\Controllers\Dashboard\CompanyController;
use App\Http\Controllers\Dashboard\ComponentController;
use App\Http\Controllers\Dashboard\DelegateController;
use App\Http\Controllers\Dashboard\HomeController;
use App\Http\Controllers\Dashboard\InvoiceController;
use App\Http\Controllers\Dashboard\ItemController;
use App\Http\Controllers\Dashboard\OrderController;
use App\Http\Controllers\Dashboard\ProductController;
use App\Http\Controllers\Dashboard\StockController;
use App\Http\Controllers\Dashboard\SupplierController;
use App\Http\Controllers\Dashboard\UnitController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Route::group(['namespace' => 'Dashboard'], function () {

//Route::view('main','dashboard.Units.index');
    Route::get('login',[HomeController::class,'viewLogin'])->name('login');
    Route::post('login',[HomeController::class,'login'])->name('dashboard.saveLogin');

    Route::group(['middleware' => 'auth'], function () {

        Route::get('/index', [HomeController::class,'home'])->name('index');
        Route::get('showProfile',   [HomeController::class,'showProfile'])->name('show_profile');
        Route::post('updateProfile/{admin}',  [HomeController::class,'UpdateProfile'])->name('update_profile');
        Route::post('logout',[HomeController::class,'logout'])->name('dashboard.logout');


        Route::resource('units',UnitController::class);
        Route::resource('companies',CompanyController::class);
        Route::resource('categories',CategoryController::class);
        Route::resource('items',ItemController::class);
        Route::resource('suppliers',SupplierController::class);
        Route::resource('delegates',DelegateController::class);

        Route::resource('invoices',InvoiceController::class);
        Route::get('invoices/item/{id}', [InvoiceController::class ,'item'])->name('unit');
        Route::get('invoices/delegate/{id}', [InvoiceController::class ,'delegate'])->name('supplier.delegates');


        Route::get('stocks',[StockController::class,'index'])->name('stocks.index');

        Route::resource('products',ProductController::class);
        Route::get('/item/{id}', [InvoiceController::class ,'item'])->name('item.unit');
        Route::get('product',[ProductController::class,'createDeteils'])->name('product.create');
        Route::post('product',[ProductController::class,'storeDeteils'])->name('product.store');

        Route::resource('components',ComponentController::class);

        Route::resource('chefs',ChefController::class);
        Route::resource('orders',OrderController::class);
        Route::get('orders/product/{id}',[OrderController::class,'product']);
//        Route::get('orders',[OrderController::class,'orderRequest']);

//        Route::view('Card/title','dashboard.Orders.order-request');

        //        Route::group(['middleware' => 'CheckPerm'], function () {});
    });
//});
