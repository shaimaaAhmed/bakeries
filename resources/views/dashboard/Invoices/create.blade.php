@extends('dashboard.layouts.master')

@section('title', 'أضافه فاتوره مشتريات')

@section('content')
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">إداره المخابز</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('index')}}">الرئسيه</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('invoices.index')}}">فواتير المشتريات</a>
                                </li>
                                <li class="breadcrumb-item active"><a href="{{route('invoices.create')}}">أضافه فاتوره مشتريات</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                <div class="mb-1 breadcrumb-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                        <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body"><section class="form-control-repeater">
                <div class="row">
                    <!-- Invoice repeater -->
                    <div class="col-12">
                        <div class="card">
                            @include('dashboard.includes.errors')
                            <div class="card-header">
                                <h4 class="card-title">أضافه فاتوره مشتريات</h4>
                            </div>
                            <div class="card-body">
                                <form class="form form-horizontal invoice-repeater" action="{{route('invoices.store')}}" method="post">
                                        @csrf
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-1 row">
                                                    <div class="col-md-6" data-select2-id="46">
                                                        <label class="form-label" for="select2-basic">نوع الفاتوره</label>
                                                        <select class="select2 form-select" id="select2-basic" name="type">
                                                            <option value="buying">شراء</option>
                                                            <option value="vibrant">مرتجع</option>
                                                            <option value="mortal">هالك</option>
                                                        </select>
                                                        @if($errors->has('type'))
                                                            <span class="text-danger">{{ $errors->first('type') }}</span>
                                                        @endif

                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label class="col-form-label" for="first-name">رقم الفاتوره</label>
                                                        <input type="number" id="first-name" class="form-control" value="{{old('number')}}" name="number" placeholder="رقم الفاتوره" />
                                                        @if($errors->has('number'))
                                                            <span class="text-danger">{{ $errors->first('number') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="mb-1 row">
                                                    <div class="col-md-6" data-select2-id="46">
                                                <label class="form-label" for="select2-basic">اختار التاجر</label>
                                                <select class="select2 form-select supplier" id="select2-basic" name="supplier_id">
                                                    <option></option>
                                                    @forelse($suppliers as $supplier)
                                                        <option value="{{$supplier->id}}" >{{$supplier->name}}</option>
                                                        {{--<option value="{{$supplier->id}}" {{old('supplier_id') == $supplier->id ? 'selected' : '' }}>{{$supplier->name}}</option>--}}
                                                    @empty
                                                        <option> لايوجد بيانات</option>
                                                    @endforelse
                                                </select>
                                                @if($errors->has('supplier_id'))
                                                    <span class="text-danger">{{ $errors->first('supplier_id') }}</span>
                                                @endif

                                            </div>
                                                    <div class="col-md-6" data-select2-id="46">
                                                        <label class="form-label" for="select2-basic">اختار المندوب</label>
                                                        <select class="select2 form-select" id="delegate_id" name="delegate_id">

                                                        </select>
                                                        @if($errors->has('delegate_id'))
                                                    <span class="text-danger">{{ $errors->first('delegate_id') }}</span>
                                                @endif

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="exampleFormControlTextarea1">ملاحظه</label>
                                                    <textarea class="form-control" name="notes" id="exampleFormControlTextarea1" placeholder="ملاحظه">{{old('notes')}}</textarea>
                                                    @if($errors->has('notes'))
                                                        <span class="text-danger">{{ $errors->first('notes') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    <div class="mb-2">
                                        <h3>المشتريات</h3>
                                    </div>
                                    <div data-repeater-list="invoice">
                                            <div data-repeater-item class="invoice_add">
                                                <div class="row d-flex align-items-end">
                                                    <div class="col-md-1 col-12">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="itemname">إختار إسم الصنف</label>
                                                            {{--<select class="form-control" id="itemname" aria-describedby="itemname" name="item_id[]">--}}
                                                            <select class="form-control item select2 form-select" id="itemname" aria-describedby="itemname" name="item_id[]" onchange="showItems(this)" required>
                                                              <option></option>
                                                                @foreach($items as $item)
                                                                    <option value="{{$item->id}}" {{old('item_id')== $item->id ? 'selected' :''}}>{{$item->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            @if($errors->has('item_id[]'))
                                                                <span class="text-danger">{{ $errors->first('item_id[]') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1 col-12 category_base" >
                                                        <div class="mb-1">
                                                            <label class="form-label" for="categoryname"> إسم القسم</label>
                                                            {{--<select class="form-control item select2 form-select" id="categoryname" aria-describedby="categoryname" name="category_id[]" >--}}
                                                               <input name="category" value="{{old('category')}}" type="text" readonly="" class="form-control-plaintext" placeholder="القسم">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1 col-12 company_base" >
                                                        <div class="mb-1">
                                                            <label class="form-label" for="companyname"> إسم الشركه</label>
                                                            <input name="company" type="text" readonly="" value="{{old('company')}}" class="form-control-plaintext" placeholder="الشركه">

                                                            {{--<select class="form-control item select2 form-select" id="companyname" aria-describedby="companyname" name="company_id[]" >--}}
                                                                {{--<option></option>--}}
                                                            {{--</select>--}}
                                                        </div>
                                                    </div>

                                                    <div class="col-md-1 col-12 unit_base">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="itemcost">إختار الوحده</label>
                                                            <select class="form-control  select2 form-select" id="itemcost" aria-describedby="itemcost" name="unit_id[]" required>
                                                                <option></option>
                                                                @foreach($units as $unit)
                                                                    <option value="{{$unit->id}}" {{old('unit_id')== $unit->id ? 'selected' :''}}>{{$unit->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            @if($errors->has('unit_id[]'))
                                                                <span class="text-danger">{{ $errors->first('unit_id[]')}}</span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-1 col-12 price_base">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="staticprice">السعر</label>
                                                            <input type="text" name="price[]" value="{{old('price')}}" class="form-control itemprice" onchange="showPriceItems(this)" id="staticprice" aria-describedby="staticprice" placeholder="السعر" step="any" />
                                                            @if($errors->has('price[]'))
                                                                <span class="text-danger">{{ $errors->first('price[]') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-1 col-12 qty_base">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="itemquantity">الكميه</label>
                                                            <input value="{{old('qty',1)}}"
                                                                   type="number"
                                                                    class="form-control itemquantity"
                                                                    id="itemquantity"
                                                                    aria-describedby="itemquantity"
                                                                    placeholder="1"
                                                                    name="qty[]"
                                                                     onchange="showQuantityItems(this)"
                                                            />
                                                            @if($errors->has('qty[]'))
                                                                <span class="text-danger">{{ $errors->first('qty[]') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-1 col-12 count_item_base">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="count_item">عدد الوحده</label>
                                                            <input onchange="showCount(this)" value="{{old('count_item',0)}}"
                                                                   type="number"
                                                                   class="form-control"
                                                                   id="count_item"
                                                                   placeholder="عدد الوحده وليكن 14 زجاجه"
                                                                   aria-describedby="count_item"
                                                                   name="count_item"
                                                            />
                                                            @if($errors->has('count_item[]'))
                                                                <span class="text-danger">{{ $errors->first('count_item[]') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1 col-12 price_unit_base">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="price_unit">سعر الوحده</label>
                                                            <input type="text" readonly="" name="price_unit" class="form-control-plaintext" id="price_unit" value="{{old('price_unit',0)}}" step="any">
                                                            @if($errors->has('price_unit'))
                                                                <span class="text-danger">{{ $errors->first('price_unit') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1 col-12 size_base">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="size_base">حجم الوحده</label>
                                                            <input type="text" readonly="" name="size" class="form-control-plaintext" id="size_base" value="{{old('size',0)}}" step="any">
                                                            @if($errors->has('size'))
                                                                <span class="text-danger">{{ $errors->first('size') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-1 col-12 min_stock_base" >
                                                        <div class="mb-1">
                                                            <input name="min_stock" type="hidden" readonly="" class="form-control-plaintext" placeholder="الحد الادني للمخزون" value="{{old('min_stock')}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1 col-12 type_unit_base" >
                                                        <div class="mb-1">
                                                            <input name="type_unit" type="hidden" readonly="" class="form-control-plaintext" placeholder="نوع الوحده" value="{{old('type_unit')}}">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-1 col-12 mb-50">
                                                        <div class="mb-1">
                                                            <button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete type="button">
                                                                <i data-feather="x" class="me-25"></i>
                                                                <span>حذف</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr />
                                            </div>
                                        </div>
                                    <div class="row">
                                            <div class="col-12">
                                                <button class="btn btn-icon btn-primary" type="button" data-repeater-create>
                                                    <i data-feather="plus" class="me-25"></i>
                                                    <span>اضافه جديده</span>
                                                </button>
                                            </div>
                                        </div>
                                    <!-- Invoice Total starts -->
                                    <div class="col-md-12 mt-2">
                                        <div class="mb-1 row">
                                            <div class="col-sm-2">
                                                <label class="form-label" for="total_before_dis">المبلغ قبل الخصم</label>
                                                <input type="text" readonly="" name="total_before_dis" class="form-control-plaintext" id="total_before_dis" value="0" step="any">
                                                {{--<p>المبلغ قبل الخصم: $1800</p>--}}
                                            </div>
                                            <div class="col-sm-2 mb-1">
                                                <label class="col-form-label" for="first-name">مبلغ الخصم</label>
                                                <input type="number" id="discount" class="form-control discount" value="{{old('discount',0)}}" name="discount" placeholder="مبلغ الخصم" step="any"/>
                                                @if($errors->has('discount'))
                                                    <span class="text-danger">{{ $errors->first('discount') }}</span>
                                                @endif
                                            </div>
                                            <div class="col-sm-2">
                                                        <p class="invoice-total-title">نسبه الخصم:</p>
                                                        {{--<input type="number" id="discount_percentage" readonly="" class="form-control-plaintext discount_percentage" value="0" name="discount_percentage"  step="any"/>--}}
                                                <p id="discount_percentage" class="invoice-total-amount discount_percentage"> 0 %</p>
                                                    </div>
                                            <hr class="my-25" />
                                            <div class="col-sm-2 mt-2">
                                                {{--<p class="invoice-total-title">المبلغ الكلي:$1690</p>--}}
                                                <label class="form-label" for="staticprice">المبلغ الكلي</label>
                                                <input type="text" readonly="" name="total" class="form-control-plaintext all_total" id="all_total" value="0" step="any">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-1 row">
                                            <div class="col-sm-6">
                                                <label class="col-form-label" for="first-name">المبلغ المدفوع</label>
                                                <input type="number" id="paid" class="form-control" value="{{old('paid',0)}}" name="paid" placeholder="المبلغ المدفوع" step="any"/>
                                                @if($errors->has('paid'))
                                                    <span class="text-danger">{{ $errors->first('paid') }}</span>
                                                @endif
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="col-form-label" for="first-name">المبلغ المتبقي</label>
                                                <input type="number" id="residual" class="form-control" value="{{old('residual',0)}}" name="residual" placeholder="المبلغ المتبقي" step="any"/>
                                                @if($errors->has('residual'))
                                                    <span class="text-danger">{{ $errors->first('residual') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Invoice Total ends -->

                                        <div class="col-sm-9 offset-sm-3">
                                            <button type="submit" class="btn btn-primary me-1" >حفظ</button>
                                            <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                        </div>
                                    </form>
                            </div>
                        </div>
                    </div>
                    <!-- /Invoice repeater -->
                </div>
            </section>

        </div>
    </div>
@stop

@section('scripts')

    <script>
        var oldCountItem  = 0;
        var oldQuantity  = 0;
        var oldPrice  = 0;
        var oldPriceUnit  = 0;
        var suptotal = 0;
    function showItems(that) {

        var oldTo = 0 ;
        oldTo  = document.getElementById('total_before_dis').value;
        // var sum = 0;
        var sum = oldTo;
        // alert(sum);
        var itme_id = $(that).val();
        // console.log(itme_id);
        // if(itme_id){rl: 'item/'+itme_id,
        if(itme_id){
            $.ajax({
                url: 'item/'+itme_id,
                type:'get',
                success:function (data) {
                    // console.log(data);
                    if(data.status === 1){
                        that.parentNode.parentNode.parentNode.getElementsByClassName('unit_base')[0].getElementsByTagName('select')[0].value = data.data.unit_id
                        oldPrice = that.parentNode.parentNode.parentNode.getElementsByClassName('price_base')[0].getElementsByTagName('input')[0].value = data.data.price
                        oldPriceUnit = that.parentNode.parentNode.parentNode.getElementsByClassName('price_unit_base')[0].getElementsByTagName('input')[0].value = data.data.price_unit
                        oldCountItem =  that.parentNode.parentNode.parentNode.getElementsByClassName('count_item_base')[0].getElementsByTagName('input')[0].value = data.data.count_item
                        that.parentNode.parentNode.parentNode.getElementsByClassName('size_base')[0].getElementsByTagName('input')[0].value = data.data.	wright
                        that.parentNode.parentNode.parentNode.getElementsByClassName('company_base')[0].getElementsByTagName('input')[0].value = data.data.company.name
                        that.parentNode.parentNode.parentNode.getElementsByClassName('category_base')[0].getElementsByTagName('input')[0].value = data.data.category.name
                        that.parentNode.parentNode.parentNode.getElementsByClassName('min_stock_base')[0].getElementsByTagName('input')[0].value = data.data.min_stock
                        that.parentNode.parentNode.parentNode.getElementsByClassName('type_unit_base')[0].getElementsByTagName('input')[0].value = data.data.type_unit
                        oldQuantity =  that.parentNode.parentNode.parentNode.getElementsByClassName('qty_base')[0].getElementsByTagName('input')[0].value = 1

                        // $('#itemtotal').val(suptotal);
                       }
                    // oldQuantity =  that.parentNode.parentNode.parentNode.getElementsByClassName('qty_base')[0].getElementsByTagName('input')[0].value ;

                    suptotal = oldQuantity * oldPrice;
                    // sum += Math.round(100*suptotal)/100;
                    sum = parseFloat(oldTo) + parseFloat(suptotal);
                    // sum += parseFloat(suptotal);

                    $('#total_before_dis').val(sum);
                    $('#all_total').val(sum);
                    // total_ammount_price();
                },
                error: function (jqXhr, textStatus, errorMessage) { // error callback
                    alert(errorMessage);
                }
            });
        }
        else{
            $("#itemcost").empty();
            $("#staticprice").empty();
        }
    }

    // function total_ammount_price() {
    //     $('.itemprice').each(function(){
    //         var value = $(this).val();
    //         if(value.length != 0)
    //         {
    //             suptotal = oldQuantity * value;
    //             // sum += Math.round(100*suptotal)/100;
    //             sum += suptotal;
    //             // sum += parseFloat(suptotal);
    //         }
    //     });
    //     $('#total_before_dis').val(sum);
    //     $('#all_total').val(sum);
    // }

        var sum = 0;

 function showCount(that){
     var unitCount = $(that).val();
     var div = 1/oldCountItem;
     var sub = unitCount - oldCountItem;
     var price = that.parentNode.parentNode.parentNode.getElementsByClassName('price_base')[0].getElementsByTagName('input')[0].value ;
     if(sub != 0){
       var equale = unitCount*div;
        var roundEq = Math.round(10*equale)/10;
        var qunt = that.parentNode.parentNode.parentNode.getElementsByClassName('qty_base')[0].getElementsByTagName('input')[0].value = roundEq;
        suptotal = qunt * price;
        // suptotal = qunt * oldPrice;
        sum +=  Math.round(100 * suptotal) / 100;
        $('#total_before_dis').val(sum);
        $('#all_total').val(sum);
        // newTotalPrice(qunt);

    }
 }
 function showPriceItems(that){

     // var sum = 0;
     var priceItem = $(that).val();
     alert(priceItem.count());
     // oldCountItem
      var priceUnit = priceItem / oldCountItem ;
        // var qunt = that.parentNode.parentNode.parentNode.getElementsByClassName('qty_base')[0].getElementsByTagName('input')[0].value ;
        // alert(priceItem);
        // alert(qunt);
        // suptotal = qunt * priceItem;
        // sum =  Math.round(100 * suptotal) / 100;
     that.parentNode.parentNode.parentNode.getElementsByClassName('price_unit_base')[0].getElementsByTagName('input')[0].value  = Math.round(100 * priceUnit) / 100;
        // $('#total_before_dis').val(sum);
        // $('#all_total').val(sum);
        // newTotalPrice(qunt);

 }

 function showQuantityItems(that){
     var suptotal = 0;
     // alert(sum);
     var qunt = $(that).val();
     // var sub = qunt - oldQuantity;
     var price = that.parentNode.parentNode.parentNode.getElementsByClassName('price_base')[0].getElementsByTagName('input')[0].value ;
     // if(sub != 0){
         var equale = qunt * oldCountItem;
         that.parentNode.parentNode.parentNode.getElementsByClassName('count_item_base')[0].getElementsByTagName('input')[0].value = equale;
         suptotal = qunt * price;
         // suptotal = qunt * oldPrice;
         sum += Math.round(100 * suptotal) / 100;
         $('#total_before_dis').val(sum);
         $('#all_total').val(sum);

         // newTotalPrice(qunt);
     // }
 }

 function newTotalPrice (Quantity){
     var sum = 0;
     var suptotal = 0;

     $('.itemprice').each(function(){
         var value = $(this).val();
         if(value.length != 0)
         {
             suptotal = Quantity * value;
             sum += Math.round(100*suptotal)/100;

         }
     });
     $('#total_before_dis').val(sum);
     $('#all_total').val(sum);
 }

    $(document).ready(function(){
        $("#discount").on("input", function(){
            var total = document.getElementById('total_before_dis').value;
            var Discount = $(this).val();
          var sub = total - Discount;
          var div = (Discount / total)*100;
           document.getElementById('discount_percentage').innerText = Math.round(div)+ "%";
            // $("#discount_percentage").val(div);
            $("#all_total").val(sub);
        });

        $("#paid").on("input", function(){
            var total = document.getElementById('all_total').value;
            var Paid = $(this).val();
            var sub = total - Paid;
            $("#residual").val(sub);
        });
    });

    $(".supplier").change(function (e) {
            e.preventDefault();
            var supplier_id =  $(".supplier").val();
            // console.log(supplier_id);
        $('#delegate_id').html('');
            if(supplier_id){
                $.ajax({
                    url: 'delegate/'+supplier_id,
                    type:'get',
                    success:function (data) {
                        // console.log(data);
                        if(data.status === 1){
                            // console.log(data);
                            $('#delegate_id').html('<option value="">اختار المندوب</option>');
                            $.each(data.data, function (key, value) {
                                $('#delegate_id').append('<option value="' + value
                                    .id + '">' + value.name + '</option>');});
                        }
                    },
                    error: function (jqXhr, textStatus, errorMessage) { // error callback
                        alert(errorMessage);
                    }
                });
            }
            else{
                $("#delegate_id").empty();
            }
        });

    </script>
    @endsection