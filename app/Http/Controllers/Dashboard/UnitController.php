<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\UnitRequest;
use App\Models\Unit;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    public function index()
    {
        $units = Unit::latest()->get();
        return view('dashboard.Units.index',compact('units'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('dashboard.Units.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'    => 'required|string|unique:units',
            'notes' => 'nullable|string',
        ],
        [
            'name.required'=>'اسم الوحده مطلوب',
            'name.string'=>'يجب ان يكون اسم الوحده كلمه',
            'name.unique'=>'اسم الوحده موجود من قبل',
            'notes.string'=>'يجب ان يكون ملاحظه الوحده كلمات',
        ]);

        Unit::create([
            'name'=>$request->name,
            'notes'=>$request->notes,
        ]);
        return redirect()->route('units.index')->with('message', 'تم إضافه الوحده بنجاح');

    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $unit = Unit::whereId($id)->first();
        if ($unit){
            return view('dashboard.Units.edit',compact('unit'));
        }else{
            return back()->with('error', 'لا يوجد بيانات');

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UnitRequest $request, $id)
    {
        $unit = Unit::whereId($id)->first();
        if ($unit){
            $unit->update($request->validated());
            return redirect()->route('units.index')->with('message', 'تم تعديل الوحده بنجاح');

        }else{
            return back()->with('error', 'لا يوجد بيانات');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $unit = Unit::whereId($id)->first();
        if ($unit){
            $unit->delete();
            return redirect()->route('units.index')->with('message', 'تم حذف الوحده بنجاح');
        }else{
            return back()->with('error', 'لا يوجد بيانات');

        }
    }
}
