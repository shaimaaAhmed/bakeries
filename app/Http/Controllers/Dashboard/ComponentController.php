<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\ComponentRequest;
use App\Models\Component;
use App\Models\Item;
use App\Models\Product;
use Illuminate\Http\Request;

class ComponentController extends Controller
{

    public function index()
    {
        $components = Component::latest()->get();
        return view('dashboard.Components.index',compact('components'));

    }

    public function create()
    {
        $products = Product::get();
        $items = Item::get();
        return view('dashboard.Components.create',compact('products','items'));

    }


    public function store(ComponentRequest $request)
    {
        try{
            $product = Product::where('id',$request->product_id)->first();
       // update cost_price Product
            Component::create($request->validated()+[
//                'size_one_gram' => ,
                ]);
            return redirect()->route('components.index')->with('message', 'تم إضافه المكون بنجاح');

        }catch (\Exception $e){
            return back()->with('error', 'حدث خطأ في إرسال البيانات');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
