@extends('dashboard.layouts.master')

@section('title', 'إضافه وحده جديد')


@section('content')

    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">إداره المخابز</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('index')}}">الرئسيه</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('units.index')}}">الوحدات</a>
                                </li>
                                <li class="breadcrumb-item active"><a href="{{route('units.create')}}">إضافه وحده جديده</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                <div class="mb-1 breadcrumb-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                        <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body"><!-- Basic Horizontal form layout section start -->
            <section id="basic-horizontal-layouts">
                <div class="row">
                    <div class="col-md-12 col-12">
                        <div class="card">
                            @include('dashboard.includes.errors')
                            <div class="card-header">
                                <h4 class="card-title">إضافه وحده جديده</h4>
                            </div>
                            <div class="card-body">
                                <form class="form form-horizontal" action="{{route('units.store')}}" method="post">
                                   @csrf
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="col-sm-12">
                                                    <label class="col-form-label" for="first-name">اسم الوحده</label>
                                                    <input type="text" id="first-name" class="form-control" value="{{old('name')}}" name="name" placeholder="اسم الوحده" />
                                                    @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong style="color: red;">{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="mb-1 col-sm-12">
                                                    <label class="form-label" for="exampleFormControlTextarea1">ملاحظه</label>
                                                    <textarea class="form-control" name="notes" id="exampleFormControlTextarea1" rows="3" placeholder="ملاحظه">{{old('notes')}}</textarea>
                                                    @error('notes')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong style="color: red;">{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                                {{--<div class="col-sm-4">--}}
                                                    {{--<label class="col-form-label" for="password">ملاحظه</label>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-sm-8">--}}
                                                    {{--<textarea name="notes" placeholder="ملاحظه">{{old('notes')}}</textarea>--}}
                                                {{--</div>--}}
                                            </div>
                                        </div>
                                        {{--<div class="col-sm-9 offset-sm-3">--}}
                                            {{--<div class="mb-1">--}}
                                                {{--<div class="form-check">--}}
                                                    {{--<input type="checkbox" class="form-check-input" id="customCheck1" />--}}
                                                    {{--<label class="form-check-label" for="customCheck1">Remember me</label>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        <div class="col-sm-9 offset-sm-3">
                                            <button type="submit" class="btn btn-primary me-1" >حفظ</button>
                                            <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    {{--<div class="col-md-6 col-12">--}}
                        {{--<div class="card">--}}
                            {{--<div class="card-header">--}}
                                {{--<h4 class="card-title">Horizontal Form with Icons</h4>--}}
                            {{--</div>--}}
                            {{--<div class="card-body">--}}
                                {{--<form class="form form-horizontal">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-12">--}}
                                            {{--<div class="mb-1 row">--}}
                                                {{--<div class="col-sm-3">--}}
                                                    {{--<label class="col-form-label" for="fname-icon">First Name</label>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-sm-9">--}}
                                                    {{--<div class="input-group input-group-merge">--}}
                                                        {{--<span class="input-group-text"><i data-feather="user"></i></span>--}}
                                                        {{--<input--}}
                                                                {{--type="text"--}}
                                                                {{--id="fname-icon"--}}
                                                                {{--class="form-control"--}}
                                                                {{--name="fname-icon"--}}
                                                                {{--placeholder="First Name"--}}
                                                        {{--/>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-12">--}}
                                            {{--<div class="mb-1 row">--}}
                                                {{--<div class="col-sm-3">--}}
                                                    {{--<label class="col-form-label" for="email-icon">Email</label>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-sm-9">--}}
                                                    {{--<div class="input-group input-group-merge">--}}
                                                        {{--<span class="input-group-text"><i data-feather="mail"></i></span>--}}
                                                        {{--<input--}}
                                                                {{--type="email"--}}
                                                                {{--id="email-icon"--}}
                                                                {{--class="form-control"--}}
                                                                {{--name="email-id-icon"--}}
                                                                {{--placeholder="Email"--}}
                                                        {{--/>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-12">--}}
                                            {{--<div class="mb-1 row">--}}
                                                {{--<div class="col-sm-3">--}}
                                                    {{--<label class="col-form-label" for="contact-icon">Mobile</label>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-sm-9">--}}
                                                    {{--<div class="input-group input-group-merge">--}}
                                                        {{--<span class="input-group-text"><i data-feather="smartphone"></i></span>--}}
                                                        {{--<input--}}
                                                                {{--type="number"--}}
                                                                {{--id="contact-icon"--}}
                                                                {{--class="form-control"--}}
                                                                {{--name="contact-icon"--}}
                                                                {{--placeholder="Mobile"--}}
                                                        {{--/>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-12">--}}
                                            {{--<div class="mb-1 row">--}}
                                                {{--<div class="col-sm-3">--}}
                                                    {{--<label class="col-form-label" for="pass-icon">Password</label>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-sm-9">--}}
                                                    {{--<div class="input-group input-group-merge">--}}
                                                        {{--<span class="input-group-text"><i data-feather="lock"></i></span>--}}
                                                        {{--<input--}}
                                                                {{--type="password"--}}
                                                                {{--id="pass-icon"--}}
                                                                {{--class="form-control"--}}
                                                                {{--name="contact-icon"--}}
                                                                {{--placeholder="Password"--}}
                                                        {{--/>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-sm-9 offset-sm-3">--}}
                                            {{--<div class="mb-1">--}}
                                                {{--<div class="form-check">--}}
                                                    {{--<input type="checkbox" class="form-check-input" id="customCheck2" />--}}
                                                    {{--<label class="form-check-label" for="customCheck2">Remember me</label>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-sm-9 offset-sm-3">--}}
                                            {{--<button type="reset" class="btn btn-primary me-1">Submit</button>--}}
                                            {{--<button type="reset" class="btn btn-outline-secondary">Reset</button>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</form>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>
            </section>
            <!-- Basic Horizontal form layout section end -->
        </div>
    </div>


    {{--<div class="row" style="margin: 15px;">--}}
        {{--<div class="col-md-6">--}}

            {{--<!-- Basic layout-->--}}
            {{--<form action="{{ route('users.store') }}" class="form-horizontal" method="post"--}}
                  {{--enctype="multipart/form-data">--}}
                {{--@csrf--}}
                {{--<div class="panel panel-flat">--}}
                    {{--<div class="panel-heading">--}}
                        {{--<h5 class="panel-title"> اضافة عميل جديد </h5>--}}
                        {{--<div class="heading-elements">--}}
                            {{--<ul class="icons-list">--}}
                                {{--<li><a data-action="collapse"></a></li>--}}
                                {{--<li><a data-action="reload"></a></li>--}}
                                {{--<li><a data-action="close"></a></li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="panel-body">--}}



                        {{--<div class="form-group">--}}
                            {{--<label class="col-lg-3 control-label">{{ trans('dash.full_name') }}</label>--}}
                            {{--<div class="col-lg-9">--}}
                                {{--<input type="text" name="name" value="{{ old('full_name') }}" class="form-control"--}}
                                       {{--placeholder="{{ trans('dash.full_name') }}">--}}
                            {{--</div>--}}
                            {{--@error('name')--}}
                            {{--<span class="invalid-feedback" role="alert">--}}
                        	{{--<strong style="color: red;">{{ $message }}</strong>--}}
                        {{--</span>--}}
                            {{--@enderror--}}
                        {{--</div>--}}


                        {{--<div class="form-group">--}}
                            {{--<label class="col-lg-3 control-label">{{ trans('dash.email') }}</label>--}}
                            {{--<div class="col-lg-9">--}}
                                {{--<input type="email" name="email" class="form-control" value="{{ old('email') }}"--}}
                                       {{--placeholder="{{ trans('dash.email') }}" >--}}
                            {{--</div>--}}
                            {{--@error('email')--}}
                            {{--<span class="invalid-feedback" role="alert">--}}
                        	{{--<strong style="color: red;">{{ $message }}</strong>--}}
                        {{--</span>--}}
                            {{--@enderror--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<label class="col-lg-3 control-label">{{ trans('dash.phone') }}</label>--}}
                            {{--<div class="col-lg-9">--}}
                                {{--<input type="text" name="phone" value="{{ old('phone') }}" class="form-control"--}}
                                       {{--placeholder="{{ trans('dash.phone') }}">--}}
                            {{--</div>--}}
                            {{--@error('phone')--}}
                            {{--<span class="invalid-feedback" role="alert">--}}
                        	{{--<strong style="color: red;">{{ $message }}</strong>--}}
                        {{--</span>--}}
                            {{--@enderror--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<label class="col-lg-3 control-label">{{ trans('dash.address') }}</label>--}}
                            {{--<div class="col-lg-9">--}}
                                {{--<input type="text" name="address" value="{{ old('address') }}" class="form-control"--}}
                                       {{--placeholder="{{ trans('dash.address') }}">--}}
                            {{--</div>--}}
                            {{--@error('address')--}}
                            {{--<span class="invalid-feedback" role="alert">--}}
                        	{{--<strong style="color: red;">{{ $message }}</strong>--}}
                        {{--</span>--}}
                            {{--@enderror--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<label class="col-lg-3 control-label">{{ trans('dash.gender') }}</label>--}}
                            {{--<div class="col-lg-9">--}}
                                {{--<select name="gander" class="select-border-color border-warning">--}}
                                    {{--<option value="male">{{trans('dash.male')}}</option>--}}
                                    {{--<option value="female">{{trans('dash.female')}}</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                            {{--@error('gander')--}}
                            {{--<span class="invalid-feedback" role="alert">--}}
                        	{{--<strong style="color: red;">{{ $message }}</strong>--}}
                        {{--</span>--}}
                            {{--@enderror--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<label class="col-lg-3 control-label"> {{ trans('dash.image') }}</label>--}}
                            {{--<div class="col-lg-9">--}}
                                {{--<input type="file" class="file-styled" name="image">--}}
                                {{--<span class="help-block"> التنسيقات المقبولة: gif, png, jpg. Max file size 2Mb</span>--}}
                            {{--</div>--}}
                            {{--@error('image')--}}
                            {{--<span class="invalid-feedback" role="alert">--}}
                        	{{--<strong style="color: red;">{{ $message }}</strong>--}}
                        {{--</span>--}}
                            {{--@enderror--}}
                        {{--</div>--}}


                        {{--<div class="form-group">--}}
                            {{--<label class="col-lg-3 control-label"> {{ trans('dash.password') }} </label>--}}
                            {{--<div class="col-lg-9">--}}
                                {{--<input type="password" class="form-control" name="password"--}}
                                       {{--placeholder=" {{ trans('dash.password') }} " />--}}
                            {{--</div>--}}
                            {{--@error('password')--}}
                            {{--<span class="invalid-feedback" role="alert">--}}
                        	{{--<strong style="color: red;">{{ $message }}</strong>--}}
                        {{--</span>--}}
                            {{--@enderror--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<label class="col-lg-3 control-label"> {{ trans('dash.confirm_password') }} </label>--}}
                            {{--<div class="col-lg-9">--}}
                                {{--<input type="password" class="form-control" name="password_confirmation"--}}
                                       {{--placeholder=" {{ trans('dash.confirm_password') }} " />--}}
                            {{--</div>--}}
                            {{--@error('password_confirmation')--}}
                            {{--<span class="invalid-feedback" role="alert">--}}
                        	{{--<strong style="color: red;">{{ $message }}</strong>--}}
                        {{--</span>--}}
                            {{--@enderror--}}
                        {{--</div>--}}


                        {{--<div class="text-right">--}}
                            {{--<input type="submit" class="btn btn-primary"--}}
                                   {{--value=" {{ trans('dash.added_and_forward_to_list') }} "/>--}}
                            {{--<input type="submit" class="btn btn-success" name="back" value=" {{ trans('dash.add_and_come_back') }} " />--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</form>--}}
            {{--<!-- /basic layout -->--}}

        {{--</div>--}}


        {{--<div class="col-md-6">--}}
            {{--<div class="panel panel-flat">--}}

                {{--<div class="panel-heading">--}}
                    {{--<h5 class="panel-title"> {{ trans('dash.user.last') }} </h5>--}}
                    {{--<div class="heading-elements">--}}
                        {{--<ul class="icons-list">--}}
                            {{--<li><a data-action="collapse"></a></li>--}}
                            {{--<li><a data-action="reload"></a></li>--}}
                            {{--<li><a data-action="close"></a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{--<div class="panel-body">--}}

                    {{--<table class="table table-bordered table-hover">--}}
                        {{--<tr class="text-center">--}}
                            {{--<th> {{ trans('dash.full_name') }} </th>--}}
                            {{--<th> {{ trans('dash.email') }} </th>--}}
                            {{--<th> {{ trans('dash.image') }} </th>--}}
                        {{--</tr>--}}
                        {{--@forelse($last_users as $user)--}}
                            {{--<tr>--}}
                                {{--<td> {{ $user->name }} </td>--}}
                                {{--<td> {{ $user->email }} </td>--}}
                               {{--</tr>--}}
                        {{--@empty--}}
                            {{--<tr><td class="alert alert-info">لا يوجد مستخدمين</td></tr>--}}
                        {{--@endforelse--}}
                    {{--</table>--}}
                {{--</div>--}}

            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
@stop
