@extends('dashboard.layouts.master')

@section('title', 'تعديل وحده ')


@section('content')

    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">إداره المخابز</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('index')}}">الرئسيه</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('units.index')}}">الوحدات</a>
                                </li>
                                <li class="breadcrumb-item active"><a href="{{route('units.edit',$unit->id)}}">تعديل وحده </a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                <div class="mb-1 breadcrumb-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                        <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body"><!-- Basic Horizontal form layout section start -->
            <section id="basic-horizontal-layouts">
                <div class="row">
                    <div class="col-md-12 col-12">
                        <div class="card">
                            @include('dashboard.includes.errors')
                            <div class="card-header">
                                <h4 class="card-title">تعديل وحده {{$unit->name}}</h4>
                            </div>
                            <div class="card-body">
                                <form class="form form-horizontal" action="{{route('units.update',$unit->id)}}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <input name="id" value="{{$unit->id}}" type="hidden">
                                                <div class="col-sm-12">
                                                    <label class="col-form-label" for="first-name">اسم الوحده</label>
                                                    <input type="text" id="first-name" class="form-control" value="{{$unit->name}}" name="name" placeholder="اسم الوحده" />
                                                    @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong style="color: red;">{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="mb-1 col-sm-12">
                                                    <label class="form-label" for="exampleFormControlTextarea1">ملاحظه</label>
                                                    <textarea class="form-control" name="notes" id="exampleFormControlTextarea1" rows="3" placeholder="ملاحظه">{{$unit->notes}}</textarea>
                                                    @error('notes')
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong style="color: red;">{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-sm-9 offset-sm-3">
                                            <button type="submit" class="btn btn-primary me-1" >حفظ</button>
                                            <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    {{--<div class="col-md-6 col-12">--}}
                    {{--<div class="card">--}}
                    {{--<div class="card-header">--}}
                    {{--<h4 class="card-title">Horizontal Form with Icons</h4>--}}
                    {{--</div>--}}
                    {{--<div class="card-body">--}}
                    {{--<form class="form form-horizontal">--}}
                    {{--<div class="row">--}}
                    {{--<div class="col-12">--}}
                    {{--<div class="mb-1 row">--}}
                    {{--<div class="col-sm-3">--}}
                    {{--<label class="col-form-label" for="fname-icon">First Name</label>--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-9">--}}
                    {{--<div class="input-group input-group-merge">--}}
                    {{--<span class="input-group-text"><i data-feather="user"></i></span>--}}
                    {{--<input--}}
                    {{--type="text"--}}
                    {{--id="fname-icon"--}}
                    {{--class="form-control"--}}
                    {{--name="fname-icon"--}}
                    {{--placeholder="First Name"--}}
                    {{--/>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-12">--}}
                    {{--<div class="mb-1 row">--}}
                    {{--<div class="col-sm-3">--}}
                    {{--<label class="col-form-label" for="email-icon">Email</label>--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-9">--}}
                    {{--<div class="input-group input-group-merge">--}}
                    {{--<span class="input-group-text"><i data-feather="mail"></i></span>--}}
                    {{--<input--}}
                    {{--type="email"--}}
                    {{--id="email-icon"--}}
                    {{--class="form-control"--}}
                    {{--name="email-id-icon"--}}
                    {{--placeholder="Email"--}}
                    {{--/>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-12">--}}
                    {{--<div class="mb-1 row">--}}
                    {{--<div class="col-sm-3">--}}
                    {{--<label class="col-form-label" for="contact-icon">Mobile</label>--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-9">--}}
                    {{--<div class="input-group input-group-merge">--}}
                    {{--<span class="input-group-text"><i data-feather="smartphone"></i></span>--}}
                    {{--<input--}}
                    {{--type="number"--}}
                    {{--id="contact-icon"--}}
                    {{--class="form-control"--}}
                    {{--name="contact-icon"--}}
                    {{--placeholder="Mobile"--}}
                    {{--/>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-12">--}}
                    {{--<div class="mb-1 row">--}}
                    {{--<div class="col-sm-3">--}}
                    {{--<label class="col-form-label" for="pass-icon">Password</label>--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-9">--}}
                    {{--<div class="input-group input-group-merge">--}}
                    {{--<span class="input-group-text"><i data-feather="lock"></i></span>--}}
                    {{--<input--}}
                    {{--type="password"--}}
                    {{--id="pass-icon"--}}
                    {{--class="form-control"--}}
                    {{--name="contact-icon"--}}
                    {{--placeholder="Password"--}}
                    {{--/>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-9 offset-sm-3">--}}
                    {{--<div class="mb-1">--}}
                    {{--<div class="form-check">--}}
                    {{--<input type="checkbox" class="form-check-input" id="customCheck2" />--}}
                    {{--<label class="form-check-label" for="customCheck2">Remember me</label>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-9 offset-sm-3">--}}
                    {{--<button type="reset" class="btn btn-primary me-1">Submit</button>--}}
                    {{--<button type="reset" class="btn btn-outline-secondary">Reset</button>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</form>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                </div>
            </section>
            <!-- Basic Horizontal form layout section end -->
        </div>
    </div>
@stop
