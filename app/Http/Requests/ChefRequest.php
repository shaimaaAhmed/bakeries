<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChefRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|unique:users,email,'.$this->id,
            'image' => 'nullable|image',
            'titel_job' => 'required',
            'shift' => 'required',
            'notes' => 'sometimes',
//            'mobile' => 'sometimes',
            'password' => 'nullable|min:8',
//            'password' => 'required|min:8',
            'phone' => 'sometimes|nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:11',
            'mobile' => 'required_if:phone,=,null|nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:11',

        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'اسم الشيف مطلوب',
            'titel_job.required' => 'وظيفه الشيف مطلوب',
            'shift.required' => 'شيفت الشيف مطلوب',
            'email.required' => 'البريد الأاكتروني مطلوب',
//            'password.required' => 'كلمه السر مطلوب',
            'password.min' => 'عدد حروف كلمه السر لا تقل عن 8 احرف',
            'email.unique' => 'البريد الالكتروني موجود من قبل يجب اضافه بريد اخر',
             'mobile.required_if'=> 'يجب ادخال رقم الهاتف او الموبايل ',
            'mobile.numeric'=> 'يجب ان يكون الموبايل رقما',
            'phone.numeric'=> 'يجب ان يكون رقم الهاتف رقما',
        ];
    }

}