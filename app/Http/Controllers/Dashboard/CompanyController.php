<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyRequest;
use App\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function index()
    {
        $companies = Company::latest()->get();
        return view('dashboard.Companies.index',compact('companies'));
    }


    public function create()
    {
        return view('dashboard.Companies.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'    => 'required|string|unique:companies',
            'notes' => 'nullable|string',
        ],
            [
                'name.required'=>'اسم الشركه مطلوب',
                'name.string'=>'يجب ان يكون اسم الشركه كلمه',
                'name.unique'=>'اسم الشركه موجود من قبل',
                'notes.string'=>'يجب ان يكون ملاحظه الشركه كلمات',
            ]);

        Company::create([
            'name'=>$request->name,
            'notes'=>$request->notes,
        ]);
        return redirect()->route('companies.index')->with('message', 'تم إضافه الشركه بنجاح');

    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $company = Company::whereId($id)->first();
        if ($company){
            return view('dashboard.Companies.edit',compact('company'));
        }else{
            return back()->with('error', 'لا يوجد بيانات');

        }
    }

    public function update(CompanyRequest $request, $id)
    {
        $company = Company::whereId($id)->first();
        if ($company){
            $company->update($request->validated());
            return redirect()->route('companies.index')->with('message', 'تم تعديل الشركه بنجاح');

        }else{
            return back()->with('error', 'لا يوجد بيانات');

        }
    }

    public function destroy($id)
    {
        $company = Company::whereId($id)->first();
        if ($company){
            $company->delete();
            return redirect()->route('companies.index')->with('message', 'تم حذف الشركه بنجاح');
        }else{
            return back()->with('error', 'لا يوجد بيانات');

        }
    }
}
