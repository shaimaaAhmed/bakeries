<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChefRequest;
use App\Models\Chef;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Exception ;

class ChefController extends Controller
{
    public function index()
    {
       $chefs = Chef::latest()->get();
       return view('dashboard.Chefs.index', compact('chefs'));
    }


    public function create()
    {
        return view('dashboard.Chefs.create');
    }

    public function store(ChefRequest $request)
    {
        try{
            $request->except(['image']);
            $image ='';
            if ($request->image) $image =  uploaded($request->image, 'chef');
            $chef = Chef::create($request->validated());
            $chef->update([
                'image' =>$image,
            ]);
            $chef->save();
            return redirect()->route('chefs.index')->with('success', 'تم اضافه الشيف بنجاح');

        }
        catch (\Exception $er){
//            return $er;
            return redirect()->route('chefs.index')->with('error', 'حدث خطأ يرجي المحاوله مره اخري');
        }

    }




    public function show($id)
    {
        $chef = Chef::find($id);
        if (!$chef) {
            return redirect()->route('chefs.index')->with('error', 'لا يوجد شيف بهذه البيانات' );
        }
        return view('dashboard.Chefs.show', compact('chef'));

    }


    public function edit($id)
    {
        try{
            $chef = Chef::find($id);
            if (!$chef) {
                return redirect()->route('chefs.index')->with('error','لا يوجد شيف بهذه البيانات');
            }

            return view('dashboard.Chefs.edit', compact('chef'));
        }
        catch (\Exception $ex){

            return redirect()->route('chefs.index')->with('error', 'حدث خطأ يرجي المحاوله مره اخري');
        }

    }


    public function update(ChefRequest $request, $id)
    {
        try{
            $chef = Chef::find($id);
            $request_data = $request->except(['password','image']);


            if ($request->has('password')){
                $request_data['password'] = $request->password;
            }

            if ($request->has('image'))
            {
                File::delete(public_path('uploads/chefs/' . $chef->image));

                $request_data['image'] = uploaded($request->image, 'chef');
            }
            $chef->update($request_data);
            return redirect()->route('chefs.index')->with('message', 'تم التعديل  بنجاح');
        }

        catch (\Exception $ex){
            return redirect()->route('chefs.index')->with('error', 'حدث خطأ يرجي المحاوله مره اخري');
        }
    }


    public function destroy($id)
    {
        $chef = Chef::find($id);

        if(!$chef) return back()->with('error', 'لا يوجد بيانات');
        try
        {
            File::delete(public_path('uploads/chefs/' . $chef->image));
            $chef->delete();
            return redirect()->route('chefs.index')->with('message', 'تم حذف الشيف بنجاح');
            }
        catch (Exception $e) {
            return back()->with('error', 'لا يوجد بيانات');

        }

    }
}
