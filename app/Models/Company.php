<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $guarded =[];

    public function items(){
        return $this->hasMany(Item::class );
    }

    public function suppliers(){
        return $this->hasMany(Supplier::class );
    }
}
