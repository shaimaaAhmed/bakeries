@extends('dashboard.layouts.master')

@section('title', 'إضافه تاجر جديد')

@section('style')

    <link rel="stylesheet" type="text/css" href="{{asset('dashboard/app-assets/vendors/css/forms/select/select2.min.css')}}">

@endsection

@section('content')

    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">إداره المخابز</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('index')}}">الرئسيه</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('suppliers.index')}}">التجار</a>
                                </li>
                                <li class="breadcrumb-item active"><a href="{{route('suppliers.create')}}">إضافه تاجر جديد</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                <div class="mb-1 breadcrumb-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                        <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body"><!-- Basic Horizontal form layout section start -->
            <section id="basic-horizontal-layouts">
                <div class="row">
                    <div class="col-md-12 col-12">
                        <div class="card">
                            @include('dashboard.includes.errors')
                            <div class="card-header">
                                <h4 class="card-title">إضافه تاجر جديد</h4>
                            </div>
                            <div class="card-body">
                                <form class="form form-horizontal" action="{{route('suppliers.store')}}" method="post">
                                   @csrf
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="first-name">اسم التاجر</label>
                                                    <input type="text" id="first-name" class="form-control" value="{{old('name')}}" name="name" placeholder="اسم التاجر" />
                                                    @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong style="color: red;">{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="first-name">رقم التاجر</label>
                                                    <input type="text" id="first-name" class="form-control" value="{{old('phone')}}" name="phone" placeholder="رقم التاجر" />
                                                    @error('phone')
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong style="color: red;">{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="col-md-12" data-select2-id="46">
                                            <label class="form-label" for="select2-basic">اختار الشركه</label>
                                            <select class="select2 form-select" id="select2-basic" name="company_id">
                                               <option></option>
                                                @forelse($companies as $company)
                                                    <option value="{{$company->id}}">{{$company->name}}</option>
                                                @empty
                                                    <option> لايوجد بيانات</option>
                                                @endforelse
                                            </select>
                                            @error('company_id')
                                            <span class="invalid-feedback" role="alert">
                                                            <strong style="color: red;">{{ $message }}</strong>
                                                        </span>
                                            @enderror
                                        </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="mb-1 col-sm-12">
                                                    <label class="form-label" for="exampleFormControlTextarea1">ملاحظه</label>
                                                    <textarea class="form-control" name="notes" id="exampleFormControlTextarea1" rows="3" placeholder="ملاحظه">{{old('notes')}}</textarea>
                                                    @error('notes')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong style="color: red;">{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        {{--<div class="col-sm-9 offset-sm-3">--}}
                                            {{--<div class="mb-1">--}}
                                                {{--<div class="form-check">--}}
                                                    {{--<input type="checkbox" class="form-check-input" id="customCheck1" />--}}
                                                    {{--<label class="form-check-label" for="customCheck1">Remember me</label>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        <div class="col-sm-9 offset-sm-3">
                                            <button type="submit" class="btn btn-primary me-1" >حفظ</button>
                                            <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Basic Horizontal form layout section end -->
        </div>
    </div>

@stop
@section('script')
    <script src="{{asset('dashboard/app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>

    <script src="{{asset('dashboard/app-assets/js/scripts/forms/form-select2.min.js')}}"></script>
    @endsection