<?php

use Illuminate\Support\Arr;
use Illuminate\Support\Str;


function str()
{
    return new \Illuminate\Support\Str();
}

function floating($right, $left)
{
    return app()->isLocale('ar') ? $right : $left;
}
function isLocalised($lang) { return LaravelLocalization::getLocalizedURL($lang); }
function direction() { return app()->isLocale('ar') ? 'rtl' : 'ltr'; }
function isNullable($text){ return (!isset($text) || $text == null || $text == '') ? trans('back.no-value') : ucwords($text); }


function cruds()
{
    return ['admins','users','reviewers','pages'];
}

function permission_route_checker($route)
{
    $auth = auth()->user();

    if($auth->admin_group_id == 1) return true;

    return in_array($route, json_decode($auth->admin_group->permissions));
}



function getModelCount($model, $withDeleted = false)
{
    if($withDeleted)
    {
        if($model == 'admin') return \App\Models\User::onlyTrashed()->where('is_super_Admin', '!=', 1)->count();
        $mo = "App\\Models\\".ucwords($model);
        return $mo::onlyTrashed()->count();
    }

    if($model == 'admin') return \App\Models\User::where('is_super_Admin', '!=', 1)->count();

    $mo = "App\\Models\\".ucwords($model);

    return $mo::count();
}


function getImage($type, $img)
{
    if($type == 'users')
        return ($img != null) ? url('public/uploads/default.png/'.$img) : asset('public/uploads/default.png');

    elseif($type == 'admins')
        return ($img != null) ? url('public/uploads/default.png/'.$img) : asset('public/uploads/default.png');

    else
        return asset('public/uploads/default.png');
}

function array_except($array, $keys)
{
    return Arr::except($array, $keys);
}

function uploaded($img, $model)
{
    $file = $img;
    $filename= date('YmdHi').$file->getClientOriginalName();
    if (!file_exists(public_path('uploads/'.Str::plural($model).'/')))
        mkdir(public_path('uploads/'.Str::plural($model).'/'), 0777, true);

    $file-> move(public_path('uploads/'.Str::plural($model).'/'), $filename);
    return $filename;

}

function checkIfHasRole($group, $crud, $type)
{
    if($type == 'delete') return in_array('ajax-delete-'.str()->singular($crud), $group->permissions_arr);

    return in_array($crud.'.'.$type, $group->permissions_arr);
}

