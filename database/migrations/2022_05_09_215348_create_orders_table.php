<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->time('time');
            $table->date('date');
            $table->integer('qty');
            $table->double('price');
            $table->double('discount')->default(0);
            $table->double('total');
            $table->enum('status',['تم الإرسال','جاري التجهيز','جاري الانتظار','إلغاء'])->default('جاري الانتظار');
            $table->foreignId('chef_id')
                ->constrained('chefs')->onDelete('cascade');
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade');
            $table->foreignId('product_id')->constrained('products')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
