@extends('dashboard.layouts.master')

@section('title', 'إضافه شيف جديد')


@section('content')

    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">إداره المخابز</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('index')}}">الرئسيه</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('chefs.index')}}">الشيفات</a>
                                </li>
                                <li class="breadcrumb-item active"><a href="{{route('chefs.create')}}">إضافه شيف جديده</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                <div class="mb-1 breadcrumb-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                        <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body"><!-- Basic Horizontal form layout section start -->
            <section id="basic-horizontal-layouts">
                <div class="row">
                    <div class="col-md-12 col-12">
                        <div class="card">
                            @include('dashboard.includes.errors')
                            <div class="card-header">
                                <h4 class="card-title">إضافه شيف جديده</h4>
                            </div>
                            <div class="card-body">
                                <form class="form form-horizontal" action="{{route('chefs.store')}}" method="post"  enctype="multipart/form-data">
                                   @csrf
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="first-name">اسم الشيف</label>
                                                    <input type="text" id="first-name" class="form-control" value="{{old('name')}}" name="name" placeholder="اسم الشيف" />

                                                    @if($errors->has('name'))
                                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="email"> البريد الإلكتروني</label>
                                                    <input type="email" id="email" class="form-control" value="{{old('email')}}" name="email" placeholder="بريد الإلكتروني" />

                                                    @if($errors->has('email'))
                                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="titel_job">وظيفه الشيف</label>
                                                    <input type="text" id="titel_job" class="form-control" value="{{old('titel_job')}}" name="titel_job" placeholder="وظيفه الشيف" />

                                                    @if($errors->has('titel_job'))
                                                        <span class="text-danger">{{ $errors->first('titel_job') }}</span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="shift">شيفت الشيف</label>
                                                    <input type="text" id="shift" class="form-control" value="{{old('shift')}}" name="shift" placeholder="شيفت الشيف" />

                                                    @if($errors->has('shift'))
                                                        <span class="text-danger">{{ $errors->first('shift') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="phone">تلفون الشيف</label>
                                                    <input type="text" id="phone" class="form-control" value="{{old('phone')}}" name="phone" placeholder="تلفون الشيف" />

                                                    @if($errors->has('phone'))
                                                        <span class="text-danger">{{ $errors->first('phone') }}</span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="mobile">موبيل الشيف</label>
                                                    <input type="text" id="mobile" class="form-control" value="{{old('mobile')}}" name="mobile" placeholder="موبيل الشيف" />

                                                    @if($errors->has('mobile'))
                                                        <span class="text-danger">{{ $errors->first('mobile') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="password">كلمه السر الشيف</label>
                                                    <input type="password" id="password" class="form-control" value="{{old('password')}}" name="password" placeholder="كلمه السر الشيف" />

                                                    @if($errors->has('password'))
                                                        <span class="text-danger">{{ $errors->first('password') }}</span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="formFile">صوره الشيف</label>
                                                    <input type="file" id="formFile" class="form-control" value="{{old('image')}}" name="image" placeholder="صوره الشيف" />

                                                    @if($errors->has('image'))
                                                        <span class="text-danger">{{ $errors->first('image') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="mb-1 col-sm-12">
                                                    <label class="form-label" for="exampleFormControlTextarea1">ملاحظه</label>
                                                    <textarea class="form-control" name="notes" id="exampleFormControlTextarea1" rows="3" placeholder="ملاحظه">{{old('notes')}}</textarea>
                                                    @if($errors->has('notes'))
                                                        <span class="text-danger">{{ $errors->first('notes') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-9 offset-sm-3">
                                            <button type="submit" class="btn btn-primary me-1" >حفظ</button>
                                            <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <!-- Basic Horizontal form layout section end -->
        </div>
    </div>

@stop
