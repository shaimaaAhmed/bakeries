<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\ItemRequest;
use App\Models\Category;
use App\Models\Company;
use App\Models\Item;
use App\Models\Unit;
use Illuminate\Http\Request;

class ItemController extends Controller
{
   
    public function index()
    {
        $items = Item::latest()->get();
        return view('dashboard.Items.index',compact('items'));
    }
    
    public function create()
    {
        $units = Unit::get();
        $categories = Category::get();
        $companies = Company::get();
        return view('dashboard.Items.create',compact('units','categories','companies'));
    }

    public function store(ItemRequest $request)
    {
//        $wright = 0;
//        $name = $request->name;
//        $size = $request->size;
//        if ($name == "زيت" && $size == 900){
//            $wright = 900*$count_item;
//        }else{
//            $wright = $size*1000*$count_item ;
//        }

        $count_item = $request->count_item;
        $value = $request->price / $count_item;
        $price_unit= round($value, 2);
//         Item::create($request->validated());
        Item::create($request->validated()+[
//            'wright' => $wright,
            'price_unit' => $price_unit,
            ]);
        return redirect()->route('items.index')->with('message', 'تم إضافه الصنف بنجاح');

    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $units = Unit::get();
        $companies = Company::get();
        $categories = Category::get();
        $item = Item::whereId($id)->first();
        if ($item){
            return view('dashboard.Items.edit',compact('item','units','companies','categories'));
        }else{
            return back()->with('error', 'لا يوجد بيانات');

        }
    }

    
    public function update(ItemRequest $request, $id)
    {
        $item = Item::whereId($id)->first();
        if ($item){

//            $wright = 0;
//            $name = $request->name;
//            $size = $request->size;
            $count_item = $request->count_item;
//            if ($name == "زيت" && $size == 900){
//                $wright = 900*$count_item;
//            }else{
//                $wright = $size*1000*$count_item ;
//            }
            $value = $request->price / $count_item;
            $price_unit= round($value, 2);
            $item->update($request->validated()+[
//                    'wright' => $wright,
                    'price_unit' => $price_unit,
                ]);
//            $item->update($request->validated());
            return redirect()->route('items.index')->with('message', 'تم تعديل الصنف بنجاح');

        }else{
            return back()->with('error', 'لا يوجد بيانات');

        }
    }

   
    public function destroy($id)
    {
        $item = Item::whereId($id)->first();
        if ($item){
            $item->delete();
            return redirect()->route('items.index')->with('message', 'تم حذف الصنف بنجاح');
        }else{
            return back()->with('error', 'لا يوجد بيانات');

        }
    }
}
