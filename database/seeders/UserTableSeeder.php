<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_groups')->insert([
            'id'             => 1,
            'name'           => 'المدير',
            'permissions'    => '*',
            'description'    => 'المدير',
            'created_at'     => now(),
            'updated_at'     => now(),
        ]);

        DB::table('users')->insert([
            'id'                  => 1,
            'name'           => 'Super Admin',
            'email'               => 'admin@admin.com',
            'is_super_admin'      => 1,
            'password'            => bcrypt('123456'),
            'phone'              => '012346978',
            'admin_group_id'      =>1,
            'created_at'          => now(),
            'updated_at'          => now(),
        ]);

    }
}
