@extends('dashboard.layouts.master')

@section('title', 'أضافه مكونات المنتج')

@section('content')
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">إداره المخابز</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('index')}}">الرئسيه</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('invoices.index')}}">مكونات المنتجات</a>
                                </li>
                                    <li class="breadcrumb-item active"><a href="{{route('product.create')}}">أضافه مكونات المنتج</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                <div class="mb-1 breadcrumb-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                        <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body"><section class="form-control-repeater">
                <div class="row">
                    <!-- Invoice repeater -->
                    <div class="col-12">
                        <div class="card">
                            @include('dashboard.includes.errors')
                            <div class="card-header">
                                <h4 class="card-title">أضافه مكونات المنتج</h4>
                            </div>
                            <div class="card-body">
                                <form class="form form-horizontal invoice-repeater" action="{{route('product.store')}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="first-name">اسم المنتج</label>
                                                    <input type="text" id="first-name" class="form-control" value="{{old('name')}}" name="name" placeholder="اسم المنتج" />
                                                    @if($errors->has('name'))
                                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="first-name">كود المنتج</label>
                                                    <input type="number" id="first-name" class="form-control" value="{{old('code')}}" name="code" placeholder="كود المنتج" />
                                                    @if($errors->has('code'))
                                                        <span class="text-danger">{{ $errors->first('code') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="col-md-6 col-6">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="staticwright">وزن المنتج</label>
                                                        <input type="text" name="wright" class="form-control" id="staticwright" aria-describedby="staticwright" placeholder="وزن المنتج" step="any" />
                                                        @if($errors->has('wright'))
                                                            <span class="text-danger">{{ $errors->first('wright') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-6">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="staticsize">حجم المنتج</label>
                                                        <input type="text" name="size" class="form-control" id="staticsize" aria-describedby="staticsize" placeholder="حجم المنتج" step="any" />
                                                        @if($errors->has('size'))
                                                            <span class="text-danger">{{ $errors->first('size') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="col-md-6 col-6">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="staticnum_pieces">عدد القطع</label>
                                                        <input type="text" name="num_pieces" class="form-control" id="staticnum_pieces" aria-describedby="staticnum_pieces" placeholder="عدد القطع" step="any" />
                                                        @if($errors->has('num_pieces'))
                                                            <span class="text-danger">{{ $errors->first('num_pieces') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-6">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="staticunit_type">نوع الوحده</label>
                                                        <input type="text" name="unit_type" class="form-control" id="staticunit_type" aria-describedby="staticunit_type" placeholder="نوع الوحده" step="any" />
                                                        @if($errors->has('unit_type'))
                                                            <span class="text-danger">{{ $errors->first('unit_type') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="col-md-6 col-6">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="staticselling_price">سعر البيع</label>
                                                        <input type="text" name="selling_price" class="form-control" id="staticselling_price" aria-describedby="staticselling_price" placeholder="سعر البيع" step="any" />
                                                        @if($errors->has('selling_price'))
                                                            <span class="text-danger">{{ $errors->first('selling_price') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-6">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="staticwholesale_price">سعر الجمله</label>
                                                        <input type="text" name="wholesale_price" class="form-control" id="staticwholesale_price" aria-describedby="staticwholesale_price" placeholder="سعر الجمله" step="any" />
                                                        @if($errors->has('wholesale_price'))
                                                            <span class="text-danger">{{ $errors->first('wholesale_price') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1 row">
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="formFile">صوره المنتج</label>
                                                    <input type="file" id="formFile" class="form-control" value="{{old('image')}}" name="image" placeholder="صوره الشيف" />

                                                    @if($errors->has('image'))
                                                        <span class="text-danger">{{ $errors->first('image') }}</span>
                                                    @endif
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="col-form-label" for="">إجمالي وزن المكونات</label>
                                                    <input type="number" class="form-control" step="any" name="total_weight_comp" value="{{old('total_weight_comp')}}" placeholder="إجمالي وزن المكونات">
                                                    @if($errors->has('total_weight_comp'))
                                                        <span class="text-danger">{{ $errors->first('total_weight_comp') }}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <label class="form-label" for="exampleFormControlTextarea1">الوصف</label>
                                            <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3" placeholder="الوصف">{{old('description')}}</textarea>

                                            @if($errors->has('description'))
                                                <span class="text-danger">{{ $errors->first('description') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="mb-2">
                                        <h3>مكونات المنتج</h3>
                                    </div>
                                    <div data-repeater-list="invoice">
                                        <div data-repeater-item class="invoice_add">
                                            <div class="row d-flex align-items-end">
                                                <div class="col-md-2 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="itemname">إختار إسم الصنف</label>
                                                        {{--<select class="form-control" id="itemname" aria-describedby="itemname" name="item_id[]">--}}
                                                        <select class="form-control item select2 form-select" id="itemname" aria-describedby="itemname" name="item_id[]" onchange="showItems(this)" required>
                                                            <option></option>
                                                            @foreach($items as $item)
                                                                <option value="{{$item->id}}" {{old('item_id')== $item->id ? 'selected' :''}}>{{$item->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        @if($errors->has('item_id[]'))
                                                            <span class="text-danger">{{ $errors->first('item_id[]') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-1 col-12 company_base" >
                                                    <div class="mb-1">
                                                        <label class="form-label" for="companyname"> إسم الشركه</label>
                                                        <input name="company" type="text" readonly="" value="{{old('company')}}" class="form-control-plaintext" placeholder="الشركه">
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-12 size_base">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="size_base">حجم الصنف المستهلك</label>
                                                        <input type="text"  name="size" class="form-control" id="size_base" value="{{old('size',0)}}" step="any" onchange="showPriceItems(this)">
                                                        @if($errors->has('size'))
                                                            <span class="text-danger">{{ $errors->first('size') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-12 price_gram_base">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="staticprice_gram">السعر</label>
                                                        <input type="text" name="price_gram[]" value="{{old('price_gram')}}" class="form-control itemprice_gram" id="staticprice_gram" aria-describedby="staticprice_gram" placeholder="السعر" step="any" />
                                                        @if($errors->has('price_gram[]'))
                                                            <span class="text-danger">{{ $errors->first('price_gram[]') }}</span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-md-1 col-12 qty_base">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="itemquantity">الكميه</label>
                                                        <input value="{{old('qty',1)}}"
                                                               type="number"
                                                               class="form-control itemquantity"
                                                               id="itemquantity"
                                                               aria-describedby="itemquantity"
                                                               placeholder="1"
                                                               name="qty[]"
                                                               onchange="showQuantityItems(this)"
                                                        />
                                                        @if($errors->has('qty[]'))
                                                            <span class="text-danger">{{ $errors->first('qty[]') }}</span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-md-1 col-12 price_unit_base">
                                                    <div class="mb-1">
                                                        {{--<label class="form-label" for="price_unit">سعر الوحده</label>--}}
                                                        <input type="hidden" readonly="" name="price_unit" class="form-control-plaintext" id="price_unit" value="{{old('price_unit',0)}}" step="any">
                                                        @if($errors->has('price_unit'))
                                                            <span class="text-danger">{{ $errors->first('price_unit') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-1 col-12 wright_base">
                                                    <div class="mb-1">
                                                        {{--<label class="form-label" for="wright_base">سعر الوحده</label>--}}
                                                        <input type="hidden" readonly="" name="weight" class="form-control-plaintext" id="wright_base" value="{{old('weight',0)}}" step="any">
                                                        @if($errors->has('weight'))
                                                            <span class="text-danger">{{ $errors->first('weight') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-12 mb-50">
                                                    <div class="mb-1">
                                                        <button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete type="button">
                                                            <i data-feather="x" class="me-25"></i>
                                                            <span>حذف</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <button class="btn btn-icon btn-primary" type="button" data-repeater-create>
                                                <i data-feather="plus" class="me-25"></i>
                                                <span>اضافه جديده</span>
                                            </button>
                                        </div>
                                    </div>
                                    <!-- Invoice Total starts -->
                                    <div class="col-md-12 mt-2">
                                        <div class="mb-1 row">
                                            <div class="col-sm-2 pl-5">
                                                <label class="form-label" for="total_before_dis">سعر تكلفه المنتج</label>
                                                <input type="text" readonly="" name="cost_price" class="form-control-plaintext" id="total_before_dis" value="0" step="any" >
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Invoice Total ends -->
                                    <div class="col-12">
                                        <div class="mb-1 row">
                                            <div class="col-sm-6">
                                                <label class="col-form-label" for="first-name">مصاريف إداريه</label>
                                                <input type="number" id="paid" class="form-control" value="{{old('paid',0)}}" name="paid" placeholder="مصاريف إداريه" step="any"/>
                                                @if($errors->has('paid'))
                                                    <span class="text-danger">{{ $errors->first('paid') }}</span>
                                                @endif
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="col-form-label" for="first-name">المبلغ الكلي للتكلفه</label>
                                                <input type="number" id="residual" class="form-control" value="{{old('residual',0)}}" name="residual" placeholder="المبلغ الكلي للتكلفه" step="any"/>
                                                @if($errors->has('residual'))
                                                    <span class="text-danger">{{ $errors->first('residual') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-9 offset-sm-3">
                                        <button type="submit" class="btn btn-primary me-1" >حفظ</button>
                                        <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /Invoice repeater -->
                </div>
            </section>

        </div>
    </div>
@stop

@section('scripts')

    <script>
        var oldQuantity  = 0;
        function showItems(that) {

            // var oldTo = 0 ;
            // oldTo  = document.getElementById('total_before_dis').value;
            // // var sum = 0;
            // var sum = oldTo;
            // // alert(sum);
            var itme_id = $(that).val();
             // console.log(itme_id);
            // if(itme_id){rl: 'item/'+itme_id,
            if(itme_id){
                $.ajax({
                    url: 'item/'+itme_id,
                    type:'get',
                    success:function (data) {
                        // console.log(data);
                        if(data.status === 1){
                            that.parentNode.parentNode.parentNode.getElementsByClassName('company_base')[0].getElementsByTagName('input')[0].value = data.data.company.name
                           oldQuantity =  that.parentNode.parentNode.parentNode.getElementsByClassName('qty_base')[0].getElementsByTagName('input')[0].value = 1
                            that.parentNode.parentNode.parentNode.getElementsByClassName('price_unit_base')[0].getElementsByTagName('input')[0].value = data.data.price_unit;
                            var weight = that.parentNode.parentNode.parentNode.getElementsByClassName('wright_base')[0].getElementsByTagName('input')[0].value = data.data.weight_gram ;


                        }

                    },
                    error: function (jqXhr, textStatus, errorMessage) { // error callback
                        alert(errorMessage);
                    }
                });
            }
            else{
                $("#itemcost").empty();
                $("#staticprice").empty();
            }
        }


        function showPriceItems(that){
            var suptotal = 0;
            // alert(sum);
            var size = $(that).val();
            var priceUnit = that.parentNode.parentNode.parentNode.getElementsByClassName('price_unit_base')[0].getElementsByTagName('input')[0].value ;
            var weight = that.parentNode.parentNode.parentNode.getElementsByClassName('wright_base')[0].getElementsByTagName('input')[0].value ;

            var div = priceUnit /weight;
            // parseFloat();

            var valuePrice = size * div;

             that.parentNode.parentNode.parentNode.getElementsByClassName('price_gram_base')[0].getElementsByTagName('input')[0].value = Math.round(100*valuePrice)/100;

            var sum = 0;
            $('.itemprice_gram').each(function(){
                var value = $(this).val();
                if(value.length != 0)
                {
                    sum += parseFloat(value);
                }
            });
            $('#total_before_dis').val(sum);

        }

        $("#paid").on("input", function(){
            var total = document.getElementById('total_before_dis').value;
            var Paid = $(this).val();
            var add = parseFloat(total) + parseFloat(Paid);
            $("#residual").val(parseFloat(add));
        });


    </script>
@endsection