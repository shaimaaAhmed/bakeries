<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->id();
            $table->double('qty');
            $table->date('date');
            $table->boolean('consumption')->default('1'); // هل يوجد متبقي من الصنف بالسعر الحالي(الاستهلاك في حاله واحد يكون فيه كميه متبقيه)
            $table->double('weight')->nullable(); // الوزن الكلي بالجرام او الملي
            $table->double('weight_total')->nullable(); // الوزن الكلي بالجرام او الملي
            $table->json('residual')->nullable(); //لحساب الكميه المتبقيه في كل مره تم السحب منها حتي نفاذها (وليكن عندي الكميه=2 وتم سحب 500ملي المتبقي 1,11,400 مثال زيت 500ملي )

            $table->double('price');
            $table->double('total');
            $table->double('count_item'); // عدد الوحده وليكن 14 زجاجه والمشتري كرتونه و2 زجاجه
            $table->double('price_unit')->nullable(); // سعر الوحده الواحده
            $table->text('notes')->nullable();
            $table->unsignedBigInteger('item_id');
            $table->unsignedBigInteger('invoice_id');
            $table->unsignedBigInteger('unit_id');
            $table->foreign('item_id')
                ->references('id')->on('items')->onDelete('cascade');
            $table->foreign('unit_id')
                ->references('id')->on('units')->onDelete('cascade');
             $table->foreign('invoice_id')
                ->references('id')->on('invoices')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
