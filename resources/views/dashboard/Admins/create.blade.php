@extends('dashboard.layouts.master')

@section('title', trans('back.create-var',['var'=>trans('back.admin')]))


@section('content')


    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <i class="icon-arrow-right6 position-left"></i>
                    <span class="text-semibold">@lang('back.home')</span>
                    - @lang('back.create-var',['var'=>trans('back.admin')])
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{route('dashboard.index') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a></li>
                <li><a href="{{ route('admins.index') }}"><i class="icon-admins position-left"></i> @lang('back.admins')
                    </a></li>
                <li class="active">@lang('back.create-var',['var'=>trans('back.admin')])</li>
            </ul>

            @include('dashboard.includes.quick-links')
        </div>
    </div>
    <!-- /page header -->


    @include('dashboard.includes.errors')


    <div class="row" style="margin: 15px;">
        <div class="col-md-6">

            <!-- Basic layout-->
            <form action="{{ route('admins.store') }}" class="form-horizontal" method="post"
                  enctype="multipart/form-data">
                @csrf
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title"> اضافة مشرف جديد </h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">

                        <div class="form-group">
                            <label
                                class="col-lg-3 control-label display-block"> {{ trans('back.name_of_job') }} </label>
                            <div class="col-lg-9">
                                <select name="admin_group_id" class="select-border-color border-warning"
                                        required>
                                    <optgroup label="{{ trans('back.choose_name_of_job') }}">
                                        @foreach ($admin_groups as $group)
                                            <option value="{{ $group->id }}"> {{ $group->name }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">{{ trans('dash.full_name') }}</label>
                            <div class="col-lg-9">
                                <input type="text" name="name" value="{{ old('name') }}" class="form-control"
                                       placeholder="{{ trans('dash.full_name') }}">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-lg-3 control-label">{{ trans('dash.email') }}</label>
                            <div class="col-lg-9">
                                <input type="email" name="email" class="form-control" value="{{ old('email') }}"
                                       placeholder="{{ trans('dash.email') }}" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">{{ trans('dash.mobile') }}</label>
                            <div class="col-lg-9">
                                <input type="text" name="phone" value="{{ old('phone') }}" class="form-control"
                                       placeholder="{{ trans('dash.mobile') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">{{ trans('dash.gender') }}</label>
                            <div class="col-lg-9">
                                <select name="gender" class="select-border-color border-warning">
                                    <option value="male">{{trans('dash.male')}}</option>
                                    <option value="female">{{trans('dash.female')}}</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label"> {{ trans('dash.image') }}</label>
                            <div class="col-lg-9">
                                <input type="file" class="file-styled" name="image">
                                <span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-lg-3 control-label"> {{ trans('dash.password') }} </label>
                            <div class="col-lg-9">
                                <input type="password" class="form-control" name="password"
                                       placeholder=" {{ trans('dash.password') }} " required/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label"> {{ trans('dash.confirm_password') }} </label>
                            <div class="col-lg-9">
                                <input type="password" class="form-control" name="password_confirmation"
                                       placeholder=" {{ trans('dash.confirm_password') }} " required/>
                            </div>
                        </div>


                        <div class="text-right">
                            <input type="submit" class="btn btn-primary"
                                   value=" {{ trans('dash.added_and_forward_to_list') }} "/>
                            {{--<input type="submit" class="btn btn-success" name="back" value=" {{ trans('dash.add_and_come_back') }} " />--}}
                        </div>
                    </div>
                </div>
            </form>
            <!-- /basic layout -->

        </div>


        <div class="col-md-6">
            <div class="panel panel-flat">

                <div class="panel-heading">
                    <h5 class="panel-title"> {{ trans('back.last_admins') }} </h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">

                    <table class="table table-bordered table-hover">
                        <tr class="text-center">
                            <th> {{ trans('dash.full_name') }} </th>
                            <th> {{ trans('dash.mobile') }} </th>
                            {{--<th> {{ trans('dash.image') }} </th>--}}
                        </tr>
                        @forelse($last_admins as $admin)
                            <tr>
                                <td> {{ $admin->name }} </td>
                                <td> {{ $admin->phone }} </td>
                               </tr>
                        @empty
                            <tr><td class="alert alert-info">لا يوجد ادمن</td></tr>
                        @endforelse
                    </table>
                </div>

            </div>
        </div>
    </div>
@stop
