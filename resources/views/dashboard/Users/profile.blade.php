@extends('dashboard.layouts.master')

@section('title', trans('back.my-account'))

@section('style')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('public/admin/assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('charts') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin/assets/js/plugins/forms/inputs/touchspin.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('public/admin/assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin/assets/js/pages/form_input_groups.js') }}"></script>

    <script type="text/javascript" src="{{ asset('charts') }}"></script>
    <!-- /theme JS files -->

	<style>
		.admin-profile {
			background-image: url('{{url('http://demo.interface.club/limitless/assets/images/bg.png')}}');
			background-size: contain;
		}
		.admin-image { width: 110px; height: 110px; }
	</style>
@stop

@section('content')

    <!-- Page header -->
    <div class="page-header page-header-default" dir="{{ direction() }}">
        <div class="page-header-content">
           <div class="page-title">
                <h4>
                    <i class="icon-arrow-right6 position-left"></i>
                    <span class="text-semibold">@lang('back.dashboard')</span> - @lang('back.profile')
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb" style="float: {{ floating('right', 'left') }}">
                <li><a href="{{ url('/dashboard') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a></li>
                <li class="active">@lang('back.profile')</li>
            </ul>

            @include('dashboard.includes.quick-links')
        </div>
    </div>
    <!-- /page header -->

	<div class="content">
		<div class="row">
			<div class="col-md-3" style="float: {{ floating('right', 'left') }}">

				<div class="sidebar-detached">
					<div class="sidebar sidebar-default sidebar-separate">
						<div class="sidebar-content">
							<!-- User details -->
							<div class="content-group">
								<div class="panel-body bg-indigo-400 border-radius-top text-center admin-profile">
									<div class="content-group-sm">
										<h6 class="text-semibold no-margin-bottom">{{ ucwords($admin->name) }}</h6>
										<span class="display-block">{{$admin->email}}</span>
									</div>
{{--                                    Auth::guard('admin')->user()->ImagePath--}}
									<a href="javascript:void(0);" class="display-inline-block content-group-sm">
										<img src="{{$admin->ImagePath}}" class="img-circle img-responsive admin-image" alt="">
									</a>
								</div>

								<div class="panel no-border-top no-border-radius-top">
									<ul class="navigation">
										<li class="navigation-header">@lang('back.main')</li>

										<li class="active">
											<a href="#profile" data-toggle="tab"><i class="icon-user"></i> @lang('back.profile')</a>
										</li>

										<li>
											<a href="#schedule" data-toggle="tab"><i class="icon-gear"></i> @lang('dash.edit')</a>
										</li>

										<li class="navigation-divider"></li>

										<li>
	                                        <a href="{{ route('dashboard.logout') }}" onclick="event.preventDefault();document.getElementById('logout-form-2').submit();">
	                                            <i class="icon-switch2"></i> @lang('back.logout')
	                                        </a>

	                                        <form id="logout-form-2" action="{{ route('dashboard.logout') }}" method="POST" style="display: none;">
	                                            @csrf
	                                        </form>
										</li>
									</ul>
								</div>
							</div>
							<!-- /user details -->
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-9" style="float: {{ floating('left', 'right') }}">
				<div class="container-detached">

					<div class="content-detached">

						<!-- Tab content -->
						<div class="tab-content">
							<div class="tab-pane fade in active" id="profile">
								<!-- Profile info -->
								<div class="panel panel-flat">
									<div class="panel-heading">
										<h6 class="panel-title">@lang('back.profile-info')</h6>
									</div>

									<div class="panel-body">
										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label>@lang('back.form-name')</label>
													<input type="text" dir="{{ direction() }}" value="{{ ucwords($admin->name) }}" readonly class="form-control">
												</div>
												<div class="col-md-6">
													<label>@lang('back.form-email')</label>
													<input type="text" dir="{{ direction() }}" value="{{$admin->email}}" readonly class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label>@lang('back.form-phone')</label>
													<input type="text" dir="{{ direction() }}" value="{{ !empty(Auth::guard('admin')->user()->phone) ? Auth::guard('admin')->user()->phone : trans('back.no-value') }}" readonly class="form-control">
												</div>
												<div class="col-md-6">
													<label>@lang('back.since')</label>
													<input type="text" dir="{{ direction() }}" value="{{ Auth::guard('admin')->user()->created_at->diffForHumans() }}" readonly class="form-control">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label>@lang('back.type')</label><br>
{{--													@if ($admin->is_super_admin == 1)--}}
{{--														<label class="label label-success">@lang('back.super-admin')</label>--}}
{{--													@else--}}
{{--														<label class="label label-danger">@lang('back.admin')</label>--}}
{{--													@endif--}}
                                                    <label class="label label-danger">@lang('back.admin')</label>

                                                </div>
												<div class="col-md-6">
													<label>@lang('back.form-status')</label><br>
{{--								                    @if($admin->status == 1)--}}
{{--                                                        <span class="label label-success">@lang('back.active')</span>--}}
{{--								                    @else <span class="label label-danger">@lang('back.disactive')</span>--}}
{{--								                    @endif--}}

                                                    <span class="label label-success">@lang('back.active')</span>

                                                </div>
											</div>
										</div>
									</div>
								</div>
								<!-- /profile info -->
							</div>

							<div class="tab-pane fade" id="schedule">
								<!-- Account settings -->
								<div class="panel panel-flat">

									<div class="panel-heading"><h6 class="panel-title">@lang('dash.account_settings')</h6></div>

									<div class="panel-body">
                                        <form action="{{ route('update_profile', $admin->id) }}" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <div class="form-valid floating @error('full_name') 'has-error' @enderror">
                                                        <label for="full_name">{{ trans('back.form-name') }}</label>
                                                        <input type="text" value="{{ $admin->name }}" class="form-control" name="name" id="full_name" dir="{{ direction() }}">
                                                        @error('name') <span><strong>{{ $message }}</strong></span> @enderror
                                                    </div><br>

                                                    <div class="form-valid floating @error('email') 'has-error' @enderror">
                                                        <label for="email">{{ trans('back.form-email') }}</label>
                                                        <input type="email" value="{{ $admin->email }}" class="form-control" name="email" id="email" dir="{{ direction() }}">
                                                        @error('email') <span><strong>{{ $message }}</strong></span> @enderror
                                                    </div><br>

                                                    <div class="form-valid floating @error('mobile') 'has-error' @enderror">
                                                        <label for="mobile">{{ trans('back.form-mobile') }}</label>
                                                        <input type="tel"  value="{{ $admin->phone }}"maxlength="11" class="form-control" name="phone" id="mobile" dir="{{ direction() }}">
                                                        @error('phone') <span><strong>{{ $message }}</strong></span> @enderror
                                                    </div><br>

                                                    <div class="form-valid floating @error('password') 'has-error' @enderror">
                                                        <label for="password">{{ trans('back.form-password') }}</label>
                                                        <input type="password" class="form-control" name="password" id="password" dir="{{ direction() }}">
                                                        @error('password') <span><strong>{{ $message }}</strong></span> @enderror
                                                    </div><br>

                                                    <div class="form-valid floating">
                                                        <label for="password_confirmation">{{ trans('dash.confirm_password') }}</label>
                                                        <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" dir="{{ direction() }}">
                                                    </div><br>

                                                    <div class="col-xs-12 @error('image') 'has-error' @enderror">
                                                        <div class="row img-media">
                                                            <div class="col-xs-12">
                                                                <div class="form-valid">
                                                                    <label for="image">{{ trans('back.form-image') }}</label><br>
                                                                    <input type="file"value="{{ $admin->ImagePath }}" class="file-styled form-control" id="image" accept="image/*" name="image">
                                                                    @error('image') <span><strong>{{ $message }}</strong></span> @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="text-right">
                                                <input type="submit" name="submit" class="btn btn-primary" value="@lang('back.save')">
                                            </div>
                                        </form>
                                    </div>
								</div>
								<!-- /account settings -->
							</div>
						</div>
						<!-- /tab content -->
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('scripts')
    <script>
        $('input[name=image]').change(function(){
            readURL(this);
        });
    </script>
@stop
